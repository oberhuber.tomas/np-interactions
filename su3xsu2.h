#ifndef su3xsu2H
#define su3xsu2H

#include "su2.h"
#include "su3.h"

namespace SU3xSU2 
{
   struct LABELS: SU3::LABELS 
   {
      /****
       * Intel compiler had some issues with IDENTITY_OPERATOR [undefined symbol
       * when linking] and so I decided to comment definition of IDENTITY_OPERATOR
       * and use instead SU3xSU2::LABELS(+1, 0, 0, 0) instead.
       *
       * The following construction allows me to use SU3xSU2::LABELS::IDENTITY_OPERATOR as a parameter of SU3xSU2::LABELS constructor
       * and also IR0 = SU3xSU2::LABELS::IDENTITY_OPERATOR
       *
       * Example: 
       *    if (bra == ket && dA == 0) 
       *    {
       *       Tensor.push_back( SU3xSU2::LABELS::IDENTITY_OPERATOR );
       *    }
       *
       * struct IdentityOperatorLabels {
       * 		const static SU3::rho_type rho = +1;
       *		const static SU3::lm_type lm = 0;
       * 		const static SU3::mu_type mu = 0;
       *		const static unsigned char S2 = 0;
       *	};
       *	static IdentityOperatorLabels IDENTITY_OPERATOR;
       *	LABELS(IdentityOperatorLabels identity): SU3::LABELS(identity.rho, identity.lm, identity.mu), S2(identity.S2) {} 
       */
       
       /****
        * adan > 0 ====> creation operator on HO shell n = adan - 1             ==>	1(lm=(adan-1) mu=0) 1/2
        * adan < 0 ====> annihilation operator on HO shell n = -(adan + 1)	==>	1(lm=0 mu=-(adan+1)) 1/2
        */

      LABELS( const char adan )
      : SU3::LABELS( 1, (adan > 0) ? (adan - 1):0, (adan > 0) ? 0:-1*(adan + 1) ),
        S2( 1 ) {}

      LABELS( const SU3::LABELS& Su3Labels, const SU2::LABEL& Su2Label)
      : SU3::LABELS(Su3Labels),
        S2(Su2Label ) {}
      
      LABELS( const SU3::rho_type rho, const SU3::lm_type lm, const SU3::mu_type mu, const SU2::LABEL s2)
      : SU3::LABELS(rho, lm, mu),
        S2(s2) {}

      LABELS( const SU3::lm_type lm, const SU3::mu_type mu, const SU2::LABEL s2)
      : SU3::LABELS(lm, mu),
        S2(s2) {}
	
      //LABELS(const UN::SU3xSU2& unsu3su2): SU3::LABELS(unsu3su2.lm, unsu3su2.mu), S2(unsu3su2.S2) {}
      
      LABELS()
      : SU3::LABELS() { S2 = 0;}

      inline bool operator<( const SU3xSU2::LABELS& rhs ) const
      {
         return ((lm < rhs.lm || (lm == rhs.lm && mu < rhs.mu)) || ((lm == rhs.lm && mu == rhs.mu) && S2 < rhs.S2));
      }
      
      inline bool operator==( const SU3xSU2::LABELS& rhs ) const
      {
         return (SU3::LABELS::operator==(rhs) && S2 == rhs.S2);
      }
	
      inline bool operator!=( const SU3xSU2::LABELS& rhs ) const
      {
         return (S2 != rhs.S2 || SU3::LABELS::operator!=(rhs));
      }

      operator SU2::LABEL() const
      {
         return S2;
      }

      //inline size_t dim() {return SU3::LABELS::dim()*SU2::dim(S2);}

      SU2::LABEL S2;
   };
}

inline std::ostream& operator << (std::ostream& os, const SU3xSU2::LABELS& labels)
{
#ifdef USER_FRIENDLY_OUTPUT             
   return (os << (const SU3::LABELS&)labels << (int)labels.S2);
#else
   return (os << (const SU3::LABELS&)labels << " " << (int)labels.S2);
#endif
}

#endif
