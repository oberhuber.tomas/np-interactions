/***************************************************************************
                          config.h  -  description
                             -------------------
    begin                : 2015/01/20
    copyright            : (C) 2015 by Tomá¹ Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef configH
#define configH

class Config
{
   public:

      Config();
   
      bool parseCommandLine( int argc, char* argv[] );

      void printHelp( const char* programName );

      const char* inputFile;

      const char* outputFile;

      int Nmax, valenceShell;
   
      int processesPerNode;

      bool gpuSpeedupTest;
      
      bool optimizedGpuTransfer;
      
      bool debug;
};

extern Config config;

#endif
