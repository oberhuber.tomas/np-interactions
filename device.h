#ifndef deviceH
#define deviceH

enum enumDevice{ enumHost = 0, enumCuda };

class cuda
{
   public:

   static enumDevice getEnumDevice()
   {
      return enumCuda;
   }
};

class host
{
   public:

   static enumDevice getEnumDevice()
   {
      return enumHost;
   }
};

#endif
