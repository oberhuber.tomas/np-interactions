/***************************************************************************
                          matrixElement.h  -  description
                             -------------------
    begin                : 2015/01/19
    copyright            : (C) 2015 by Tomá¹ Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef matrixElementH
#define matrixElementH

#include <iostream>
#include <vector>
#ifdef HAVE_MPI
#include <mpi.h>
#endif
#include "sps2Index.h"
#include "functions.h"

using std::vector;

// it was ME_DATA
class matrixElement  // ??????????????????????
{
   public:

   typedef uint32_t SizeType;
   typedef uint16_t IType;
   typedef uint8_t  JType;
   typedef double   VType;

   int n[ 4 ];
   SizeType size;
   vector< IType > i1, i2, i3, i4;
   vector< JType > J;
   vector< VType > Vpp, Vnn, Vpn;

#ifdef HAVE_MPI
   MPI_Request sendRequest[ 9 ];
#endif

   matrixElement()
   : size( 0 ){};

   bool read( std::istream& file )
   {
      file.read( ( char* ) this->n, 4 * sizeof( int ) );
      
      file.read( ( char* ) &this->size, sizeof( SizeType ) );

      this->i1.resize( this->size );
      this->i2.resize( this->size );
      this->i3.resize( this->size );
      this->i4.resize( this->size );
      this->J.resize( this->size );
      this->Vpp.resize( this->size );
      this->Vnn.resize( this->size );
      this->Vpn.resize( this->size );
		
      file.read( ( char* ) this->i1.data(),  this->size * sizeof( IType ) );
      file.read( ( char* ) this->i2.data(),  this->size * sizeof( IType ) );
      file.read( ( char* ) this->i3.data(),  this->size * sizeof( IType ) );
      file.read( ( char* ) this->i4.data(),  this->size * sizeof( IType ) );
      file.read( ( char* ) this->J.data(),   this->size * sizeof( JType ) );
      file.read( ( char* ) this->Vpp.data(), this->size * sizeof( VType ) );
      file.read( ( char* ) this->Vnn.data(), this->size * sizeof( VType ) );
      file.read( ( char* ) this->Vpn.data(), this->size * sizeof( VType ) );

      if( ! file )
         return false;
      return true;
   }

   /****
    * This function multiplies all matrix elements by a factor
    * provided by the first line of equation (1) in devel/docs/Recoupling/Recoupling.pdf
    */
   void multiplyByCommonFactor()
   {
      for( int i = 0; i < this->size; ++i)
      {
         int na, la, jja, nb, lb, jjb, nc, lc, jjc, nd, ld, jjd, JJ;

         sps2Index::getNLJ( i1[ i ], na, la, jja );
         sps2Index::getNLJ( i2[ i ], nb, lb, jjb );
         sps2Index::getNLJ( i3[ i ], nc, lc, jjc );
         sps2Index::getNLJ( i4[ i ], nd, ld, jjd );
         JJ = 2 * J[ i ];

         double common_phase = MINUSto( nd + nc - (jjd + jjc)/2)*sqrt(jja + 1)*sqrt(jjb + 1)*sqrt(jjc + 1)*sqrt(jjd + 1)*(JJ + 1);
         // delta_{na nb}{la lb}{ja jb}{ta tb}
         int iab = (na == nb) && (la == lb) && (jja == jjb);
         // delta_{nc nd}{lc ld}{jc jd}{tc td}
         int idc = (nd == nc) && (ld == lc) && (jjd == jjc);

         Vpp[i] *= common_phase*sqrt((iab + 1)*(idc + 1));
         Vnn[i] *= common_phase*sqrt((iab + 1)*(idc + 1));
         Vpn[i] *= common_phase;
      }   
   }

   void switchHOConvention()
   {
      for( int i = 0; i < this->size; ++i )
      {
         int na, la, jja, nb, lb, jjb, nc, lc, jjc, nd, ld, jjd, JJ;

         sps2Index::getNLJ(i1[i], na, la, jja);
         sps2Index::getNLJ(i2[i], nb, lb, jjb);
         sps2Index::getNLJ(i3[i], nc, lc, jjc);
         sps2Index::getNLJ(i4[i], nd, ld, jjd);

         int phase = MINUSto((na - la + nb - lb + nc - lc + nd - ld)/2);

         Vpp[i] *= phase;
         Vnn[i] *= phase;
         Vpn[i] *= phase;
      }
   }

#ifdef HAVE_MPI
   
   void MPI_Send( int dest, int tag, MPI_Comm comm )
   {
      ::MPI_Isend( this->n, 4 * sizeof( int ), MPI_BYTE, dest, tag, comm, &sendRequest[ 0 ] );
      ::MPI_Isend( &this->size, sizeof( SizeType ), MPI_BYTE, dest, tag, comm, &sendRequest[ 0 ] );
      ::MPI_Isend( i1.data(),  this->size * sizeof( IType ), MPI_BYTE, dest, tag, comm, &sendRequest[ 1 ] );
      ::MPI_Isend( i2.data(),  this->size * sizeof( IType ), MPI_BYTE, dest, tag, comm, &sendRequest[ 2 ] );
      ::MPI_Isend( i3.data(),  this->size * sizeof( IType ), MPI_BYTE, dest, tag, comm, &sendRequest[ 3 ] );
      ::MPI_Isend( i4.data(),  this->size * sizeof( IType ), MPI_BYTE, dest, tag, comm, &sendRequest[ 4 ] );
      ::MPI_Isend( J.data(),   this->size * sizeof( JType ), MPI_BYTE, dest, tag, comm, &sendRequest[ 5 ] );
      ::MPI_Isend( Vpp.data(), this->size * sizeof( VType ), MPI_BYTE, dest, tag, comm, &sendRequest[ 6 ] );
      ::MPI_Isend( Vnn.data(), this->size * sizeof( VType ), MPI_BYTE, dest, tag, comm, &sendRequest[ 7 ] );
      ::MPI_Isend( Vpn.data(), this->size * sizeof( VType ), MPI_BYTE, dest, tag, comm, &sendRequest[ 8 ] );
      
      /*::MPI_Send( &this->size, sizeof( SizeType ), MPI_BYTE, dest, tag, comm );
      ::MPI_Send( i1.data(),  this->size * sizeof( IType ), MPI_BYTE, dest, tag, comm );
      ::MPI_Send( i2.data(),  this->size * sizeof( IType ), MPI_BYTE, dest, tag, comm );
      ::MPI_Send( i3.data(),  this->size * sizeof( IType ), MPI_BYTE, dest, tag, comm );
      ::MPI_Send( i4.data(),  this->size * sizeof( IType ), MPI_BYTE, dest, tag, comm );
      ::MPI_Send( J.data(),   this->size * sizeof( JType ), MPI_BYTE, dest, tag, comm );
      ::MPI_Send( Vpp.data(), this->size * sizeof( VType ), MPI_BYTE, dest, tag, comm );
      ::MPI_Send( Vnn.data(), this->size * sizeof( VType ), MPI_BYTE, dest, tag, comm );
      ::MPI_Send( Vpn.data(), this->size * sizeof( VType ), MPI_BYTE, dest, tag, comm );*/
   };
   
   void MPI_Bcast( int root, MPI_Comm comm )
   {
      ::MPI_Bcast( this->n,     4 * sizeof( int ),            MPI_BYTE, root, comm );
      ::MPI_Bcast( &this->size, sizeof( SizeType ),           MPI_BYTE, root, comm );
      this->i1.resize( this->size );
      this->i2.resize( this->size );
      this->i3.resize( this->size );
      this->i4.resize( this->size );
      this->J.resize( this->size );
      this->Vpp.resize( this->size );
      this->Vnn.resize( this->size );
      this->Vpn.resize( this->size );
      ::MPI_Bcast( i1.data(),   this->size * sizeof( IType ), MPI_BYTE, root, comm );
      ::MPI_Bcast( i2.data(),   this->size * sizeof( IType ), MPI_BYTE, root, comm );
      ::MPI_Bcast( i3.data(),   this->size * sizeof( IType ), MPI_BYTE, root, comm );
      ::MPI_Bcast( i4.data(),   this->size * sizeof( IType ), MPI_BYTE, root, comm );
      ::MPI_Bcast( J.data(),    this->size * sizeof( JType ), MPI_BYTE, root, comm );
      ::MPI_Bcast( Vpp.data(),  this->size * sizeof( VType ), MPI_BYTE, root, comm );
      ::MPI_Bcast( Vnn.data(),  this->size * sizeof( VType ), MPI_BYTE, root, comm );
      ::MPI_Bcast( Vpn.data(),  this->size * sizeof( VType ), MPI_BYTE, root, comm );
   };

   void MPI_Recv( int source, int tag, MPI_Comm comm, MPI_Status* status )
   {
      ::MPI_Recv( this->n, 4 * sizeof( int ), MPI_BYTE, source, tag, comm, status );
      ::MPI_Recv( &this->size, sizeof( SizeType ), MPI_BYTE, source, tag, comm, status );
      this->i1.resize( this->size );
      this->i2.resize( this->size );
      this->i3.resize( this->size );
      this->i4.resize( this->size );
      this->J.resize( this->size );
      this->Vpp.resize( this->size );
      this->Vnn.resize( this->size );
      this->Vpn.resize( this->size );
      ::MPI_Recv( this->i1.data(),  this->size * sizeof( IType ), MPI_BYTE, source, tag, comm, status );
      ::MPI_Recv( this->i2.data(),  this->size * sizeof( IType ), MPI_BYTE, source, tag, comm, status );
      ::MPI_Recv( this->i3.data(),  this->size * sizeof( IType ), MPI_BYTE, source, tag, comm, status );
      ::MPI_Recv( this->i4.data(),  this->size * sizeof( IType ), MPI_BYTE, source, tag, comm, status );
      ::MPI_Recv( this->J.data(),   this->size * sizeof( JType ), MPI_BYTE, source, tag, comm, status );
      ::MPI_Recv( this->Vpp.data(), this->size * sizeof( VType ), MPI_BYTE, source, tag, comm, status );
      ::MPI_Recv( this->Vnn.data(), this->size * sizeof( VType ), MPI_BYTE, source, tag, comm, status );
      ::MPI_Recv( this->Vpn.data(), this->size * sizeof( VType ), MPI_BYTE, source, tag, comm, status );
   };
#endif

   size_t sizeOf() const
   {
      return 4 * sizeof( int ) + sizeof( SizeType ) + this->size * ( 4 * sizeof( IType ) + sizeof( JType ) + 3 * sizeof( VType )  );
   }

   void print( std::ostream& str ) const
   {
      for( int i = 0; i < this->size; ++i)
      {
         int na, la, jja, nb, lb, jjb, nc, lc, jjc, nd, ld, jjd, JJ;

         sps2Index::getNLJ( i1[i], na, la, jja );
         sps2Index::getNLJ( i2[i], nb, lb, jjb );
         sps2Index::getNLJ( i3[i], nc, lc, jjc );
         sps2Index::getNLJ( i4[i], nd, ld, jjd );

         str << "\t\t";

         str << la << " " << lb << " " << lc << " " << ld << "\t\t";
         str << (int)J[i] << "\t\t" << jja << "/2 " << jjb << "/2 " << jjc << "/2 " << jjd << "/2\t\t";

         str << Vpp[i] << " " << Vnn[i] << " " << Vpn[i] << std::endl;
      }      
   }
};
#endif
