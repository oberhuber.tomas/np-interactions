#pragma once

#ifdef HAVE_CUDA
#include <cuda.h>
#include "checkCUDADevice.h"
#endif

#include <iostream>
#include "matrixElement.h"
#include "transformationResult.h"
#include "StrengthsTensorDataBuffer.h"

struct strengthsTensorData
{
   strengthsTensorData() {};
   strengthsTensorData( uint32_t size,
                        int* SfSiS0,
                        double* PiSfSiS0 )
   : size( size ), SfSiS0( SfSiS0 ), PiSfSiS0( PiSfSiS0 )
   {};

   strengthsTensorData( SU3::LABELS& _wf,
                        SU3::LABELS& _wi,
                        SU3::LABELS& _w0,
                        const matrixElement& me,
                        uint16_t* _lalb_indices,
                        int _lalb_indices_size,
                        uint16_t* _ldlc_indices,
                        int _ldlc_indices_size,
                        uint16_t* _lfli_indices, 
                        int _lfli_indices_size, 
                        double* _abf_cgSU3,
                        int _abf_cgSU3_size,
                        double* _dci_cgSU3,
                        int _dci_cgSU3_size,
                        double* _fi0_cgSU3, 
                        int _fi0_cgSU3_size,
                        int* _SfSiS0,
                        double* _PiSfSiS0, 
                        double* _results,
                        int _resultsISSize )
   : wf( _wf ), wi( _wi ), w0( _w0 ),
     size( me.size ),
     i1( const_cast< uint16_t* >( me.i1.data() ) ),
     i2( const_cast< uint16_t* >( me.i2.data() ) ),
     i3( const_cast< uint16_t* >( me.i3.data() ) ), 
     i4( const_cast< uint16_t* >( me.i4.data() ) ),
     J( const_cast< uint8_t* >( me.J.data() ) ),
     Vpp( const_cast< double* >( me.Vpp.data() ) ),
     Vnn( const_cast< double* >( me.Vnn.data() ) ),
     Vpn( const_cast< double* >( me.Vpn.data() ) ),
     //matrixEl( me ),
     lalb_indices( _lalb_indices ), ldlc_indices( _ldlc_indices ), lfli_indices( _lfli_indices ),
     lalb_indices_size( _lalb_indices_size ), ldlc_indices_size( _ldlc_indices_size ), lfli_indices_size( _lfli_indices_size ),
     abf_cgSU3( _abf_cgSU3 ), dci_cgSU3( _dci_cgSU3 ), fi0_cgSU3( _fi0_cgSU3 ),
     abf_cgSU3_size( _abf_cgSU3_size ), dci_cgSU3_size( _dci_cgSU3_size ), fi0_cgSU3_size( _fi0_cgSU3_size ),
     SfSiS0( _SfSiS0 ),
     PiSfSiS0( _PiSfSiS0 ),
     results( _results ),
     resultsISSize( _resultsISSize )
     {
     };
     
#ifdef HAVE_CUDA
      void copyMEData( const matrixElement& me_data, 
                      StrengthsTensorDataBuffer& tensorDataBuffer,
                      bool optimizedTransfer = true )
      {
         if( optimizedTransfer )
         {
            double *Vpp_data, *Vnn_data, *Vpn_data;
            uint16_t *i1_data, *i2_data, *i3_data, *i4_data;
            uint8_t* J_data;
            tensorDataBuffer.allocate_double( me_data.size, Vpp_data, this->Vpp );
            tensorDataBuffer.allocate_double( me_data.size, Vnn_data, this->Vnn );
            tensorDataBuffer.allocate_double( me_data.size, Vpn_data, this->Vpn );
            tensorDataBuffer.allocate_uint16( me_data.size, i1_data, this->i1 );
            tensorDataBuffer.allocate_uint16( me_data.size, i2_data, this->i2 );
            tensorDataBuffer.allocate_uint16( me_data.size, i3_data, this->i3 );
            tensorDataBuffer.allocate_uint16( me_data.size, i4_data, this->i4 );
            tensorDataBuffer.allocate_uint8( me_data.size, J_data, this->J );
            memcpy( Vpp_data, me_data.Vpp.data(), me_data.size * sizeof( double ) );
            memcpy( Vnn_data, me_data.Vnn.data(), me_data.size * sizeof( double ) );
            memcpy( Vpn_data, me_data.Vpn.data(), me_data.size * sizeof( double ) );
            memcpy( i1_data , me_data.i1.data(),  me_data.size * sizeof( uint16_t ) );
            memcpy( i2_data , me_data.i2.data(),  me_data.size * sizeof( uint16_t ) );
            memcpy( i3_data , me_data.i3.data(),  me_data.size * sizeof( uint16_t ) );
            memcpy( i4_data , me_data.i4.data(),  me_data.size * sizeof( uint16_t ) );
            memcpy( J_data , me_data.J.data(),    me_data.size * sizeof( uint8_t ) );
         }
         else
         {
            size = me_data.size;
            //std::cerr << "Allocating ME data on GPU ..." << std::endl;
            cudaMalloc( ( void** ) &Vpp, size*sizeof( double ) );
            cudaMalloc( ( void** ) &Vnn, size*sizeof( double ) );
            cudaMalloc( ( void** ) &Vpn, size*sizeof( double ) );
            cudaMalloc( ( void** ) &i1,  size*sizeof( uint16_t ) );
            cudaMalloc( ( void** ) &i2,  size*sizeof( uint16_t ) );
            cudaMalloc( ( void** ) &i3,  size*sizeof( uint16_t ) );
            cudaMalloc( ( void** ) &i4,  size*sizeof( uint16_t ) );
            cudaMalloc( ( void** ) &J,   size*sizeof( uint8_t ) );
            CHECK_CUDA_DEVICE;


            cudaMemcpy( Vpp, me_data.Vpp.data(), size*sizeof( double ), cudaMemcpyHostToDevice );
            cudaMemcpy( Vnn, me_data.Vnn.data(), size*sizeof( double ), cudaMemcpyHostToDevice );
            cudaMemcpy( Vpn, me_data.Vpn.data(), size*sizeof( double ), cudaMemcpyHostToDevice );
            cudaMemcpy( i1,  me_data.i1.data(),  size*sizeof( uint16_t ), cudaMemcpyHostToDevice );
            cudaMemcpy( i2,  me_data.i2.data(),  size*sizeof( uint16_t ), cudaMemcpyHostToDevice );
            cudaMemcpy( i3,  me_data.i3.data(),  size*sizeof( uint16_t ), cudaMemcpyHostToDevice );
            cudaMemcpy( i4,  me_data.i4.data(),  size*sizeof( uint16_t ), cudaMemcpyHostToDevice );
            cudaMemcpy( J,   me_data.J.data(),   size*sizeof( uint8_t ), cudaMemcpyHostToDevice );
            CHECK_CUDA_DEVICE;

            freeMEData = true;           
         }
      }
     
     void copy_FinalIrreps( SU3::LABELS& wf,
                            vector< uint16_t >& lalb_indices,
                            vector< double >& abf_cgSU3,
                            StrengthsTensorDataBuffer& tensorDataBuffer,
                            bool optimizedTransfer = true )
     {
        this->wf = wf;        
        if( optimizedTransfer )
        {
            uint16_t* lalb_indices_data;
            double* abf_cgSU3_data;
            tensorDataBuffer.allocate_uint16( lalb_indices.size(), lalb_indices_data, this->lalb_indices );
            tensorDataBuffer.allocate_double( abf_cgSU3.size(), abf_cgSU3_data, this->abf_cgSU3 );
            memcpy( lalb_indices_data, lalb_indices.data(), lalb_indices.size() * sizeof( uint16_t ) );
            memcpy( abf_cgSU3_data, abf_cgSU3.data(), abf_cgSU3.size() * sizeof( double ) );
        }
        else
        {
            //std::cerr << "Allocating Final Irreps on GPU ..." << std::endl;
            cudaMalloc( ( void** ) &this->lalb_indices, lalb_indices.size()*sizeof( uint16_t ) );
            cudaMalloc( ( void** ) &this->abf_cgSU3, abf_cgSU3.size()*sizeof( double ) );
            CHECK_CUDA_DEVICE;
            cudaMemcpy( this->lalb_indices, lalb_indices.data(), lalb_indices.size()*sizeof( uint16_t ), cudaMemcpyHostToDevice );
            CHECK_CUDA_DEVICE;
            cudaMemcpy( this->abf_cgSU3, abf_cgSU3.data(), abf_cgSU3.size()*sizeof( double ), cudaMemcpyHostToDevice );
            CHECK_CUDA_DEVICE;
            freeFinalIrreps = true;
        }
     }
     
     void copy_InitIrreps( SU3::LABELS& wi,
                           vector< uint16_t >& ldlc_indices,
                           vector< double >& dci_cgSU3,
                           StrengthsTensorDataBuffer& tensorDataBuffer,
                           bool optimizedTransfer = true )
     {
        this->wi = wi;        
        if( optimizedTransfer )
        {
            uint16_t* ldlc_indices_data;
            double* dci_cgSU3_data;
            tensorDataBuffer.allocate_uint16( ldlc_indices.size(), ldlc_indices_data, this->ldlc_indices );
            tensorDataBuffer.allocate_double( dci_cgSU3.size(), dci_cgSU3_data, this->dci_cgSU3 );
            memcpy( ldlc_indices_data, ldlc_indices.data(), ldlc_indices.size() * sizeof( uint16_t ) );
            memcpy( dci_cgSU3_data, dci_cgSU3.data(), dci_cgSU3.size() * sizeof( double ) );
        }
        else
        {
            //std::cerr << "Allocating Init Irreps on GPU ..." << std::endl;
            cudaMalloc( ( void** ) &this->ldlc_indices, ldlc_indices.size()*sizeof( uint16_t ) );
            cudaMalloc( ( void** ) &this->dci_cgSU3, dci_cgSU3.size()*sizeof( double ) );
            CHECK_CUDA_DEVICE;

            cudaMemcpy( this->ldlc_indices, ldlc_indices.data(), ldlc_indices.size()*sizeof( uint16_t ), cudaMemcpyHostToDevice );
            cudaMemcpy( this->dci_cgSU3, dci_cgSU3.data(), dci_cgSU3.size()*sizeof( double ), cudaMemcpyHostToDevice );
            CHECK_CUDA_DEVICE;
            freeInitIrreps = true;
        }
     }
     
     void copy_Irreps0( SU3::LABELS& w0,
                        vector< uint16_t >& lfli_indices,
                        vector< double >& fi0_cgSU3,
                        StrengthsTensorDataBuffer& tensorDataBuffer,
                        bool optimizedTransfer = true )
     {
        this->w0 = w0;
        if( optimizedTransfer )
        {
            uint16_t* lfli_indices_data;
            double* fi0_cgSU3_data;
            tensorDataBuffer.allocate_uint16( lfli_indices.size(), lfli_indices_data, this->lfli_indices );
            tensorDataBuffer.allocate_double( fi0_cgSU3.size(), fi0_cgSU3_data, this->fi0_cgSU3 );
            memcpy( lfli_indices_data, lfli_indices.data(), lfli_indices.size() * sizeof( uint16_t ) );
            memcpy( fi0_cgSU3_data, fi0_cgSU3.data(), fi0_cgSU3.size() * sizeof( double ) );
        }
        else
        {
            //std::cerr << "Allocating Irreps0 on GPU ..." << std::endl;
            cudaMalloc( ( void** ) &this->lfli_indices, lfli_indices.size()*sizeof( uint16_t ) );
            cudaMalloc( ( void** ) &this->fi0_cgSU3, fi0_cgSU3.size()*sizeof( double ) );
            CHECK_CUDA_DEVICE;
            
            cudaMemcpy( this->lfli_indices, lfli_indices.data(), lfli_indices.size()*sizeof( uint16_t ), cudaMemcpyHostToDevice );
            cudaMemcpy( this->fi0_cgSU3, fi0_cgSU3.data(), fi0_cgSU3.size()*sizeof( double ), cudaMemcpyHostToDevice );
            CHECK_CUDA_DEVICE;
            freeIrreps0 = true;           
        }
     }
     
     
     bool transferPiSfSiS0( int* _SfSiS0, double* _PiSfSiS0 )
     {
        //std::cerr << "Allocating PiSfSiS0 on GPU ..." << std::endl;
         cudaMalloc( ( void** ) &this->SfSiS0, 18*sizeof( int ) );
         cudaMalloc( ( void** ) &this->PiSfSiS0, 6*sizeof( double ) );
         CHECK_CUDA_DEVICE;
         
         cudaMemcpy( this->SfSiS0, _SfSiS0, 18*sizeof( int ), cudaMemcpyHostToDevice );
         cudaMemcpy( this->PiSfSiS0, _PiSfSiS0, 6*sizeof( double ), cudaMemcpyHostToDevice );
         CHECK_CUDA_DEVICE;
         this->freePiSfSiS0 = true;
         return true;
     }
     
     
     bool setupResults( double* _results,
                        int _resultsISSize,
                        int cudaBlocks )
     {
         this->resultsISSize = _resultsISSize;
         cudaMalloc( ( void** ) &results, cudaBlocks*6*resultsISSize*sizeof( double ) );
         CHECK_CUDA_DEVICE;
         return true;
     }
     
     
     void resetDealocationFlags()
     {
        this->freeMEData = false;
        this->freeFinalIrreps = false;
        this->freeInitIrreps = false;
        this->freeIrreps0 = false;
        this->freePiSfSiS0 = false;
     }
     
     void freeGPUData()
     {
        if( this->freePiSfSiS0 )
        {
           //std::cerr << "Freeing PiSfSiS0 on GPU ..." << std::endl;
            cudaFree( SfSiS0 );
            cudaFree( PiSfSiS0 );
            CHECK_CUDA_DEVICE;
        }
        
        if( this->freeMEData )
        {
           //std::cerr << "Freeing ME data on GPU ..." << std::endl;
            cudaFree( Vpp );
            cudaFree( Vnn );
            cudaFree( Vpn );
            cudaFree( i1 );
            cudaFree( i2 );
            cudaFree( i3 );
            cudaFree( i4 );
            cudaFree( J );
            CHECK_CUDA_DEVICE;
        }
         
        if( this->freeFinalIrreps )
        {
           //std::cerr << "Freeing Final Irreps on GPU ..." << std::endl;
           cudaFree( lalb_indices );
           cudaFree( abf_cgSU3 );
           CHECK_CUDA_DEVICE;
        }
        
        if( this->freeInitIrreps )
        {
           //std::cerr << "Freeing Init Irreps on GPU ..." << std::endl;
           cudaFree( ldlc_indices );
           cudaFree( dci_cgSU3 );
           CHECK_CUDA_DEVICE;
        }
        if( this->freeIrreps0 )
        {
           //std::cerr << "Freeing Irreps0 on GPU ..." << std::endl;
           cudaFree( lfli_indices );
           cudaFree( fi0_cgSU3 );
           CHECK_CUDA_DEVICE;
        }
        //std::cerr << "Freeing results on GPU ..." << std::endl;
        //cudaFree( results );
        CHECK_CUDA_DEVICE;
     }
#endif
     
     void checkIndices()
     {
        for( int i = 0; i < size; i++ )
        {
            int na, nb, nc, nd, la, lb, lc, ld, jja, jjb, jjc, jjd;
            sps2Index::getNLJ( i1[i], na, la, jja);
            sps2Index::getNLJ( i2[i], nb, lb, jjb);
            sps2Index::getNLJ( i3[i], nc, lc, jjc);
            sps2Index::getNLJ( i4[i], nd, ld, jjd);
            assert( la*(nb+1) + lb < lalb_indices_size );
            assert( ld*(nc+1) + lc < ldlc_indices_size );
        }
     }
     

     
   SU3::LABELS wf, wi, w0;
   //const matrixElement* matrixEl;
   uint32_t size;
   uint16_t *i1, *i2, *i3, *i4;
   uint8_t* J;
   double *Vpp, *Vnn, *Vpn;
   uint16_t *lalb_indices, *ldlc_indices, *lfli_indices;
   int lalb_indices_size, ldlc_indices_size, lfli_indices_size;
   double *abf_cgSU3, *dci_cgSU3, *fi0_cgSU3;
   int abf_cgSU3_size, dci_cgSU3_size, fi0_cgSU3_size;
   int* SfSiS0;
   double* PiSfSiS0; 
   double* results, *results_host;
   int resultsISSize;
   
   bool freeMEData = false,
        freeFinalIrreps = false,
        freeInitIrreps = false,
        freeIrreps0 = false,
        freePiSfSiS0  = false;
     
};

inline bool transferTensorDataToGPU( const strengthsTensorData& hostTensor,
                                     const int cudaBlocks,
                                     strengthsTensorData& deviceTensor )
{
   const int totalSize = hostTensor.size*( 3*sizeof( double )+4*sizeof( uint16_t )+sizeof( uint8_t ) ) + 
                         ( hostTensor.lalb_indices_size+hostTensor.ldlc_indices_size+hostTensor.lfli_indices_size )*sizeof( uint16_t ) +
                         ( hostTensor.abf_cgSU3_size+hostTensor.dci_cgSU3_size+hostTensor.fi0_cgSU3_size )*sizeof( double ) +
                         18*sizeof( int ) + 6*sizeof( double )+
                         cudaBlocks*6*hostTensor.resultsISSize*sizeof( double );
   //std::cout << "Allocating " << totalSize*1.0e-9 << " GB on GPU." << std::endl;

#ifdef HAVE_CUDA
   //cudaMalloc( ( void** ) &deviceTensor.Vpp, hostTensor.size*sizeof( double ) );
   cudaMalloc( ( void** ) &deviceTensor.Vnn, hostTensor.size*sizeof( double ) );
   cudaMalloc( ( void** ) &deviceTensor.Vpn, hostTensor.size*sizeof( double ) );
   cudaMalloc( ( void** ) &deviceTensor.i1, hostTensor.size*sizeof( uint16_t ) );
   cudaMalloc( ( void** ) &deviceTensor.i2, hostTensor.size*sizeof( uint16_t ) );
   cudaMalloc( ( void** ) &deviceTensor.i3, hostTensor.size*sizeof( uint16_t ) );
   cudaMalloc( ( void** ) &deviceTensor.i4, hostTensor.size*sizeof( uint16_t ) );
   cudaMalloc( ( void** ) &deviceTensor.J, hostTensor.size*sizeof( uint8_t ) );
   cudaMalloc( ( void** ) &deviceTensor.lalb_indices, hostTensor.lalb_indices_size*sizeof( uint16_t ) );
   cudaMalloc( ( void** ) &deviceTensor.ldlc_indices, hostTensor.ldlc_indices_size*sizeof( uint16_t ) );
   cudaMalloc( ( void** ) &deviceTensor.lfli_indices, hostTensor.lfli_indices_size*sizeof( uint16_t ) ); 
   cudaMalloc( ( void** ) &deviceTensor.abf_cgSU3, hostTensor.abf_cgSU3_size*sizeof( double ) );
   cudaMalloc( ( void** ) &deviceTensor.dci_cgSU3, hostTensor.dci_cgSU3_size*sizeof( double ) );
   cudaMalloc( ( void** ) &deviceTensor.fi0_cgSU3, hostTensor.fi0_cgSU3_size*sizeof( double ) );
   cudaMalloc( ( void** ) &deviceTensor.results, cudaBlocks*6*hostTensor.resultsISSize*sizeof( double ) );
   //cudaMalloc( ( void** ) &deviceTensor.SfSiS0, 18*sizeof( int ) );
   //cudaMalloc( ( void** ) &deviceTensor.PiSfSiS0, 6*sizeof( double ) );
   CHECK_CUDA_DEVICE;
   
   if( //cudaMemcpy( deviceTensor.Vpp, hostTensor.Vpp, hostTensor.size*sizeof( double ), cudaMemcpyHostToDevice ) != cudaSuccess ||
       cudaMemcpy( deviceTensor.Vnn, hostTensor.Vnn, hostTensor.size*sizeof( double ), cudaMemcpyHostToDevice ) != cudaSuccess ||
       cudaMemcpy( deviceTensor.Vpn, hostTensor.Vpn, hostTensor.size*sizeof( double ), cudaMemcpyHostToDevice ) != cudaSuccess ||
       cudaMemcpy( deviceTensor.i1, hostTensor.i1, hostTensor.size*sizeof( uint16_t ), cudaMemcpyHostToDevice ) != cudaSuccess ||
       cudaMemcpy( deviceTensor.i2, hostTensor.i2, hostTensor.size*sizeof( uint16_t ), cudaMemcpyHostToDevice ) != cudaSuccess ||
       cudaMemcpy( deviceTensor.i3, hostTensor.i3, hostTensor.size*sizeof( uint16_t ), cudaMemcpyHostToDevice ) != cudaSuccess ||
       cudaMemcpy( deviceTensor.i4, hostTensor.i4, hostTensor.size*sizeof( uint16_t ), cudaMemcpyHostToDevice ) != cudaSuccess ||
       cudaMemcpy( deviceTensor.J, hostTensor.J, hostTensor.size*sizeof( uint8_t ), cudaMemcpyHostToDevice ) != cudaSuccess ||
       cudaMemcpy( deviceTensor.lalb_indices, hostTensor.lalb_indices, hostTensor.lalb_indices_size*sizeof( uint16_t ), cudaMemcpyHostToDevice ) != cudaSuccess ||
       cudaMemcpy( deviceTensor.ldlc_indices, hostTensor.ldlc_indices, hostTensor.ldlc_indices_size*sizeof( uint16_t ), cudaMemcpyHostToDevice ) != cudaSuccess ||
       cudaMemcpy( deviceTensor.lfli_indices, hostTensor.lfli_indices, hostTensor.lfli_indices_size*sizeof( uint16_t ), cudaMemcpyHostToDevice ) != cudaSuccess ||
       cudaMemcpy( deviceTensor.abf_cgSU3, hostTensor.abf_cgSU3, hostTensor.abf_cgSU3_size*sizeof( double ), cudaMemcpyHostToDevice ) != cudaSuccess ||
       cudaMemcpy( deviceTensor.dci_cgSU3, hostTensor.dci_cgSU3, hostTensor.dci_cgSU3_size*sizeof( double ), cudaMemcpyHostToDevice ) != cudaSuccess ||
       cudaMemcpy( deviceTensor.fi0_cgSU3, hostTensor.fi0_cgSU3, hostTensor.fi0_cgSU3_size*sizeof( double ), cudaMemcpyHostToDevice ) != cudaSuccess || 
       cudaMemcpy( deviceTensor.SfSiS0, hostTensor.SfSiS0, 18*sizeof( int ), cudaMemcpyHostToDevice ) != cudaSuccess ||
       cudaMemcpy( deviceTensor.PiSfSiS0, hostTensor.PiSfSiS0, 6*sizeof( double ), cudaMemcpyHostToDevice ) != cudaSuccess )
   {
      std::cerr << "Unable to transfer the data to the GPU." << std::endl;
      return false;
   }
   deviceTensor.wf = hostTensor.wf;
   deviceTensor.wi = hostTensor.wi;
   deviceTensor.w0 = hostTensor.w0;
   deviceTensor.size = hostTensor.size;
   deviceTensor.lalb_indices_size = hostTensor.lalb_indices_size;
   deviceTensor.ldlc_indices_size = hostTensor.ldlc_indices_size;
   deviceTensor.lfli_indices_size = hostTensor.lfli_indices_size;
   deviceTensor.resultsISSize = hostTensor.resultsISSize;
   return true;
#else
   return false;
#endif
}

inline void freeGPUData( const strengthsTensorData& deviceTensor )
{
#ifdef HAVE_CUDA
   //std::cout << "Freeing data from device" << std::endl;
    /*cudaFree( deviceTensor.Vpp );
    cudaFree( deviceTensor.Vnn );
    cudaFree( deviceTensor.Vpn );
    cudaFree( deviceTensor.i1 );
    cudaFree( deviceTensor.i2 );
    cudaFree( deviceTensor.i3 );
    cudaFree( deviceTensor.i4 );
    cudaFree( deviceTensor.J );*/
    cudaFree( deviceTensor.lalb_indices );
    cudaFree( deviceTensor.ldlc_indices );
    cudaFree( deviceTensor.lfli_indices );
    cudaFree( deviceTensor.abf_cgSU3 );
    cudaFree( deviceTensor.dci_cgSU3 );
    cudaFree( deviceTensor.fi0_cgSU3 );
    cudaFree( deviceTensor.results );
    cudaFree( deviceTensor.SfSiS0 );
    cudaFree( deviceTensor.PiSfSiS0 );
#endif
}
