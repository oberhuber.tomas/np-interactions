#include <cmath>

int SfSiS0[6][3] =  {{0, 0, 0}, {1, 1, 0}, {0, 1, 1}, {1, 0, 1}, {1, 1, 1},      {1, 1, 2}};
double PiSfSiS0[6] = {   1,         3,         3,         3,    std::sqrt(27),         std::sqrt(45)};

int* SfSiS0_device;
double* PiSfSiS0_device;
