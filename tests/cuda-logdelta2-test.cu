#include <cuda.h>
#include <iostream>
#include <stdio.h>

#include "../logFactTable.h"
#include "../logDelta2.h"
#include "../get-time.h"

using namespace std;

const int maxLogFact( 128 );

__global__ void logDelta2TestKernel( const int* a2,
                                     const int* b2,
                                     const int* c2,
                                     double* logDelta2Results,
                                     const int size,
                                     const double* globalLogFactTable,
                                     const int logFactTableSize )
{
   extern __shared__ double sharedLogFactTable[];
   logFactTable< cuda > lfTable( globalLogFactTable,
                                 sharedLogFactTable,
                                 logFactTableSize );
   logDelta2< cuda > logDelta2( &lfTable );
   const int gridSize = gridDim.x * blockDim.x;
   int element = blockIdx.x*blockDim.x + threadIdx.x;
   while( element < size )
   {
      logDelta2Results[ element ] = 
         logDelta2.getLogDelta2( a2[ element ], b2[ element ], c2[ element ] );
      element += gridSize; 
   }
}

int main( int argc, char* argv[] )
{
   const int size = 65536*512;
   cout << "Testing logDelta2..." << endl;
   
   logFactTable< host > logFactTable( maxLogFact );
   logDelta2< host > logDelta2( &logFactTable );

   int* a2 = new int[ size ];
   int* b2 = new int[ size ];
   int* c2 = new int[ size ];

   int i( 0 );
   while( i < 65536 && i < size )
   {
      a2[ i ] = rand() % maxLogFact;
      b2[ i ] = rand() % maxLogFact;
      c2[ i ] = rand() % maxLogFact;

      int t1 = ( a2[ i ] + b2[ i ] - c2[ i ] ) / 2;
      int t2 = ( c2[ i ] + a2[ i ] - b2[ i ] ) / 2;
      int t3 = ( b2[ i ] + c2[ i ] - a2[ i ] ) / 2;
      int t4 = ( a2[ i ] + b2[ i ] + c2[ i ] ) / 2 + 1;

      if( t1 >= 0 && t1 < maxLogFact && 
          t2 >= 0 && t2 < maxLogFact && 
          t3 >= 0 && t3 < maxLogFact && 
          t4 >= 0 && t4 < maxLogFact )
         i++;
      cout << i << " / " << 65536 << "   \r " << flush;
   }
   while( i < size )
   {
      a2[ i ] = a2[ i % 65536 ];
      b2[ i ] = b2[ i % 65536 ];
      c2[ i ] = c2[ i % 65536 ];
      i++;
   }

   double* logDelta2Results = new double[ size ];
   double startHostTime = getTime();
   for( int i = 0; i < size; i++ )
   {
      logDelta2Results[ i ] = logDelta2.getLogDelta2( a2[ i ], b2[ i ], c2[ i ] );
   }
   double endHostTime = getTime();

   int *cuda_a2, *cuda_b2, *cuda_c2;
   if( cudaMalloc( ( void** ) &cuda_a2, size*sizeof( int ) ) != cudaSuccess ||
       cudaMalloc( ( void** ) &cuda_b2, size*sizeof( int ) ) != cudaSuccess ||
       cudaMalloc( ( void** ) &cuda_c2, size*sizeof( int ) ) != cudaSuccess )
   {
      cerr << "Unable to allocate logDelat2 parameters on the GPU." << endl;
      return EXIT_FAILURE;
   }
   
   double* logDelta2ResultsCuda;
   cudaMalloc( ( void** ) &logDelta2ResultsCuda, size*sizeof( double ) );
   CHECK_CUDA_DEVICE;
   /*if( ! checkCUDADevice() )
   {
      cerr << "Unable to allocate logFactResultsCuda on GPU." << endl;
      return EXIT_FAILURE;
   }*/

   if( cudaMemcpy( cuda_a2, a2, size*sizeof( int ), cudaMemcpyHostToDevice ) != cudaSuccess ||
       cudaMemcpy( cuda_b2, b2, size*sizeof( int ), cudaMemcpyHostToDevice ) != cudaSuccess ||
       cudaMemcpy( cuda_c2, c2, size*sizeof( int ), cudaMemcpyHostToDevice ) != cudaSuccess )
   {
      cerr << "Unable to copy the logDelta2 parameters on the GPU." << endl;
      return EXIT_FAILURE;
   }

   dim3 gridSize, blockSize;
   blockSize.x = 256;
   gridSize.x = min( maxGridSize, size / blockSize.x + ( size % blockSize.x != 0 ) );
   const int sharedTableSize = maxLogFact*warpSize*sizeof( double );
   double startCudaTime = getTime();
   logDelta2TestKernel<<< gridSize, blockSize, sharedTableSize >>>( cuda_a2,
                                                                    cuda_b2,
                                                                    cuda_c2,
                                                                    logDelta2ResultsCuda,
                                                                    size,
                                                                    logFactTable.getGPUTable(),
                                                                    maxLogFact );
   cudaThreadSynchronize();
   double endCudaTime = getTime();
   CHECK_CUDA_DEVICE;
   /*if( ! checkCUDADevice() )
   {
      cerr << "The kernel launch failed. " << endl;
      return EXIT_FAILURE;
   }*/
   
   double* aux = new double[ size ];
   cudaMemcpy( aux, logDelta2ResultsCuda, size*sizeof( double ), cudaMemcpyDeviceToHost );
   CHECK_CUDA_DEVICE;
   /*if( ! checkCUDADevice() )
   {
      cerr << "Unable to copy the result from the GPU to the host." << endl;
      return EXIT_FAILURE;
   }*/

   double cpuTime = endHostTime - startHostTime;
   double cudaTime = endCudaTime - startCudaTime;
   cout << "The CPU time is " << cpuTime << endl;
   cout << "The GPU time is " << cudaTime << endl;
   cout << "The speed-up is " << cpuTime / cudaTime << endl;

   for( int i = 0; i < size; i++ )
   {
      //cout << a2[ i ] << " " << b2[ i ] << " " << c2[ i ] << " " << aux[ i ] << " ? " << logDelta2Results[ i ] << endl;
      if( aux[ i ] != logDelta2Results[ i ] )
      {
         cerr << "The results differ by " << aux[ i ] - logDelta2Results[ i ] << " at position " << i << "." << endl;
         //return EXIT_FAILURE;
      }
   }

   delete[] a2;
   delete[] b2;
   delete[] c2;
   delete[] logDelta2Results;
   delete[] aux;
   cudaFree( cuda_a2 );
   cudaFree( cuda_b2 );
   cudaFree( cuda_c2 );
   cudaFree( logDelta2ResultsCuda );
   CHECK_CUDA_DEVICE;
   /*if( ! checkCUDADevice() )
      return EXIT_FAILURE;*/
   return EXIT_SUCCESS;
}
