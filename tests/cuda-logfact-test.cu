#include <cuda.h>
#include <iostream>
#include <stdio.h>

#include "../logFactTable.h"
#include "../get-time.h"

using namespace std;

const int maxLogFact( 128 );

__global__ void logFactTestKernel( int* logFactParams,
                                   double* logFactResults,
                                   const int size,
                                   const double* globalLogFactTable,
                                   const int logFactTableSize )
{
   extern __shared__ double sharedLogFactTable[];
   logFactTable< cuda > lfTable( globalLogFactTable,
                                 sharedLogFactTable,
                                 logFactTableSize );
   const int gridSize = gridDim.x * blockDim.x;
   int element = blockIdx.x*blockDim.x + threadIdx.x;
   while( element < size )
   {
      logFactResults[ element ] = 
         lfTable.getLogFact( logFactParams[ element ] );
      element += gridSize; 
   }
}

int main( int argc, char* argv[] )
{
   const int size = 65536*512;
   cout << "Testing logFactTable..." << endl;
   
   logFactTable< host > logFactTable( maxLogFact );

   int* logFactParams = new int[ size ];
   for( int i = 0; i < size; i++ )
      logFactParams[ i ] = i % maxLogFact;

   double* logFactResults = new double[ size ];
   double startHostTime = getTime();
   for( int i = 0; i < size; i++ )
   {
      logFactResults[ i ] = logFactTable.getLogFact( logFactParams[ i ] );
   }
   double endHostTime = getTime();

   int* logFactParamsCuda;
   cudaMalloc( ( void** ) &logFactParamsCuda, size*sizeof( int ) );
   CHECK_CUDA_DEVICE;
   /*if( ! checkCUDADevice() )
   {
      cerr << "Unable to allocate logFactParamsCuda on GPU." << endl;
      return EXIT_FAILURE;
   }*/
   
   double* logFactResultsCuda;
   cudaMalloc( ( void** ) &logFactResultsCuda, size*sizeof( double ) );
   CHECK_CUDA_DEVICE;
   /*if( ! checkCUDADevice() )
   {
      cerr << "Unable to allocate logFactResultsCuda on GPU." << endl;
      return EXIT_FAILURE;
   }*/

   cudaMemcpy( logFactParamsCuda, logFactParams, size*sizeof( int ), cudaMemcpyHostToDevice );
   CHECK_CUDA_DEVICE;
   /*if( ! checkCUDADevice() )
   {
      cerr << "Unable to copy the logFactParams on the GPU." << endl;
      return EXIT_FAILURE;
   }*/

   dim3 gridSize, blockSize;
   gridSize.x = 16384;
   blockSize.x = 256;
   const int sharedTableSize = maxLogFact*warpSize*sizeof( double );
   double startCudaTime = getTime();
   logFactTestKernel<<< gridSize, blockSize, sharedTableSize >>>( logFactParamsCuda,
                                                                  logFactResultsCuda,
                                                                  size,
                                                                  logFactTable.getGPUTable(),
                                                                  maxLogFact );
   cudaThreadSynchronize();
   double endCudaTime = getTime();
   CHECK_CUDA_DEVICE;
   /*if( ! checkCUDADevice() )
   {
      cerr << "The kernel launch failed. " << endl;
      return EXIT_FAILURE;
   }*/
   
   double* aux = new double[ size ];
   cudaMemcpy( aux, logFactResultsCuda, size*sizeof( double ), cudaMemcpyDeviceToHost );
   CHECK_CUDA_DEVICE;
   /*if( ! checkCUDADevice() )
   {
      cerr << "Unable to copy the result from the GPU to the host." << endl;
      return EXIT_FAILURE;
   }*/

   double cpuTime = endHostTime - startHostTime;
   double cudaTime = endCudaTime - startCudaTime;
   cout << "The CPU time is " << cpuTime << endl;
   cout << "The GPU time is " << cudaTime << endl;
   cout << "The speed-up is " << cpuTime / cudaTime << endl;

   for( int i = 0; i < size; i++ )
   {
      if( aux[ i ] != logFactResults[ i ] )
      {
         cerr << "The results differ by " << aux[ i ] - logFactResults[ i ] << " at position " << i << "." << endl;
         return EXIT_FAILURE;
      }
   }

   delete[] logFactParams;
   delete[] logFactResults;
   delete[] aux;
   cudaFree( logFactParamsCuda );
   cudaFree( logFactResultsCuda );
   CHECK_CUDA_DEVICE;
   /*if( ! checkCUDADevice() )
      return EXIT_FAILURE;*/
   return EXIT_SUCCESS;
}
