#include <iostream>
#include <fstream>
#include <stdlib.h>

#include "../np-interactions.h"
#include "../strengthsTensor.h"

#ifdef HAVE_CUDA
#include <cuda.h>
#endif

using std::cout;
using std::cerr;
using std::endl;
using std::flush;
using std::fstream;
using std::ios;



template<typename SelectionRules>
void PerformSU3Decomposition( const uint16_t *i1, const uint16_t *i2, const uint16_t *i3, const uint16_t* i4, const uint16_t *J, 
                              const double *V_pp, const double* V_nn, const double* V_pn, const int size,
                              int Nmax,
                              int valence_shell,
                              int nmax,
                              const string& sOutputFileName )
{
   SelectionRules IsIncluded;

   /****
    * Prepare the output file
    */
   std::ofstream outfile(sOutputFileName.c_str());
   if (!outfile)
   {
      std::cerr << "Unable to open output file '" << sOutputFileName << "'" << std::endl;
      exit(EXIT_FAILURE);
   }
   outfile.precision(10);

   /****
    * Prepare the strengths tensor
    */
   strengthsTensor< host > strTensor( MAX_LOGFACT );

   /****
    * (la, lb): lalb_indices[la*lb_max + lb] ---> <(na 0) la; (nb 0) lb || wf * *>
    * (ld, lc): lalb_indices[ld*lc_max + lc] ---> <(0 nd) ld; (0 nc) lc || wi * *>
    * (Lf, Li): lflf_indices[Lf*Li_max + Li] ---> {<wf * Lf; wi * Li || w0 * L0=0>_{*}, <wf * Lf; wi * Li || w0 * L0=1>_{*}, <wf * Lf; wi * Li || w0 * L0=2>_{*} }
    */
   vector< uint16_t > lalb_indices;
   vector< uint16_t > ldlc_indices; 
   vector< uint16_t > lfli_indices;
        
   /****
    * abf_cgSU3 -> <(na 0) * (nb 0) * || (lm mu)f *>
    * dci_cgSU3 -> <(0 nd) * (0 nc) * || (lm mu)i *>
    * fi0_cgSU3 -> <wf * wi * || w0 * L0={0,1,2}>_{*}
    */
   vector< double > abf_cgSU3; 
   vector< double > dci_cgSU3; 
   vector< double > fi0_cgSU3;  	
   abf_cgSU3.reserve(1024);
   dci_cgSU3.reserve(1024);
   fi0_cgSU3.reserve(1024);
   
   /****
    * Prepare SU3 labels
    */
   enum {kA = 0, kB = 1, kC = 2, kD = 3};
   SU3::LABELS ir[4] = { SU3::LABELS(0, 0), SU3::LABELS(0, 0), SU3::LABELS(0, 0), SU3::LABELS(0, 0) };

   /*****
    * Prepare intial and final IRREPS (irreducible representations)
    * InitIrreps: list of (lmi mui) irreps resulting from (0 nd) x (0 nc)
    * FinalIrreps: list of (lmf muf) irreps resulting from (na 0) x (nb 0)
    *
    * Reserve it to (na 0) x (nb 0) ---> max(na, nb) SU(3) irreps
    */
   SU3_VEC FinalIrreps;	
   SU3_VEC InitIrreps;			
   FinalIrreps.reserve(nmax + 1);
   InitIrreps.reserve(nmax + 1); 

   /****
    * Compute all combinations of {na, nb, nc, nd} HO shell quantum numbers
    * and iterate over them
    */
   vector<NANBNCND> ho_shells_combinations;
   GenerateAllCombinationsHOShells< ParityPreserving >( Nmax, valence_shell, nmax, ho_shells_combinations );
   for( int ishells = 0; ishells < 1 /*ho_shells_combinations.size()*/; ++ishells )
   {
      /****
       * Take one combination of (na, nb, nc, nd ) to process
       */
      
      /*ir[kA].lm = ho_shells_combinations[ishells][kA];
      ir[kB].lm = ho_shells_combinations[ishells][kB];
      ir[kC].mu = ho_shells_combinations[ishells][kC];
      ir[kD].mu = ho_shells_combinations[ishells][kD];*/

      ir[kA].lm=4;
      ir[kB].lm=5;
      ir[kC].mu=5;
      ir[kD].mu=4;
      int n1 = ir[kA].lm;
      int n2 = ir[kB].lm;
      int n3 = ir[kD].mu;
      int n4 = ir[kC].mu;
      
      /****
       * Check if the operator has this combination
       */
      if( !IsIncluded( n1, n2, n3, n4 ) )
         continue;
      
      std::cout << "Processing HO shell combination: " << ir[kA] << " " << ir[kB] << " " << ir[kC] << " " << ir[kD] << "\r" << std::flush;
      
      /****
       * Generate all combinations of la, lb, lc, ld, ja, jb, jc, jd and J with
       * for given na, nb, nc and nd. Each such combination is encoded into uint_16.
       * With each such combination, appropriate values of V_pp, V_nn and V_pn are stored
       * as well. In other words we generate set of matrix elements {<na* nb*|| {Vpp, Vnn, Vpn} || nc* nd*>} )
       */
      double* meV;
      uint16_t* labels;
      int me_phase;
      int number_me_triplets = size; //JCoupledPNME.GetMe(ho_shells_combinations[ishells], meV, labels, me_phase);
      if( number_me_triplets == 0 )
         continue;

      /****
       * Generate initial and final IRREPS w_i and w_f
       * InitIrrep: (0 nd) x (0 nc) -> rho=1{(lmi mui)}
       * FinalIrreps: (na 0) x (nb 0) -> rho=1{(lmf muf)}
       */
      InitIrreps.clear();
      FinalIrreps.clear();
      SU3::Couple( ir[kD], ir[kC], InitIrreps );
      SU3::Couple( ir[kA], ir[kB], FinalIrreps );
      

      /****
       * Iterate over final irreps i.e. w_f <--> (lmf muf)
       */
      for( int iif = 0; iif < FinalIrreps.size(); ++iif )
      {
          /****
           * Compute < (na 0) * (nb 0) * || w_f * * > and store
           * the result in the array abf_cgSU3. This array is later accessed
           * via indecis in the array lalb_indecis.
           */
          ComputeSU3CGs_Simple( ir[kA], ir[kB], FinalIrreps[iif], lalb_indices, abf_cgSU3 );

          /****
           * Iterate over intial irreps i.e. w_i <--> (lmi mui )
           */
	  for (int ii = 0; ii < InitIrreps.size(); ++ii)
	  {
             /****
              * Compute < (0 nd) * (0 nc) * || w_i * * > and store
              * the result in the array dci_cgSU3. This array is later accessed
              * via indecis in the array ldlc_indecis.
              */
	      ComputeSU3CGs_Simple( ir[kD], ir[kC], InitIrreps[ii], ldlc_indices, dci_cgSU3 );
             
             /****
              * Couple the intial and final irreps into ????
              * (lmf muf) x (lmi mui) -> rho_{0} (lm0 mu0)
              */
	     SU3_VEC Irreps0;
	     SU3::Couple( FinalIrreps[iif], InitIrreps[ii], Irreps0 );

             /****
              * Iterate over rho(lm0 mu0)
              */
	     for( int i0 = 0; i0 < Irreps0.size(); ++i0 )
	     {
	        std::cout << "Processing HO shell combination: " 
                          << ir[kA] << " " << ir[kB] << " " << ir[kC] << " " << ir[kD] 
			  << "\t" << (int) Irreps0[i0].rho << " " << (int) Irreps0[i0].lm << " " << (int) Irreps0[i0].mu << " \n" << std::flush;
                /****
                 * Check it the Irrep0 has tensorial character of the operator
                 */   
	        if( ! IsIncluded( Irreps0[ i0 ] ) )
	           continue;
                
                /****
                 * Compute < w_f * * w_i * * || w_0 * * >_{*} and store
                 * the result in the array fi0_cgSU3. This aray is later
                 * accessed via indecis in the array lfli_indecis.
                 */
	        ComputeSU3CGs(FinalIrreps[iif], InitIrreps[ii], Irreps0[i0], lfli_indices, fi0_cgSU3);

               
                /****
                 * The result for each combination of n_a, n_b, n_c, n_d, w_f, w_i and w_0
                 * is a set of numbers alpha depending on S_f, S_i, S_0, k_0, rho_0
                 * and {V_pp, V_nn, V_np}
                 *
                 * Scalar operator [i.e. J0=0] has L0==S0. Since we are dealing with
                 * a two-body interaction ==> S0 = 0, 1, or 2.
                 * S0=0 appears 2 times: Sf=0 x Si=0 --> S0=0 and Sf=1 x Si=1 --> S0=0
                 * ==> 2 x SU3::kmax(w0, L0=0) x rho0_max x 3
                 *
                 * S0=1 appears 3 times: Sf=1 x Si=0 --> S0=1; Sf=0 x Si=1 --> S0=0; Sf=1 x Si=1 ---> S0=1
                 * ==> 3 x SU3::kmax(w0, L0=1) x rho0_max x 3
                 *
                 * S0=2 appears 1 times: Sf=1 x Si=1 --> S0=2
                 * ==> 1 x SU3::kmax(w0, L0=1) x rho0_max x 3
                 *
                 * Two body interaciton has L0=S0 and  can be either 0, 1, or 2.
                 *
                 * Order of elements in results: results[iS, k0, rho0, type], where iS = 0 ... 6 [= number of Sf Si S0 quantum numbers for two-body interaction]
                 *	index = iS x k0_max x rho0_max x 3 + k0 x rho0_max x 3 + rho0 x 3 + type
                 *
                 * TODO: zpusob ukladani vysledku se musi zmenit, aby to bylo kompatibilni s GPU
                 */
                
                std::vector< double > results[6];
		results[0].resize( 3*SU3::kmax( Irreps0[i0], 0 )*Irreps0[i0].rho );
		results[1].resize( 3*SU3::kmax( Irreps0[i0], 0 )*Irreps0[i0].rho );
		results[2].resize( 3*SU3::kmax( Irreps0[i0], 1 )*Irreps0[i0].rho );
		results[3].resize( 3*SU3::kmax( Irreps0[i0], 1 )*Irreps0[i0].rho );
		results[4].resize( 3*SU3::kmax( Irreps0[i0], 1 )*Irreps0[i0].rho );
		results[5].resize( 3*SU3::kmax( Irreps0[i0], 2 )*Irreps0[i0].rho );
                //double *results = new double[ 6*3*3*Irreps0[i0].rho ];

                /****
                 * We will now sum over all la, lb, lc, ld, ja, jb, jc, jd and J.
                 * We will also do the rest of the computations (Wigner coefficients etc.).
                 */
                /*strTensor.compute( n1, n2, n3, n4,
                                   FinalIrreps[iif],
                                   InitIrreps[ii],
                                   Irreps0[i0],
                                   V_pp, V_nn, V_pn,
                                   i1, i2, i3, i4, J,
                                   number_me_triplets,
                                   &lalb_indices[0],
                                   &ldlc_indices[0],
                                   &lfli_indices[0],
                                   &abf_cgSU3[0],
                                   &dci_cgSU3[0],
                                   &fi0_cgSU3[0], 
                                   results );*/

                /****
                 * Write the results to the output file
                 */
	        for( int iS = 0; iS < 6; ++iS )
                {
                   /****
                    * If k0_max equals 0 there is no nozero alpha
                    */
		   int k0_max = SU3::kmax(Irreps0[i0], SfSiS0[iS][2]);
		   if( !k0_max )
		      continue;
                   
                   /****
                    * ???
                    */
                   if( std::count_if( results[iS].begin(), results[iS].end(), Negligible) == 3*k0_max*Irreps0[i0].rho )
		     continue;
                  
                   /****
                    * Write the combination n_a, n_b, n_c, n_d, w_f, w_i, w_0, ??????????
                    */ 
		   outfile << n1 << " " << n2 << " " << n3 << " " << n4;
		   outfile << "\t\t" << FinalIrreps[iif] << " " << 2*SfSiS0[iS][0] << "\t" << InitIrreps[ii] << " " << 2*SfSiS0[iS][1] << "\t" << Irreps0[i0];
		   outfile << " " << 2*SfSiS0[iS][2] << "\n";

                   for( int k0 = 0, j = 0; k0 < k0_max; k0++ )
		   {
		       for( int rho0 = 0; rho0 < Irreps0[i0].rho; ++rho0, j += 3)
		       {   
		     	   if( Negligible(results[iS][j] ) )
			      outfile << 0 << " ";
			   else
			      outfile << results[iS][j] << " ";
                           
                           if( Negligible( results[iS][j + 1] ) )
			      outfile << 0 << " ";
			   else
			      outfile << results[iS][j + 1] << " ";

			   if( Negligible(results[iS][j + 2] ) )
			      outfile << 0 << "\n";
			   else
			      outfile << results[iS][j + 2] << "\n";
			} // for( int rho0 = 0; rho0 < Irreps0[i0].rho; ++rho0, j += 3) 
		  } // for( int k0 = 0, j = 0; k0 < k0_max; k0++ )
	       } // for( int iS = 0; iS < 6; ++iS )
               delete[] results;
	    } // for( int i0 = 0; i0 < Irreps0.size(); ++i0 )
	 } // for (int ii = 0; ii < InitIrreps.size(); ++ii)
      } // for( int iif = 0; iif < FinalIrreps.size(); ++iif )
   } // for( int ishells = 0; ishells < ho_shells_combinations.size(); ++ishells )
   outfile.close();
}


int main( int argc, char* argv[] )
{
   if( argc < 3 )
   {
      cout << "Need an input file and number of lines." << endl;
      return EXIT_FAILURE;
   }
   const char* fileName = argv[ 1 ];
   cout << "Opening file " << fileName << "..." << endl;

   fstream inputFile;
   inputFile.open( fileName, ios::in );
   if( ! inputFile )
   {
      cerr << "Unable to open it. " << endl;
      return EXIT_FAILURE;
   }
   
   const int size = atoi( argv[ 2 ] );
   int na, nb, nc, nd, la, lb, lc, ld, ja, jb, jc, jd;
   uint16_t *i1 = new uint16_t[ size ];
   uint16_t *i2 = new uint16_t[ size ];
   uint16_t *i3 = new uint16_t[ size ];
   uint16_t *i4 = new uint16_t[ size ];
   uint16_t *J = new uint16_t[ size ];
   double *V_pp = new double[ size ];
   double *V_pn = new double[ size ];
   double *V_nn = new double[ size ];
   
   char str[ 1024 ];
   cout << "Idx. " << " \t na" << " \t nb" << " \t nc" << " \t nd"
                   << " \t la" << " \t lb" << " \t lc" << " \t ld" << " \t J"
                   << " \t ja" << " \t jb" << " \t jc" << " \t jd"
                   << " \t V_pp" << " \t V_nn" << " \t V_pn" << endl;
   for( int i = 0; i < size; i++ )
   {
      inputFile >> i1[ i ];
      inputFile >> i2[ i ];
      inputFile >> i3[ i ];
      inputFile >> i4[ i ];
      inputFile >> J[ i ];
      J[ i ] /= 2;
      inputFile >> V_pp[ i ];
      inputFile >> V_nn[ i ];
      inputFile >> V_pn[ i ];
      sps2Index::getNLJ( i1[ i ], na, la, ja ); 
      sps2Index::getNLJ( i2[ i ], nb, lb, jb ); 
      sps2Index::getNLJ( i3[ i ], nc, lc, jc ); 
      sps2Index::getNLJ( i4[ i ], nd, ld, jd );
       
      cout << i << " \t " << na << " \t " << nb << " \t " << nc << " \t " << nd 
                << " \t " << la << " \t " << lb << " \t " << lc << " \t " << ld << " \t " << J[ i ]
                << " \t " << ja << " \t " << jb << " \t " << jc << " \t " << jd
                << " \t " << V_pp[ i ]  << " \t " << V_nn[ i ] << " \t " << V_pn[ i ] << " \r  " << flush;
   }
   cout << endl;
   inputFile.close();

   int Nmax = 1;
   int valence_shell = 1;
   int nmax = Nmax + valence_shell;

   blocks_();
  
   string sFileSU3Decomposition( "outFile" );

   PerformSU3Decomposition<Cinteraction_sel_rules>( i1, i2, i3, i4, J, V_pp, V_nn, V_pn, size,
                                                    Nmax, valence_shell, nmax, sFileSU3Decomposition);
   delete[] i1;
   delete[] i2;
   delete[] i3;
   delete[] i4;
   delete[] J;
   delete[] V_pp;
   delete[] V_pn;
   delete[] V_nn;
}
