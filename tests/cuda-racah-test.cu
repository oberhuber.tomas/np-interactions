#include <cuda.h>
#include <iostream>
#include <stdio.h>
#include <cstdlib>

#include "../logFactTable.h"
#include "../racah.h"
#include "../get-time.h"

using namespace std;

const int maxLogFact( 128 );

__global__ void racahTestKernel( const int* a2,
                                 const int* b2,
                                 const int* e2,
                                 const int* d2,
                                 const int* c2,
                                 const int* f2,
                                 double* results,
                                 const int size,
                                 const double* globalLogFactTable,
                                 const int logFactTableSize )
{
   extern __shared__ double sharedLogFactTable[];
   logFactTable< cuda > lfTable( globalLogFactTable,
                                 sharedLogFactTable,
                                 logFactTableSize );
   racahCoefficients< cuda > racahCoefficients( &lfTable );
   const int gridSize = gridDim.x * blockDim.x;
   int element = blockIdx.x*blockDim.x + threadIdx.x;
   while( element < size )
   {
      //printf( "element = %d \n", element );
      results[ element ] = 
         racahCoefficients.getCoefficient( a2[ element ],
                                           b2[ element ],
                                           e2[ element ],
                                           d2[ element ],
                                           c2[ element ],
                                           f2[ element ] );
      element += gridSize; 
   }
}

int main( int argc, char* argv[] )
{
   const int setSize = 65536;
   const int size = 65536*32;
   
   cout << "Testing Racah coefficients..." << endl;
   
   logFactTable< host > logFactTable( maxLogFact );

   int* racah_a2 = new int[ size ];
   int* racah_b2 = new int[ size ];
   int* racah_e2 = new int[ size ];
   int* racah_d2 = new int[ size ];
   int* racah_c2 = new int[ size ];
   int* racah_f2 = new int[ size ];
 
   cout << "Generating input parameters ... " << endl; 
   int i( 0 );
   while( i < setSize && i < size ) 
   {
      racah_a2[ i ] = rand() % 12;
      racah_b2[ i ] = rand() % 12;
      racah_e2[ i ] = rand() % 12;
      racah_d2[ i ] = rand() % 12;
      racah_c2[ i ] = rand() % 12;
      racah_f2[ i ] = rand() % 12;
     
      if( isAllowed( racah_a2[ i ], racah_b2[ i ], racah_c2[ i ] ) &&
          isAllowed( racah_c2[ i ], racah_d2[ i ], racah_e2[ i ] ) &&
          isAllowed( racah_a2[ i ], racah_e2[ i ], racah_f2[ i ] ) &&
          isAllowed( racah_b2[ i ], racah_d2[ i ], racah_f2[ i ] ) )
      i++;
      cout << i << " / " << setSize << "  \r" << flush;
   }
   while( i < size )
   {
      racah_a2[ i ] = racah_a2[ i % setSize ];
      racah_b2[ i ] = racah_b2[ i % setSize ];
      racah_e2[ i ] = racah_e2[ i % setSize ];
      racah_d2[ i ] = racah_d2[ i % setSize ];
      racah_c2[ i ] = racah_c2[ i % setSize ];
      racah_f2[ i ] = racah_f2[ i % setSize ];
      i++;
   }
   cout << endl;

   cout << "Computing the Racah coefficients on the CPU ... " << endl;
   racahCoefficients< host > racahCoefficients( &logFactTable );
   double* racahResults = new double[ size ];
   double startHostTime = getTime();
   //for( int rep = 0; rep < 10; rep++ )
      for( int i = 0; i < size; i++ )      
      {
         racahResults[ i ] = racahCoefficients.getCoefficient( racah_a2[ i ], racah_b2[ i ], racah_e2[ i ], racah_d2[ i ], racah_c2[ i ], racah_f2[ i ] );
      }
   double endHostTime = getTime();

   int *racahCuda_a2, *racahCuda_b2, *racahCuda_e2, *racahCuda_d2, *racahCuda_c2, *racahCuda_f2;
   if( cudaMalloc( ( void** ) &racahCuda_a2, size*sizeof( int ) ) != cudaSuccess ||
       cudaMalloc( ( void** ) &racahCuda_b2, size*sizeof( int ) ) != cudaSuccess ||
       cudaMalloc( ( void** ) &racahCuda_e2, size*sizeof( int ) ) != cudaSuccess ||
       cudaMalloc( ( void** ) &racahCuda_d2, size*sizeof( int ) ) != cudaSuccess ||
       cudaMalloc( ( void** ) &racahCuda_c2, size*sizeof( int ) ) != cudaSuccess ||
       cudaMalloc( ( void** ) &racahCuda_f2, size*sizeof( int ) ) != cudaSuccess )
   {
      cerr << "Unable to allocate input coefficients on the GPU." << endl;
      return EXIT_FAILURE;
   }
   
   double* racahResultsCuda;
   cudaMalloc( ( void** ) &racahResultsCuda, size*sizeof( double ) );
   CHECK_CUDA_DEVICE;
   /*if( ! checkCUDADevice() )
   {
      cerr << "Unable to allocate logFactResultsCuda on GPU." << endl;
      return EXIT_FAILURE;
   }*/

   if( cudaMemcpy( racahCuda_a2, racah_a2, size*sizeof( int ), cudaMemcpyHostToDevice ) != cudaSuccess ||
       cudaMemcpy( racahCuda_b2, racah_b2, size*sizeof( int ), cudaMemcpyHostToDevice ) != cudaSuccess ||
       cudaMemcpy( racahCuda_e2, racah_e2, size*sizeof( int ), cudaMemcpyHostToDevice ) != cudaSuccess ||
       cudaMemcpy( racahCuda_d2, racah_d2, size*sizeof( int ), cudaMemcpyHostToDevice ) != cudaSuccess ||
       cudaMemcpy( racahCuda_c2, racah_c2, size*sizeof( int ), cudaMemcpyHostToDevice ) != cudaSuccess ||
       cudaMemcpy( racahCuda_f2, racah_f2, size*sizeof( int ), cudaMemcpyHostToDevice ) != cudaSuccess )
   {
      cerr << "Unable to copy the logFactParams on the GPU." << endl;
      return EXIT_FAILURE;
   }

   cout << "Computing the Racah coefficients on the GPU ... " << endl;
   dim3 gridSize, blockSize;
   blockSize.x = 256;
   gridSize.x = min( maxGridSize, size / blockSize.x + ( size % blockSize.x != 0 ) );
   const int sharedTableSize = maxLogFact*warpSize*sizeof( double );
   double startCudaTime = getTime();
   //for( int rep = 0; rep < 10; rep++ )
      racahTestKernel<<< gridSize, blockSize, sharedTableSize >>>( racahCuda_a2,
                                                                   racahCuda_b2,
                                                                   racahCuda_e2,
                                                                   racahCuda_d2,
                                                                   racahCuda_c2,
                                                                   racahCuda_f2,
                                                                   racahResultsCuda,
                                                                   size,
                                                                   logFactTable.getGPUTable(),
                                                                   maxLogFact );
   cudaThreadSynchronize();
   double endCudaTime = getTime();
   CHECK_CUDA_DEVICE;
   /*if( ! checkCUDADevice() )
   {
      cerr << "The kernel launch failed. " << endl;
      return EXIT_FAILURE;
   }*/
   
   double* aux = new double[ size ];
   cudaMemcpy( aux, racahResultsCuda, size*sizeof( double ), cudaMemcpyDeviceToHost );
   CHECK_CUDA_DEVICE;
   /*if( ! checkCUDADevice() )
   {
      cerr << "Unable to copy the result from the GPU to the host." << endl;
      return EXIT_FAILURE;
   }*/

   double cpuTime = endHostTime - startHostTime;
   double cudaTime = endCudaTime - startCudaTime;
   cout << "The CPU time is " << cpuTime << endl;
   cout << "The GPU time is " << cudaTime << endl;
   cout << "The speed-up is " << cpuTime / cudaTime << endl;

   for( int i = 0; i < size; i++ )
   {
      if( fabs( aux[ i ] - racahResults[ i ] ) > 1.0e-15 )
      {
         cerr << "The results differ by " << aux[ i ] - racahResults[ i ] << " at position " << i << "." << endl;
         return EXIT_FAILURE;
      }
   }

   delete[] racah_a2;
   delete[] racah_b2;
   delete[] racah_e2;
   delete[] racah_d2;
   delete[] racah_c2;
   delete[] racah_f2;
   delete[] racahResults;
   delete[] aux;
   cudaFree( racahCuda_a2 );
   cudaFree( racahCuda_b2 );
   cudaFree( racahCuda_e2 );
   cudaFree( racahCuda_d2 );
   cudaFree( racahCuda_c2 );
   cudaFree( racahCuda_f2 );
   cudaFree( racahResultsCuda );
   CHECK_CUDA_DEVICE;
   /*if( ! checkCUDADevice() )
      return EXIT_FAILURE;*/
   return EXIT_SUCCESS;
}
