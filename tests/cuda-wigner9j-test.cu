#include <cuda.h>
#include <iostream>
#include <stdio.h>
#include <cstdlib>

#include "../logFactTable.h"
#include "../wigner.h"
#include "../get-time.h"

using namespace std;

const int maxLogFact( 128 );

__global__ void wigner9jTestKernel( const int* a2,
                                    const int* b2,
                                    const int* e2,
                                    const int* d2,
                                    const int* c2,
                                    const int* f2,
                                    const int* g2,
                                    const int* h2,
                                    const int* j2,
                                    double* results,
                                    const int size,
                                    const double* globalLogFactTable,
                                    const int logFactTableSize )
{
   extern __shared__ double sharedLogFactTable[];
   logFactTable< cuda > lfTable( globalLogFactTable,
                                 sharedLogFactTable,
                                 logFactTableSize );
   wigner9jCoefficients< cuda > wigner9jCoefficients( &lfTable );
   const int gridSize = gridDim.x * blockDim.x;
   int element = blockIdx.x*blockDim.x + threadIdx.x;
   while( element < size )
   {
      //printf( "element = %d \n", element );
      results[ element ] = 
         wigner9jCoefficients.getCoefficient( a2[ element ],
                                              b2[ element ],
                                              e2[ element ],
                                              d2[ element ],
                                              c2[ element ],
                                              f2[ element ],
                                              g2[ element ],
                                              h2[ element ],
                                              j2[ element ] );
      element += gridSize; 
   }
}

int main( int argc, char* argv[] )
{
   const int setSize = 65536;
   const int size = 65536*32;
   
   cout << "Testing Wigner9j coefficients..." << endl;
   
   logFactTable< host > logFactTable( maxLogFact );

   int* wigner9j_a2 = new int[ size ];
   int* wigner9j_b2 = new int[ size ];
   int* wigner9j_e2 = new int[ size ];
   int* wigner9j_d2 = new int[ size ];
   int* wigner9j_c2 = new int[ size ];
   int* wigner9j_f2 = new int[ size ];
   int* wigner9j_g2 = new int[ size ];
   int* wigner9j_h2 = new int[ size ];
   int* wigner9j_j2 = new int[ size ];
 
   cout << "Generating input parameters ... " << endl; 
   int i( 0 );
   while( i < setSize && i < size ) 
   {
      wigner9j_a2[ i ] = rand() % 12;
      wigner9j_b2[ i ] = rand() % 12;
      wigner9j_e2[ i ] = rand() % 12;
      wigner9j_d2[ i ] = rand() % 12;
      wigner9j_c2[ i ] = rand() % 12;
      wigner9j_f2[ i ] = rand() % 12;
      wigner9j_g2[ i ] = rand() % 12;
      wigner9j_h2[ i ] = rand() % 12;
      wigner9j_j2[ i ] = rand() % 12;
     
      if( isAllowed( wigner9j_a2[ i ], wigner9j_b2[ i ], wigner9j_c2[ i ] ) &&
          isAllowed( wigner9j_d2[ i ], wigner9j_e2[ i ], wigner9j_f2[ i ] ) &&
          isAllowed( wigner9j_g2[ i ], wigner9j_h2[ i ], wigner9j_j2[ i ] ) &&
          isAllowed( wigner9j_a2[ i ], wigner9j_d2[ i ], wigner9j_g2[ i ] ) &&
          isAllowed( wigner9j_b2[ i ], wigner9j_e2[ i ], wigner9j_h2[ i ] ) &&
          isAllowed( wigner9j_c2[ i ], wigner9j_f2[ i ], wigner9j_j2[ i ] ) )
      i++;
      cout << i << " / " << setSize << "  \r" << flush;
   }
   while( i < size )
   {
      wigner9j_a2[ i ] = wigner9j_a2[ i % setSize ];
      wigner9j_b2[ i ] = wigner9j_b2[ i % setSize ];
      wigner9j_e2[ i ] = wigner9j_e2[ i % setSize ];
      wigner9j_d2[ i ] = wigner9j_d2[ i % setSize ];
      wigner9j_c2[ i ] = wigner9j_c2[ i % setSize ];
      wigner9j_f2[ i ] = wigner9j_f2[ i % setSize ];
      wigner9j_g2[ i ] = wigner9j_g2[ i % setSize ];
      wigner9j_h2[ i ] = wigner9j_h2[ i % setSize ];
      wigner9j_j2[ i ] = wigner9j_j2[ i % setSize ];
      i++;
   }
   cout << endl;

   cout << "Computing the Wigner9j coefficients on the CPU ... " << endl;
   wigner9jCoefficients< host > wigner9jCoefficients( &logFactTable );
   double* wigner9jResults = new double[ size ];
   double startHostTime = getTime();
   //for( int rep = 0; rep < 10; rep++ )
      for( int i = 0; i < size; i++ )      
      {
         wigner9jResults[ i ] = wigner9jCoefficients.getCoefficient( wigner9j_a2[ i ], wigner9j_b2[ i ], wigner9j_c2[ i ],
                                                                     wigner9j_d2[ i ], wigner9j_e2[ i ], wigner9j_f2[ i ],
                                                                     wigner9j_g2[ i ], wigner9j_h2[ i ], wigner9j_j2[ i ] );
      }
   double endHostTime = getTime();

   int *wigner9jCuda_a2, *wigner9jCuda_b2, *wigner9jCuda_c2,
       *wigner9jCuda_d2, *wigner9jCuda_e2, *wigner9jCuda_f2,
       *wigner9jCuda_g2, *wigner9jCuda_h2, *wigner9jCuda_j2;
   if( cudaMalloc( ( void** ) &wigner9jCuda_a2, size*sizeof( int ) ) != cudaSuccess ||
       cudaMalloc( ( void** ) &wigner9jCuda_b2, size*sizeof( int ) ) != cudaSuccess ||
       cudaMalloc( ( void** ) &wigner9jCuda_c2, size*sizeof( int ) ) != cudaSuccess ||
       cudaMalloc( ( void** ) &wigner9jCuda_d2, size*sizeof( int ) ) != cudaSuccess ||
       cudaMalloc( ( void** ) &wigner9jCuda_e2, size*sizeof( int ) ) != cudaSuccess ||
       cudaMalloc( ( void** ) &wigner9jCuda_f2, size*sizeof( int ) ) != cudaSuccess ||
       cudaMalloc( ( void** ) &wigner9jCuda_g2, size*sizeof( int ) ) != cudaSuccess ||
       cudaMalloc( ( void** ) &wigner9jCuda_h2, size*sizeof( int ) ) != cudaSuccess ||
       cudaMalloc( ( void** ) &wigner9jCuda_j2, size*sizeof( int ) ) != cudaSuccess )
   {
      cerr << "Unable to allocate input coefficients on the GPU." << endl;
      return EXIT_FAILURE;
   }
   
   double* wigner9jResultsCuda;
   cudaMalloc( ( void** ) &wigner9jResultsCuda, size*sizeof( double ) );
   CHECK_CUDA_DEVICE;
   /*if( ! checkCUDADevice() )
   {
      cerr << "Unable to allocate logFactResultsCuda on GPU." << endl;
      return EXIT_FAILURE;
   }*/

   if( cudaMemcpy( wigner9jCuda_a2, wigner9j_a2, size*sizeof( int ), cudaMemcpyHostToDevice ) != cudaSuccess ||
       cudaMemcpy( wigner9jCuda_b2, wigner9j_b2, size*sizeof( int ), cudaMemcpyHostToDevice ) != cudaSuccess ||
       cudaMemcpy( wigner9jCuda_c2, wigner9j_c2, size*sizeof( int ), cudaMemcpyHostToDevice ) != cudaSuccess ||
       cudaMemcpy( wigner9jCuda_d2, wigner9j_d2, size*sizeof( int ), cudaMemcpyHostToDevice ) != cudaSuccess ||
       cudaMemcpy( wigner9jCuda_e2, wigner9j_e2, size*sizeof( int ), cudaMemcpyHostToDevice ) != cudaSuccess ||
       cudaMemcpy( wigner9jCuda_f2, wigner9j_f2, size*sizeof( int ), cudaMemcpyHostToDevice ) != cudaSuccess ||
       cudaMemcpy( wigner9jCuda_g2, wigner9j_g2, size*sizeof( int ), cudaMemcpyHostToDevice ) != cudaSuccess ||
       cudaMemcpy( wigner9jCuda_h2, wigner9j_h2, size*sizeof( int ), cudaMemcpyHostToDevice ) != cudaSuccess ||
       cudaMemcpy( wigner9jCuda_j2, wigner9j_j2, size*sizeof( int ), cudaMemcpyHostToDevice ) != cudaSuccess )
   {
      cerr << "Unable to copy the logFactParams on the GPU." << endl;
      return EXIT_FAILURE;
   }

   cout << "Computing the Racah coefficients on the GPU ... " << endl;
   dim3 gridSize, blockSize;
   blockSize.x = 256;
   gridSize.x = min( maxGridSize, size / blockSize.x + ( size % blockSize.x != 0 ) );
   const int sharedTableSize = maxLogFact*warpSize*sizeof( double );
   double startCudaTime = getTime();
   //for( int rep = 0; rep < 10; rep++ )
      wigner9jTestKernel<<< gridSize, blockSize, sharedTableSize >>>( wigner9jCuda_a2,
                                                                      wigner9jCuda_b2,
                                                                      wigner9jCuda_c2,
                                                                      wigner9jCuda_d2,
                                                                      wigner9jCuda_e2,
                                                                      wigner9jCuda_f2,
                                                                      wigner9jCuda_g2,
                                                                      wigner9jCuda_h2,
                                                                      wigner9jCuda_j2,
                                                                      wigner9jResultsCuda,
                                                                      size,
                                                                      logFactTable.getGPUTable(),
                                                                      maxLogFact );
   cudaThreadSynchronize();
   double endCudaTime = getTime();
   CHECK_CUDA_DEVICE;
   /*if( ! checkCUDADevice() )
   {
      cerr << "The kernel launch failed. " << endl;
      return EXIT_FAILURE;
   }*/
   
   double* aux = new double[ size ];
   cudaMemcpy( aux, wigner9jResultsCuda, size*sizeof( double ), cudaMemcpyDeviceToHost );
   CHECK_CUDA_DEVICE;
   /*if( ! checkCUDADevice() )
   {
      cerr << "Unable to copy the result from the GPU to the host." << endl;
      return EXIT_FAILURE;
   }*/

   double cpuTime = endHostTime - startHostTime;
   double cudaTime = endCudaTime - startCudaTime;
   cout << "The CPU time is " << cpuTime << endl;
   cout << "The GPU time is " << cudaTime << endl;
   cout << "The speed-up is " << cpuTime / cudaTime << endl;

   for( int i = 0; i < size; i++ )
   {
      if( fabs( aux[ i ] - wigner9jResults[ i ] ) > 1.0e-15 )
      {
         cerr << "The results differ by " << aux[ i ] - wigner9jResults[ i ] << " at position " << i << "." << endl;
         return EXIT_FAILURE;
      }
   }

   delete[] wigner9j_a2;
   delete[] wigner9j_b2;
   delete[] wigner9j_e2;
   delete[] wigner9j_d2;
   delete[] wigner9j_c2;
   delete[] wigner9j_f2;
   delete[] wigner9j_g2;
   delete[] wigner9j_h2;
   delete[] wigner9j_j2;
   delete[] wigner9jResults;
   delete[] aux;
   cudaFree( wigner9jCuda_a2 );
   cudaFree( wigner9jCuda_b2 );
   cudaFree( wigner9jCuda_e2 );
   cudaFree( wigner9jCuda_d2 );
   cudaFree( wigner9jCuda_c2 );
   cudaFree( wigner9jCuda_f2 );
   cudaFree( wigner9jCuda_g2 );
   cudaFree( wigner9jCuda_h2 );
   cudaFree( wigner9jCuda_j2 );
   cudaFree( wigner9jResultsCuda );
   CHECK_CUDA_DEVICE;
   /*if( ! checkCUDADevice() )
      return EXIT_FAILURE;*/
   return EXIT_SUCCESS;
}
