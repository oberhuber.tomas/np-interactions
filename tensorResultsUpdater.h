#ifndef tensorResultsUpdaterH
#define tensorResultsUpdaterH

#include <iostream>
#include "functions.h"

template< typename Device, int blockSize = 1 >
class tensorResultsUpdater {};

template< int blockSize >
class tensorResultsUpdater< host, blockSize >
{
   public:

   void update( const double& local_result,
                const int iS,
                const double& Vpp,
                const double& Vnn,
                const double& Vpn,
                double* results,
                const int resultsISSize,
                const int jt  )
   {
      results[ iS*resultsISSize + jt ] += local_result*Vpp;
      results[ iS*resultsISSize + jt + 1 ] += local_result*Vnn;
      results[ iS*resultsISSize + jt + 2 ] += local_result*Vpn;
   };

};

#ifdef HAVE_CUDA
__device__ void reduceSum( const int localThreadIdx,
                           const int stride,
                           double* sharedData )
{
   __syncthreads();
   if( localThreadIdx < stride )
      sharedData[ localThreadIdx ] += sharedData[ localThreadIdx + stride ];
};
#endif

template< int blockSize >
class tensorResultsUpdater< cuda, blockSize >
{
#ifdef HAVE_CUDA 
   public:

   __device__
   tensorResultsUpdater( double* _auxArray,
                         const int _lastValidThread,
                         const int _blocksNum,
                         const int _blkIdx )
   : auxArray( _auxArray ),
     lastValidThread( _lastValidThread ),
     blocksNum( _blocksNum ),
     blkIdx( _blkIdx )
   {
   };
   
   __device__ void reduceNonaligned()
   {
      __syncthreads();
      int reducedPartSize = lastValidThread;
      //printf( "reducedPartSize = %d stride = %d \n", reducedPartSize, reducedPartSize/2 );
      for( int stride = reducedPartSize / 2; stride > 0; stride >>= 1 )
      {
         __syncthreads();
         if( threadIdx.x < stride &&
             threadIdx.x + stride < lastValidThread )
               auxArray[ threadIdx.x ] += 
                  auxArray[ threadIdx.x + stride ];
         if( 2 * stride < reducedPartSize &&
            threadIdx.x == 0 )
               auxArray[ 0 ] += auxArray[ reducedPartSize - 1 ];
         reducedPartSize = stride;
      }      
      __syncthreads(); // TODO: to by melo byt zbytecne
   };
   
   __device__ void reduceAligned()
   {
      __syncthreads();

      if( blockSize >= 1024 )
         reduceSum( threadIdx.x, 512, auxArray );
      if( blockSize >= 512 )
         reduceSum( threadIdx.x, 256, auxArray);
      if( blockSize >= 256 )
         reduceSum( threadIdx.x, 128, auxArray );
      if( blockSize >= 128 )
         reduceSum( threadIdx.x, 64, auxArray );
      __syncthreads();

      /***
       * This runs in one warp so it is synchronised implicitly.
       */      
      if( threadIdx.x < 32 )
      {
         volatile double* shared = auxArray;
         if( blockSize >= 64 )
            shared[ threadIdx.x ] += 
               shared[ threadIdx.x + 32 ];
         if( blockSize >= 32 )          
            shared[ threadIdx.x ] += 
               shared[ threadIdx.x + 16 ];
         if( blockSize >= 16 )
            shared[ threadIdx.x ] += 
               shared[ threadIdx.x + 8 ];
         if( blockSize >= 8 )
            shared[ threadIdx.x ] += 
               shared[ threadIdx.x + 4 ];
         if( blockSize >= 4 )
            shared[ threadIdx.x ] += 
               shared[ threadIdx.x + 2 ];
         if( blockSize >= 2 )
            shared[ threadIdx.x ] += 
               shared[ threadIdx.x + 1 ];
      }      
      __syncthreads(); // TODO: to by mela byt zbytecne
   };

   __device__ void update( const double& local_result,
                           const int iS,
                           const double& Vpp,
                           const double& Vnn,
                           const double& Vpn,
                           double* results,
                           const int resultsISSize,
                           const int jt  )
   {
      //if( lastValidThread < blockSize )
      {
         if( threadIdx.x < lastValidThread )
         {
            auxArray[ threadIdx.x ] = local_result*Vpp;
            //printf( "   TID %d -> %f \n", threadIdx.x,  local_result*Vpp );
         }
         __syncthreads();
         reduceNonaligned();
         if( threadIdx.x == 0 )
         {
            results[ ( iS*resultsISSize + jt )*blocksNum + blkIdx ] += auxArray[ 0 ];
            //if( iS*resultsISSize + jt > 30 )
            //printf( "   Sum is: %f result is %f ( idx = %d ) \n", auxArray[ 0 ],results[ ( iS*resultsISSize + jt )*blocksNum + blkIdx ],iS*resultsISSize + jt );
         }
         __syncthreads();
         
         if( threadIdx.x < lastValidThread )
            auxArray[ threadIdx.x ] = local_result*Vnn;
         reduceNonaligned();
         if( threadIdx.x == 0 )
            results[ ( iS*resultsISSize + jt + 1 )*blocksNum + blkIdx ] += auxArray[ 0 ];

         __syncthreads();
         
         if( threadIdx.x < lastValidThread )
            auxArray[ threadIdx.x ] = local_result*Vpn;
         reduceNonaligned();
         if( threadIdx.x == 0 )
            results[ ( iS*resultsISSize + jt + 2 )*blocksNum + blkIdx ] += auxArray[ 0 ];
         __syncthreads();
      }
      /*else
      {
         auxArray[ threadIdx.x ] = phaseSU2[iS]*su3cg_sum*Vpp;
         reduceAligned();
         if( threadIdx.x == 0 )
            results[ ( iS*resultsISSize + jt )*blocksNum + blkIdx ] += auxArray[ 0 ];
         
         auxArray[ threadIdx.x ] = phaseSU2[iS]*su3cg_sum*Vnn;
         reduceAligned();
         if( threadIdx.x == 0 )
            results[ ( iS*resultsISSize + jt + 1 )*blocksNum + blkIdx ] += auxArray[ 0 ];

         auxArray[ threadIdx.x ] = phaseSU2[iS]*su3cg_sum*Vpn;
         reduceAligned();
         if( threadIdx.x == 0 )
            results[ ( iS*resultsISSize + jt + 2 )*blocksNum + blkIdx ] += auxArray[ 0 ];
      }*/
   };

   double* auxArray;

   const int lastValidThread, blocksNum, blkIdx;
#endif
};

#endif
