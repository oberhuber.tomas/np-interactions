#ifndef get_timeH
#define get_timeH

#include <sys/time.h>

inline double getTime()
{
   struct timeval tp;
   gettimeofday( &tp, NULL );
   return ( double ) tp. tv_sec + 1.0e-6 * ( double ) tp. tv_usec;   
}

#endif
