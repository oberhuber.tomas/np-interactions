#ifndef nanbncndH
#define nanbncndH

#include <stdint.h>
#include "CTuple.h"

typedef CTuple< uint8_t, 4> NANBNCND;
enum {kNA=0, kNB=1, kNC=2, kND=3};

#endif
