#ifndef sps2IndexH
#define sps2IndexH

#include <math.h>
#include "device.h"

#ifdef HAVE_CUDA
#include <cuda.h>
#endif

/****
 * SPS = single particle state 
 * Transformation between quantum numbers n,l,j of the harmonic oscilator and single integer encoding.
 */
class sps2Index
{
   public:

#ifdef HAVE_CUDA
   __device__ __host__
#endif
   static inline void getNLJ( const int index,
                              int& n,
                              int& l,
                              int& j )
   {
      n = (-1 + sqrt( 1.0 + 8.0 * ( index - 1 ) ) )/2;
      int i = ( index-1 ) - n*( n+1 )/2;
      if( n%2 )
      {
         l = 1 + 2*( i/2 );
         j = (i % 2 ) ? ( 2*l + 1 ) : ( 2*l - 1 );
      }
      else
      {
         l = 2*( ( i+1 )/2 );
         j = ( i % 2 ) ? 2*l - 1: 2*l + 1;
      }
   };

#ifdef HAVE_CUDA
   __device__ __host__
#endif
   static inline int getIndex( const int n,
                               const int l,
                               const int j )
   // ASSUMPTION: l == l ... usually l == 2*l, but this method expects l
   {
      int index = n*(n + 1)/2 + 1;
      if( n%2 )
         index += 2*(l/2);
      else
      {
         if( l == 0 ) 
         {
            return index;
         }
         else
         {
            index += ( 1 + 2*( ( l/2 ) - 1 ) );
         }
      }
      return ( j == 2*l - 1 ) ? index : index + 1;
   };
};

#endif
