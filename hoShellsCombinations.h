#include "nanbncnd.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <stdint.h>
#include <assert.h>

using std::vector;
using std::cout;
using std::endl;
using std::string;

#ifndef HOSHELLSCOMBINATIONS_H
#define HOSHELLSCOMBINATIONS_H

template< typename SelectionRules >
void GenerateAllCombinationsHOShells( const int Nmax,
                                      const int valence_shell,
                                      const int maxShell,
                                      vector< NANBNCND >& ho_shells_combinations )
{
   SelectionRules IsIncluded;
   ho_shells_combinations.resize( 0 );

   NANBNCND na_nb_nc_nd;
   for( int na = 0; na <= maxShell; ++na )
   {
      for( int nb = 0; nb <= maxShell; ++nb )
      {
         int adxad_Nhw = 0;
         if( na > valence_shell )
         {
            adxad_Nhw += (na - valence_shell);
         }
         if( nb > valence_shell )
         {
            adxad_Nhw += (nb - valence_shell);
         }
         if( adxad_Nhw > Nmax )
         {
            continue;
         }
         na_nb_nc_nd[kNA] = na;
         na_nb_nc_nd[kNB] = nb;

         for( int nc = 0; nc <= maxShell; ++nc )
         {
            for( int nd = 0; nd <= maxShell; ++nd )
            {
               int taxta_Nhw = 0;
               if( nc > valence_shell )
               {
                  taxta_Nhw += (nc - valence_shell);
               }
               if( nd > valence_shell )
               {
                  taxta_Nhw += (nd - valence_shell);
               }
               if( taxta_Nhw > Nmax )
               {
                  continue;
               }
               na_nb_nc_nd[kNC] = nc;
               na_nb_nc_nd[kND] = nd;

               /****
                * Check if operator has this combination of HO shells
                */
               if( IsIncluded( na, nb, nd, nc ) )
               {
                  ho_shells_combinations.push_back(na_nb_nc_nd);	//	in such case we do not need ???????
	       }
	    }
	 }
      }
   }

   /****
    * Shrink vector to its proper size to save some memory.
    */
   vector< NANBNCND >( ho_shells_combinations.begin(),
                       ho_shells_combinations.end() ).swap( ho_shells_combinations );
}

#endif
