#ifndef wignerH
#define wignerH

#include "racah.h"
#include "logDelta2.h"

template< typename Device >
class wigner6jCoefficients
{
   public:

#ifdef HAVE_CUDA
   __device__ __host__
#endif     
   wigner6jCoefficients( const logFactTable< Device >* _lfTable )
   : lfTable( _lfTable ),
     lgDelta2( _lfTable )
   {
   };
 
#ifdef HAVE_CUDA
   __device__ __host__
#endif     
   double getCoefficient( const int a2, const int b2, const int c2,
                          const int d2, const int e2, const int f2 ) const
   {
       double result( 0.0 ), numerator( 0.0 ), denominator( 0.0 );

       if( isAllowed( a2, b2, c2 ) && 
           isAllowed( c2, d2, e2 ) &&
           isAllowed( a2, e2, f2 ) &&
           isAllowed( b2, d2, f2 ) )
       {
           int zmin, zmax;
           ///register int abc, cde, aef, bdf, abde, acdf, bcef;
           int abc, cde, aef, bdf, abde, acdf, bcef;

           zmin = max( max( max( abc = ( a2 + b2 + c2 ) / 2,
                                 cde = ( c2 + d2 + e2 ) / 2 ),
                            aef = ( a2 + e2 + f2 ) / 2 ),
                       bdf = ( b2 + d2 + f2 ) / 2 );
           zmax = min( min( abde = ( a2 + b2 + d2 + e2 ) / 2,
                            acdf = ( a2 + c2 + d2 + f2 ) / 2 ),
                       bcef = ( b2 + c2 + e2 + f2 ) / 2 );

           numerator = 0.5 * ( lgDelta2.getLogDelta2( a2, b2, c2 ) +
                               lgDelta2.getLogDelta2( c2, d2, e2 ) +
                               lgDelta2.getLogDelta2( a2, e2, f2 ) +
                               lgDelta2.getLogDelta2( b2, d2, f2 ) );
            
           //printf( "           Wigner9j: numerator = %f\n", numerator );
           for( int z = zmin; z <= zmax; ++z )
           {
               denominator = lfTable->getLogFact( z + 1 ) -
                             ( lfTable->getLogFact( z - abc ) +
                               lfTable->getLogFact( z - cde ) +
                               lfTable->getLogFact( z - aef ) +
                               lfTable->getLogFact( z - bdf ) +
                               lfTable->getLogFact( abde - z ) +
                               lfTable->getLogFact( acdf - z ) +
                               lfTable->getLogFact( bcef - z ) );
               //printf( "           Wigner9j: denominator = %f\n", denominator );
               //printf( "isOdd( %d ) = %d \n", z, isOdd( z ) );
               if( isOdd( z ) )
                   result -= exp( numerator + denominator );
               else
                   result += exp( numerator + denominator );
           }
       }
       else
          printf( "           Wigner9j: NOT_ALLOWED \n" );
       return result;
   };

   protected:

   const logFactTable< Device >* lfTable;

   const logDelta2< Device > lgDelta2;
};


template< typename Device >
class wigner9jCoefficients
{
   public:

#ifdef HAVE_CUDA
   __device__ __host__
#endif
   wigner9jCoefficients( const logFactTable< Device >* _lfTable )
   : lfTable( _lfTable )
   {
   };

#ifdef HAVE_CUDA
   __device__ __host__
#endif
   double getCoefficient( const int a2, const int b2, const int c2,
                          const int d2, const int e2, const int f2,
                          const int g2, const int h2, const int j2 ) const
   {
      racahCoefficients< Device > racahCoefficients( lfTable );
      double result( 0.0 );
      if( isAllowed( a2, b2, c2 ) &&
          isAllowed( d2, e2, f2 ) &&
          isAllowed( g2, h2, j2 ) &&
          isAllowed( a2, d2, g2 ) &&
          isAllowed( b2, e2, h2 ) &&
          isAllowed( c2, f2, j2 ) )
      {
          int zmin, zmax;
          zmin = max( max( abs( a2 - j2 ), abs( b2 - f2 ) ), abs( d2 - h2 ) );
          zmax = min( min(      a2 + j2  ,      b2 + f2   ),      d2 + h2   );

          for( int z2 = zmin; z2 <= zmax; z2 += 2 )
          {
              result += double (z2 + 1) * racahCoefficients.getCoefficient( a2, j2, d2, h2, z2, g2 )
                                        * racahCoefficients.getCoefficient( a2, j2, b2, f2, z2, c2 )
                                        * racahCoefficients.getCoefficient( b2, f2, h2, d2, z2, e2 );
          }
      }
      return result;
  };
   
  protected:

  const logFactTable< Device >* lfTable;
};


#endif
