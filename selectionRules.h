#ifndef rulesH
#define rulesH

#include "su2.h"
#include "su3.h"

struct CNoSelectionRules
{
   inline bool operator() (int n1, int n2, int n3, int n4) const
   {
      return true;
   }

   inline bool operator() (const SU2::LABEL& Spin) const 
   {
      return true;
   }

   inline bool operator() (const SU3::LABELS& su3ir) const 
   {
      return true;
   }
};


struct ParityPreserving
{
   inline bool operator() (int n1, int n2, int n3, int n4) const
   {
      /****
       * Return true if parity is conserved.
       */
      return ((MINUSto(n1)*MINUSto(n2)) == (MINUSto(n3)*MINUSto(n4)));
   }
};


struct Cinteraction_sel_rules
{
   inline bool operator() (int n1, int n2, int n3, int n4) const
   {
      /****
       * Return true if parity is conserved.
       */
      return ((MINUSto(n1)*MINUSto(n2)) == (MINUSto(n3)*MINUSto(n4)));
   }

   inline bool operator() (const SU2::LABEL& Spin) const 
   {
      return true;
   }

   inline bool operator() (const SU3::LABELS& su3ir) const 
   {
      return ((su3ir.lm + su3ir.mu) % 2) == 0;
   }
};


#endif
