#ifndef unH
#define unH

class UN
{
   public:

      /****
       * New naming is better. One can use UN::SU3 and UN::SU3xSU2 which are different from ordinary SU3 and SU3xSU2
       * due to the outer multiplicity mult arising from reduction U(N)>SU(3)xSU(2).
       */
   
      struct SU3
      {
         typedef unsigned short mult_type; // and I reserve mult = 0xFFFF for vacuum
         typedef unsigned char mu_type;
	 typedef unsigned char lm_type;

         // unsigned long mult; // unsigned long is too much, e.g. 64Ge Nmax=10 ==> max(mult) == 567 ==> unsigned short just fine
	 
         mult_type mult; // 64Ge Nmax=14 ==> max(mult) == 1493 ==> unsigned short will be just fine
	 lm_type lm;
	 mu_type mu;

	 SU3( const mult_type mult_, const lm_type lm_, const mu_type mu_)
         : mult(mult_), lm(lm_), mu(mu_) {}

         // SU3(const unsigned char lm_, const unsigned char mu_): mult(1), lm(lm_), mu(mu_) {} // implicitly set mult=1
	
         SU3()
         : mult(+1), lm(0), mu(0) {} 
	
         unsigned long C2() const
         { 
            return (lm*lm + lm*mu + mu*mu + 3*(lm + mu)); 
         }
         
         bool operator==( const SU3& unsu3 ) const
         {
            return (unsu3.mult == mult && unsu3.lm == lm && unsu3.mu == mu);
         }
         
         // inline bool operator< (const SU3MULT& rhs) const { return ( (lm < rhs.lm) || ((lm == rhs.lm && mu < rhs.mu)) ); }
         // inline bool operator< (const SU3MULT& rhs) const { return (C2() < rhs.C2()); }
      };	
};


#endif
