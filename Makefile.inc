# Uncomment the following line to enable CUDA
WITH_MPI = yes
WITH_CUDA = yes
#BLUE_WATERS = yes
CUDA_ARCH = sm_21
#CUDA_ARCH = sm_60

ifdef WITH_CUDA
   TARGET = np-interactions-cuda
else
   TARGET = np-interactions
endif

INSTALL_DIR = ${HOME}/.local

CXX = g++
CXX_FLAGS = -O3 -g -std=c++11 -fopenmp
LD_FLAGS = -L/usr/lib/x86_64-linux-gnu -lgfortran -lgomp

ifdef WITH_MPI
   CXX_FLAGS += -DHAVE_MPI
   
   ifdef BLUE_WATERS
      CXX =  CC
   else
      CXX_FLAGS += `mpic++ --showme:compile`
      LD_FLAGS += `mpic++ --showme:link`
   endif
endif

ifdef WITH_CUDA
   ifdef BLUE_WATERS
      CUDA_ARCH = sm_35
      CXX_FLAGS += ${CRAY_CUDATOOLKIT_INCLUDE_OPTS}
      LD_FLAGS += ${CRAY_CUDATOOLKIT_POST_LINK_OPTS}
   endif
   CUDA_CXX = nvcc
   CUDA_CXX_FLAGS = -DNDEBUG -O3 -DHAVE_CUDA -arch ${CUDA_ARCH} -Xcompiler -fopenmp --default-stream per-thread -ccbin=${CXX} -DHAVE_MPI -I/usr/lib/openmpi/include -I/usr/lib/openmpi/include/openmpi
   CUDA_LD_FLAGS = $(LD_FLAGS) -L/usr/local/cuda/lib64 -lcudart -lgomp
endif

FORTRAN = gfortran
FORTRAN_FLAGS = -m64 -frecursive -DSU3DBL -pg -g
