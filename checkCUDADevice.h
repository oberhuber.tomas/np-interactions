#ifndef checkCUDADeviceH
#define checkCUDADeviceH

#ifdef HAVE_CUDA

#define CHECK_CUDA_DEVICE checkCUDADevice( cudaGetLastError(), __FILE__, __LINE__ );

bool checkCUDADevice( cudaError error, const char* file, int line );

#endif

#endif
