#ifndef jCoupledProtonNeutron2BMeHermitianSingleOperatorH
#define jCoupledProtonNeutron2BMeHermitianSingleOperatorH

#include "nanbncnd.h"
#include "strengthsTensor.h"
#include "computeSU3CGs.h"
#include "matrixElement.h"
#include "su3xsu2.h"
#include "timer.h"
#include "config.h"
#include "get-time.h"
#include "transformationResult.h"
#include "StrengthsTensorDataBuffer.h"
#include <map>
#include <vector>
#include <list>
#include <string>
#include <iostream>
#include <omp.h>
#define SQRT sqrt
#ifdef HAVE_MPI
#include <mpi.h>
#endif
#include <utmpx.h>

using std::map;
using std::vector;
using std::string;
using std::list;
using std::cout;
using std::endl;

const int minGPUJobSize( 200 );

// Nvidia K20
/*const size_t MAX_UINT8_DATA_SIZE( 1<<20 );
const size_t MAX_UINT16_DATA_SIZE( 1<<20 );
const size_t MAX_INT_DATA_SIZE( 1<<20 );
const size_t MAX_DOUBLE_DATA_SIZE( 1<<23 );
const size_t MAX_DOUBLE_PTR_DATA_SIZE( 1<<16 );
const size_t MAX_RESULTS_DATA_SIZE( 1<<22 );
const size_t MAX_TENSORS_DATA_SIZE( 1<<16 );*/



const size_t MAX_UINT8_DATA_SIZE( 1<<14 );
const size_t MAX_UINT16_DATA_SIZE( 1<<16 );
const size_t MAX_INT_DATA_SIZE( 1<<14 );
const size_t MAX_DOUBLE_DATA_SIZE( 1<<16 );
const size_t MAX_DOUBLE_PTR_DATA_SIZE( 1<<10 );
const size_t MAX_RESULTS_DATA_SIZE( 1<<16 );
const size_t MAX_TENSORS_DATA_SIZE( 1<<10 );


class JCoupledProtonNeutron2BMe_Hermitian_SingleOperator
{
   public:
          
      typedef map< NANBNCND, matrixElement > INTERACTION_DATA;

      INTERACTION_DATA interaction_data_;

      vector< matrixElement >  interactionData;
      
      JCoupledProtonNeutron2BMe_Hermitian_SingleOperator();
      
      JCoupledProtonNeutron2BMe_Hermitian_SingleOperator( const std::string& sInteractionFileName, vector<NANBNCND>& ho_shells_combinations );

      void SwitchHOConvention();

      /****
       * This function multiplies all matrix elements by a factor
       * provided by the first line of equation (1) in devel/docs/Recoupling/Recoupling.pdf
       */
      void MultiplyByCommonFactor();
      
      //matrixElement GetMe( const NANBNCND& nanbncnd );

      void Show();

      inline size_t GetNumberOfOperators() const {return 1;}


      void SaveOnDisk( std::vector< transformationResult >& results,
                       std::ofstream& outfile );

      template< typename SelectionRules >
      void PerformSU3Decomposition( int Nmax,
                                    int valence_shell,
                                    int nmax,
                                    const string& sOutputFileName );

   protected:
      
      void checkIndices( const matrixElement& me_data,
                         const vector< uint16_t >& lalb_indices,
                         const vector< uint16_t >& ldlc_indices );

      enum {kA = 0, kB = 1, kC = 2, kD = 3};
      

#ifdef HAVE_CUDA
      strengthsTensor< cuda > cudaStrengthsTensor;
#endif
      strengthsTensor< host > hostStrengthsTensor;
      
      template< typename SelectionRules >
      bool processMatrixElements( const matrixElement& me_data,
                                 const int nmax,
                                 const SelectionRules& isIncluded,
                                 transformationResult& result,
                                 StrengthsTensorDataBuffer& tensorDataBuffer );

      void setCudaDevice();

#ifdef HAVE_MPI
      void MPI_Bcast_interactionData( int root, MPI_Comm comm );
#endif      
      template< typename SelectionRules >
      void performSU3DecompositionSequential( int Nmax,           // ???????????????????????????????
                                              int valence_shell,  // ????????????????????
                                              int nmax,
                                              vector< transformationResult >& results );

      bool haveCudaDevice;

};

JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::JCoupledProtonNeutron2BMe_Hermitian_SingleOperator()
: hostStrengthsTensor( MAX_LOGFACT )
#ifdef HAVE_CUDA
  , cudaStrengthsTensor( MAX_LOGFACT )
#endif            
{   
   setCudaDevice();
};

JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::JCoupledProtonNeutron2BMe_Hermitian_SingleOperator( const std::string& sInteractionFileName, vector<NANBNCND>& ho_shells_combinations )
: hostStrengthsTensor( MAX_LOGFACT )
#ifdef HAVE_CUDA
  , cudaStrengthsTensor( MAX_LOGFACT )
#endif            
{
   int my_rank( 0 ), nprocs( 1 );
#ifdef HAVE_MPI
   MPI_Comm_size( MPI_COMM_WORLD, &nprocs );
   MPI_Comm_rank( MPI_COMM_WORLD, &my_rank );
   if( my_rank != 0 )
      return;
#endif

   setCudaDevice();

   int32_t n_elems, rank_destination( 0 );
   int na, nb, nc, nd;
   NANBNCND nanbncnd;

   std::ifstream file( sInteractionFileName.c_str(), std::ifstream::binary );
   if (!file)
   {
      std::cout << "Could not open file '" << sInteractionFileName << "'!" << std::endl;
      exit( EXIT_FAILURE );
   }
   int numberOfElements( 0 );

   matrixElement me;
   while( true )
   {
      if( ! me.read( file ) )
         break; 
     
      nanbncnd[kNA] = me.n[ 0 ];
      nanbncnd[kNB] = me.n[ 1 ];
      nanbncnd[kNC] = me.n[ 2 ];
      nanbncnd[kND] = me.n[ 3 ];

      //cout << "Reading HO combination " << me.n[ 0 ] << " " << me.n[ 1 ] << " " << me.n[ 2 ] << " " << me.n[ 3 ] << "                       \r" << std::endl << flush;
                           
      int position = std::distance( ho_shells_combinations.begin(),
                                    std::find( ho_shells_combinations.begin(),
                                               ho_shells_combinations.end(),
                                               nanbncnd ) );
      if( position == ho_shells_combinations.size() ) // ==> {na, nb, nc, nd} is not between HO combinations to be processes
         continue;

      INTERACTION_DATA::iterator it = interaction_data_.find( nanbncnd );
      if( it == interaction_data_.end() )
      {
         interaction_data_.insert( std::make_pair( nanbncnd, me ) );
         numberOfElements++;
      }
      else
      {
         /****
          * This should never happen
          */
         std::cerr << "The same set of {na, nb, nc, nd}={" << na << ", " << nb << ", " << nc << ", " << nc << "} occurs multiple times in the input file '" << sInteractionFileName << "' !!!" << std::endl;
         exit( EXIT_FAILURE );
      }
   }
   interactionData.resize( numberOfElements );
   int i( 0 );
   INTERACTION_DATA::iterator it = interaction_data_.begin();
   while( it != interaction_data_.end() )
      this->interactionData[ i++ ] = it++->second;   
}

void JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::MultiplyByCommonFactor()
{
   /*INTERACTION_DATA::iterator it = interaction_data_.begin();
   while( it != interaction_data_.end() )
      ( it++ )->second.multiplyByCommonFactor();*/
   for( int i = 0; i < interactionData.size(); i++ )
      interactionData[ i ].multiplyByCommonFactor();
}

void JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::SwitchHOConvention()
{
   /*INTERACTION_DATA::iterator it = interaction_data_.begin();
   while( it != interaction_data_.end() )
      ( it++ )->second.switchHOConvention();*/
   for( int i = 0; i < interactionData.size(); i++ )
      interactionData[ i ].switchHOConvention();
}

void JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::Show()
{
   cout << "na   nb   nc   nd" << endl;
   INTERACTION_DATA::iterator it = interaction_data_.begin();
   while( it != interaction_data_.end() )
   {
      NANBNCND nanbncnd = it->first;
      cout << ( int ) nanbncnd[ kNA ] << "    " 
           << ( int ) nanbncnd[ kNB ] << "    "
           << ( int ) nanbncnd[ kNC ] << "    "
           << ( int ) nanbncnd[ kND ] << endl;
      it->second.print( cout );
      it++;
   }
}


void
JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::
checkIndices( const matrixElement& me_data,
              const vector< uint16_t >& lalb_indices,
              const vector< uint16_t >& ldlc_indices )
{
   for( int i = 0; i < me_data.size; i++ )
   {
      int na, nb, nc, nd, la, lb, lc, ld, jja, jjb, jjc, jjd;
      sps2Index::getNLJ( me_data.i1[i], na, la, jja);
      sps2Index::getNLJ( me_data.i2[i], nb, lb, jjb);
      sps2Index::getNLJ( me_data.i3[i], nc, lc, jjc);
      sps2Index::getNLJ( me_data.i4[i], nd, ld, jjd);
      assert( la*(nb+1) + lb < lalb_indices.size() );
      assert( ld*(nc+1) + lc < ldlc_indices.size() );
   }
}

template< typename SelectionRules >
bool
JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::
processMatrixElements( const matrixElement& me_data,
                      const int nmax,                                                                
                      const SelectionRules& isIncluded,
                      transformationResult& result,
                      StrengthsTensorDataBuffer& tensorDataBuffer )
{
   SU3::LABELS ir[4] = {SU3::LABELS(0, 0), SU3::LABELS(0, 0), SU3::LABELS(0, 0), SU3::LABELS(0, 0)};

   ir[kA].lm = me_data.n[ 0 ];
   ir[kB].lm = me_data.n[ 1 ];
   ir[kC].mu = me_data.n[ 2 ];
   ir[kD].mu = me_data.n[ 3 ];
   //std::cout << ir[kA] << " " << ir[kB] << " " << ir[kC] << " " << ir[kD] << "\r" << std::flush;

   SU3_VEC FinalIrreps;	            // list of (lmf muf) irreps resulting from (na 0) x (nb 0)
   SU3_VEC InitIrreps;              // list of (lmi mui) irreps resulting from (0 nd) x (0 nc)
   FinalIrreps.reserve( nmax + 1 ); // (na 0) x (nb 0) ---> max(na, nb) SU(3) irreps
   InitIrreps.reserve( nmax + 1 );   
   FinalIrreps.clear();
   InitIrreps.clear();
   SU3::Couple( ir[kA], ir[kB], FinalIrreps );	//	(na 0) x (nb 0) -> rho=1{(lmf muf)}
   SU3::Couple( ir[kD], ir[kC], InitIrreps );	//	(0 nd) x (0 nc) -> rho=1{(lmi mui)}	    
   

// jobsize = (pocet maticovych elementu) * {(lmf muf)}.size * {(lmi mui)}.size
   const int jobSize = me_data.size * FinalIrreps.size() * InitIrreps.size();
   vector< uint16_t > lalb_indices; // (la, lb): lalb_indices[la*lb_max + lb] ---> <(na 0) la; (nb 0) lb || wf * *>
   vector< uint16_t > ldlc_indices; // (ld, lc): lalb_indices[ld*lc_max + lc] ---> <(0 nd) ld; (0 nc) lc || wi * *>
   vector< uint16_t > lfli_indices; // (Lf, Li): lflf_indices[Lf*Li_max + Li] ---> {<wf * Lf; wi * Li || w0 * L0=0>_{*}, <wf * Lf; wi * Li || w0 * L0=1>_{*}, <wf * Lf; wi * Li || w0 * L0=2>_{*} }
   vector< double > abf_cgSU3;      // <(na 0) * (nb 0) * || (lm mu)f *>
   vector< double > dci_cgSU3;      // <(0 nd) * (0 nc) * || (lm mu)i *>
   vector< double > fi0_cgSU3;      // <wf * wi * || w0 * L0={0,1,2}>_{*}
   abf_cgSU3.reserve( 1024 );
   dci_cgSU3.reserve( 1024 );
   fi0_cgSU3.reserve( 1024 );

   
   std::vector< std::vector< std::vector<  std::vector< double > > > > results;
   // Irreps0_vec[ (lmf muf) ][ (lmi mui) ]  --->  {rho0max(lm0 mu0)}
   std::vector< std::vector< SU3_VEC > >Irreps0_vec;
   // rho0_max_vec[ (lmf muf) ][ (lmi mui) ]  --->  {rho0max}
   std::vector< std::vector< std::vector< int > > >  rho0_max_vec;
   // rho0_max_vec[ (lmf muf) ][ (lmi mui) ]  --->  {3*2*rho0max}
   std::vector< std::vector< std::vector< int > > >  resultsISSize_vec;
   std::vector< strengthsTensorData > tensorData_list;
   std::vector< int > rho0_max_list, resultsISSize_list;
   std::vector< double* > results_list;   
   Irreps0_vec.resize( FinalIrreps.size() );
   rho0_max_vec.resize( FinalIrreps.size() );
   resultsISSize_vec.resize( FinalIrreps.size() );
   results.resize( FinalIrreps.size() );
   

#ifdef HAVE_CUDA
   strengthsTensorData deviceStrengthsTensorData( me_data.size, SfSiS0_device, PiSfSiS0_device );
   if( haveCudaDevice && jobSize > minGPUJobSize  )
      deviceStrengthsTensorData.copyMEData( me_data, tensorDataBuffer, config.optimizedGpuTransfer );
#endif   
   
   /****
    * Iterate over (lmf muf) irreps:
    */
   for (int iif = 0; iif < FinalIrreps.size(); ++iif)
   {
      ComputeSU3CGs_Simple(ir[kA], ir[kB], FinalIrreps[iif], lalb_indices, abf_cgSU3);
      Irreps0_vec[ iif ].resize( InitIrreps.size() );
      rho0_max_vec[ iif ].resize( InitIrreps.size() );
      resultsISSize_vec[ iif ].resize( InitIrreps.size() );
      results[ iif ].resize( InitIrreps.size() );

#ifdef HAVE_CUDA
      if( haveCudaDevice && jobSize > minGPUJobSize )
         deviceStrengthsTensorData.copy_FinalIrreps( FinalIrreps[iif], lalb_indices, abf_cgSU3,
                                                     tensorDataBuffer, config.optimizedGpuTransfer );
#endif      
      
      /****
       * Iterate over (lmi mui) irreps:
       */
      for( int ii = 0; ii < InitIrreps.size(); ++ii )
      {
         ComputeSU3CGs_Simple(ir[kD], ir[kC], InitIrreps[ii], ldlc_indices, dci_cgSU3);
         //checkIndices( me_data, lalb_indices, ldlc_indices );
         SU3_VEC Irreps0;
         SU3::Couple(FinalIrreps[iif], InitIrreps[ii], Irreps0); // (lmf muf) x (lmi mui) -> rho_{0} (lm0 mu0)
         Irreps0_vec[ iif ][ ii ] = Irreps0;
         rho0_max_vec[ iif ][ ii ].resize( Irreps0.size() );
         resultsISSize_vec[ iif ][ ii ].resize( Irreps0.size() );
         results[ iif ][ ii ].resize( Irreps0.size() );
         
#ifdef HAVE_CUDA
         if( haveCudaDevice && jobSize > minGPUJobSize )
            deviceStrengthsTensorData.copy_InitIrreps( InitIrreps[ii], ldlc_indices, dci_cgSU3,
                                                       tensorDataBuffer, config.optimizedGpuTransfer );
#endif      
         

         /****
          * Iterate over rho(lmu0 mu0):
          */
         for( int i0 = 0; i0 < Irreps0.size(); ++i0 )
         {                                   
            if( !isIncluded( Irreps0[ i0 ] ) ) // Irrep0 does not have tensorial character of the operator
               continue;

            int rho0_max = Irreps0[i0].rho;
            rho0_max_vec[ iif ][ ii ][ i0 ] = rho0_max;
            ComputeSU3CGs( FinalIrreps[iif], InitIrreps[ii], Irreps0[i0], lfli_indices, fi0_cgSU3 );
            
#ifdef HAVE_CUDA
            if( haveCudaDevice && jobSize > minGPUJobSize )
               deviceStrengthsTensorData.copy_Irreps0( Irreps0[i0], lfli_indices, fi0_cgSU3,
                                                       tensorDataBuffer, config.optimizedGpuTransfer );
#endif      
            

            /****	
             *	Scalar opetator [i.e. J0=0] has L0==S0. Since we are dealing with
             *	a two-body interaction ==> S0 = 0, 1, or 2.
             *	S0=0 appears 2 times: Sf=0 x Si=0 --> S0=0 and Sf=1 x Si=1 --> S0=0
             *	==> 2 x SU3::kmax(w0, L0=0) x rho0_max x 3
             *
             *	S0=1 appears 3 times: Sf=1 x Si=0 --> S0=1; Sf=0 x Si=1 --> S0=0; Sf=1 x Si=1 ---> S0=1
             *	==> 3 x SU3::kmax(w0, L0=1) x rho0_max x 3
             *
             *	S0=2 appears 1 times: Sf=1 x Si=1 --> S0=2
             *	==> 1 x SU3::kmax(w0, L0=1) x rho0_max x 3
             *
             *	Two body interaction has L0=S0 and  can be either 0, 1, or 2.
             *
             *	
             * Order of elements in results:
             *   results[iS, k0, rho0, type], where iS = 0 ... 6 [= number of Sf Si S0 quantum numbers for two-body interaction]
             */
            // 3 ... PP NN PN
            // 2 ... k0_max <= 2
            int resultsISSize = 3*2*Irreps0[i0].rho;
            int resultsSize = 6*resultsISSize;
            resultsISSize_vec[ iif ][ ii ][ i0 ] = resultsISSize;
            results[ iif ][ ii ][ i0 ].resize( resultsSize );
            for( int resultIdx = 0; resultIdx < resultsSize; resultIdx++ )
               results[ iif ][ ii ][ i0 ][ resultIdx ] = 0.0;
            
            strengthsTensorData tensorData( FinalIrreps[iif], InitIrreps[ii], Irreps0[i0], 
                                            me_data, 
                                            &lalb_indices[0], lalb_indices.size(), 
                                            &ldlc_indices[0], ldlc_indices.size(), 
                                            &lfli_indices[0], lfli_indices.size(), 
                                            &abf_cgSU3[0], abf_cgSU3.size(), 
                                            &dci_cgSU3[0], dci_cgSU3.size(), 
                                            &fi0_cgSU3[0], fi0_cgSU3.size(), 
                                            &SfSiS0[0][0], 
                                            &PiSfSiS0[0], 
                                            results[ iif ][ ii ][ i0].data(),
                                            resultsISSize );
#ifdef HAVE_CUDA
            if( haveCudaDevice && jobSize > minGPUJobSize )
            {
               
               
               const int blockSize( 256 );
               const int size = me_data.size;
               const int blocksNumber = size / blockSize + ( size % blockSize != 0 );
               if( ! config.optimizedGpuTransfer )
                  deviceStrengthsTensorData.setupResults(results[ iif ][ ii ][ i0].data(), resultsISSize, blocksNumber );
               else
                  tensorDataBuffer.allocate_results( blocksNumber*6*resultsISSize,
                                                     deviceStrengthsTensorData.results_host,
                                                     deviceStrengthsTensorData.results );
               deviceStrengthsTensorData.resultsISSize = resultsISSize;
               
               tensorData_list.push_back( deviceStrengthsTensorData );
               if( ! config.optimizedGpuTransfer )
                  deviceStrengthsTensorData.resetDealocationFlags();
               rho0_max_list.push_back( rho0_max );
               resultsISSize_list.push_back( resultsISSize );
               results_list.push_back( results[ iif ][ ii ][ i0 ].data() );
            } 
            else // if( haveCudaDevice )
               if( ! this->hostStrengthsTensor.computeHost( tensorData ) )
                  abort();
#else
            if( ! this->hostStrengthsTensor.computeHost( tensorData ) )
               abort();
#endif 
         }
      }
   }

   
   /*****
    * Process data on the GPU
    */
#ifdef HAVE_CUDA
   if( haveCudaDevice && jobSize > minGPUJobSize )
   {
      /*for( int i = 0; i < tensorData_list.size(); i++ )
      {
         if( ! this->cudaStrengthsTensor.computeDevice( tensorData_list[ i ],
                                                        me_data.size,
                                                        rho0_max_list[ i ],
                                                        resultsISSize_list[ i ],
                                                        results_list[ i ] ) )
         return;
         freeGPUData( tensorData_list[ i ] );
      }*/
      
      if( ! this->cudaStrengthsTensor.computeDeviceFast( tensorData_list,
                                                         me_data.size,
                                                         rho0_max_list,
                                                         resultsISSize_list,
                                                         results_list,
                                                         tensorDataBuffer,
                                                         config.optimizedGpuTransfer ) )
         abort();
      for( int i = 0; i < tensorData_list.size(); i++ )
         tensorData_list[ i ].freeGPUData();
      
        
   }
#endif   
            
   /****
    * Store the results
    */
   for (int iif = 0; iif < FinalIrreps.size(); ++iif)
      for( int ii = 0; ii < InitIrreps.size(); ++ii )
      {
         const SU3_VEC& Irreps0 = Irreps0_vec[ iif ][ ii ];
         for( int i0 = 0; i0 < Irreps0.size(); ++i0 )
         {
            if( !isIncluded( Irreps0[ i0 ] ) ) // Irrep0 does not have tensorial character of the operator
               continue;

            const int rho0_max = rho0_max_vec[ iif ][ ii ][ i0 ];
            for (int iS = 0, index = 0; iS < 6; ++iS, index += 3*2*rho0_max )
            {
               int k0_max = SU3::kmax(Irreps0[i0], SfSiS0[iS][2]);
               if( !k0_max )
                  continue;
                     
               int negligibels( 0 );
               for( int resultsIdx = 0; resultsIdx < 3*k0_max*Irreps0[i0].rho; resultsIdx++ )
                   if( Negligible( results[ iif ][ ii ][ i0][ iS*resultsISSize_vec[ iif ][ ii ][ i0 ] + resultsIdx ] ) )
                      negligibels++;
               if( negligibels == 3*k0_max*Irreps0[i0].rho )
                  continue;

               for( int k0 = 0, j = 0; k0 < k0_max; k0++ )
               {
                  for (int rho0 = 0; rho0 < Irreps0[i0].rho; ++rho0, j += 3)
                  {
                     {
                        result.iS_wf_wi_w0.push_back( iS );
                        result.iS_wf_wi_w0.push_back( FinalIrreps[iif].lm );                                                                        
                        result.iS_wf_wi_w0.push_back( FinalIrreps[iif].mu );
                        result.iS_wf_wi_w0.push_back( InitIrreps[ii].lm );
                        result.iS_wf_wi_w0.push_back( InitIrreps[ii].mu );
                        result.iS_wf_wi_w0.push_back( Irreps0[i0].lm );
                        result.iS_wf_wi_w0.push_back( Irreps0[i0].mu );
                        result.coeffs.insert( result.coeffs.end(),
                                              &results[ iif ][ ii ][ i0 ][index],
                                              &results[ iif ][ ii ][ i0 ][index + 3*k0_max*rho0_max] );
                        result.ntensors++;
                     }                             
                  }
               }
            }
         }   
      }
   //}
   return true;
}

template< typename SelectionRules >
void 
JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::
PerformSU3Decomposition( int Nmax,
                         int valence_shell,
                         int nmax,
                         const string& sOutputFileName )
{
   
#ifdef HAVE_CUDA
   cudaMalloc( ( void** ) &SfSiS0_device, 18*sizeof( int ) );
   cudaMalloc( ( void** ) &PiSfSiS0_device, 6*sizeof( double ) );
   CHECK_CUDA_DEVICE;
   cudaMemcpy( SfSiS0_device, SfSiS0, 18*sizeof( int ), cudaMemcpyHostToDevice );
   cudaMemcpy( PiSfSiS0_device, PiSfSiS0, 6*sizeof( double ), cudaMemcpyHostToDevice );
   CHECK_CUDA_DEVICE;
#endif
   
   
   vector< transformationResult > results;
   list< int > resultsSourceNodes;
   enum{ getTask = 0, returnResult };
   bool haveMpi( false );   
   int my_rank( 0 ), nprocs( 1 );
#ifdef HAVE_MPI
   haveMpi = true;
   MPI_Comm_size( MPI_COMM_WORLD, &nprocs );
   MPI_Comm_rank( MPI_COMM_WORLD, &my_rank );
  
   double broadcastTime =  defaultTimer.GetTime();
   if( my_rank == 0 )
      cout << " Time: " << defaultTimer.GetTime() << " Broadcasting interaction data ..." << endl;
   MPI_Bcast_interactionData( 0, MPI_COMM_WORLD );
   if( my_rank == 0 )
      cout << " Time: " << defaultTimer.GetTime() << " Broadcasting interaction data ... DONE" << endl;
   broadcastTime =  defaultTimer.GetTime() - broadcastTime;
   
   if( haveMpi && nprocs > 1 )
   {
      MPI_Barrier( MPI_COMM_WORLD );
      MPI_Status status;
      MPI_Request sendRequest;   
      if( my_rank == 0 )
      {
         double computationTime = defaultTimer.GetTime();
         const int allShells = this->interactionData.size(); 
         int taskIndex = this->interactionData.size() - 1;
         for( taskIndex = this->interactionData.size() - 1; taskIndex >= 0; taskIndex-- )
         {
            int requestingProcess, request;
            //cout << "Wating for new request..." << endl;
            MPI_Recv( &request, 1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status );
            requestingProcess = status.MPI_SOURCE;
            //cout << "Getting request from process " << requestingProcess << endl;

            matrixElement& me = this->interactionData[ taskIndex ];
            SelectionRules IsIncluded;
            if( me.size == 0 || 
                ! IsIncluded( me.n[ 0 ], me.n[ 1 ], me.n[ 2 ], me.n[ 3 ] ) ) // operator does not have this combination of n1 n2 n3 n4          
               continue;
            
            cout << " Time: " << defaultTimer.GetTime()
                 << " \t Processing shell \t" << me.n[ 0 ] << "\t" << me.n[ 1 ] << "\t" << me.n[ 2 ] << "\t" << me.n[ 3 ] << "\t number "
                 << ( allShells - taskIndex ) << " / " << allShells 
                 << "\t => " << ( double ) ( allShells - taskIndex ) / ( double ) allShells * 100.0 << " % \t done ... Matrix elements: "               
                 << me.size << "\t -> process " << requestingProcess << "           \n" << flush;

            MPI_Isend( &taskIndex, 1, MPI_INT, requestingProcess, 0, MPI_COMM_WORLD, &sendRequest );
            resultsSourceNodes.push_back( requestingProcess );
         }
         for( int i = 1; i < nprocs; i++ )
         {
            int requestingProcess, request;
            MPI_Recv( &request, 1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status );
            requestingProcess = status.MPI_SOURCE;
            int taskIndex( -1 );
            MPI_Isend( &taskIndex, 1, MPI_INT, requestingProcess, 0, MPI_COMM_WORLD, &sendRequest );
            //cout << "The master process has no more tasks for process " << requestingProcess << endl;
         }
         MPI_Barrier( MPI_COMM_WORLD );
         computationTime = defaultTimer.GetTime() - computationTime;
        
        /****
         * Gather data
         */
         double gatheringTime = defaultTimer.GetTime();
         const int allResults = resultsSourceNodes.size();
         results.resize( allResults );
         list< int >::iterator result_it = resultsSourceNodes.end();
         int resultIndex( allResults - 1 );
         while( result_it != resultsSourceNodes.begin() )
         {
            result_it--;
            results[ resultIndex ].MPI_Recv( *result_it, 0, MPI_COMM_WORLD );
            resultIndex--;
            //cout << " Time: " << defaultTimer.GetTime()
            //     << " \t Gathering results \t " << resultIndex << " / " << allResults << "\t from process " << *result_it << endl;
         }
      
         gatheringTime = defaultTimer.GetTime() - gatheringTime;
         MPI_Barrier( MPI_COMM_WORLD );
         /****
          * Wait for other processes to write their messages.
          */
         for( int i = 1; i < nprocs; i++ )
            MPI_Barrier( MPI_COMM_WORLD );

         cout << "Broadcasting time: " << broadcastTime << endl;
         cout << "Computation time: " << computationTime << endl;
         cout << "Gathering time: " << gatheringTime << endl;
      }
      else
      {
         int numberOfTasks( 0 );
         double processTime = defaultTimer.GetTime();
         {
            StrengthsTensorDataBuffer tensorDataBuffer( MAX_UINT8_DATA_SIZE,
                                                        MAX_UINT16_DATA_SIZE,
                                                        MAX_INT_DATA_SIZE,
                                                        MAX_DOUBLE_DATA_SIZE,
                                                        MAX_DOUBLE_PTR_DATA_SIZE,
                                                        MAX_RESULTS_DATA_SIZE,
                                                        MAX_TENSORS_DATA_SIZE );

            int taskIndex;
            list< transformationResult > localResults;
            int request( 1 );
            ::MPI_Send( &request, 1, MPI_INT, 0, 0, MPI_COMM_WORLD );
            ::MPI_Recv( &taskIndex, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE );

            while( taskIndex >= 0 )
            {
               matrixElement& me = this->interactionData[ taskIndex ];             
               transformationResult result( me.n[ 0 ], me.n[ 1 ], me.n[ 2 ], me.n[ 3 ] );		
               SelectionRules IsIncluded;            
               processMatrixElements( me, nmax, IsIncluded, result, tensorDataBuffer );
               numberOfTasks++;
               localResults.push_back( result );
               ::MPI_Send( &request, 1, MPI_INT, 0, 0, MPI_COMM_WORLD );
               ::MPI_Recv( &taskIndex, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
            }
            //cout << "Process " << my_rank << " has finished all tasks..." << endl;
            processTime = defaultTimer.GetTime() - processTime;
            MPI_Barrier( MPI_COMM_WORLD );
        
         
            /****
             * Gather data
             */
            list< transformationResult >::iterator result_it = localResults.end();
            while( result_it-- != localResults.begin() )
            {
               //cout << "Process " << my_rank << " is sending data..." << endl;
               result_it->MPI_Send( 0, 0, MPI_COMM_WORLD );
            }
         }

         MPI_Barrier( MPI_COMM_WORLD );
         for( int i = 1; i < nprocs; i++ )
         {
            if( i == my_rank )
               cout << "Process " << my_rank << " has finished all " << numberOfTasks << " tasks in " <<  processTime << " secs." << endl;
            MPI_Barrier( MPI_COMM_WORLD );
         }
         return;
      }
   }
   else // if( haveMpi && nprocs > 1 )
#endif   
   performSU3DecompositionSequential< SelectionRules >( Nmax, valence_shell, nmax, results );
   std::ofstream outfile(sOutputFileName.c_str());
   if (!outfile)
   {
      std::cerr << "Unable to open output file '" << sOutputFileName << "'" << std::endl;
      exit(EXIT_FAILURE);
   }
   outfile.precision(10);
   SaveOnDisk( results, outfile);
#ifdef HAVE_CUDA
   cudaFree( SfSiS0_device );
   cudaFree( PiSfSiS0_device );
#endif   
}


template< typename SelectionRules >
void 
JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::
performSU3DecompositionSequential( int Nmax,
                                   int valence_shell,
                                   int nmax,
                                   vector< transformationResult >& results )
{
   // allShells == pocet ruznych {na nb nc nd} kombinaci
   const int allShells = this->interactionData.size();
   int shellsLeft( allShells );
   results.resize( this->interactionData.size() );

   
#pragma omp parallel
   {
#ifdef HAVE_CUDA
      int deviceCount;
      cudaGetDeviceCount( &deviceCount );
      const int threadsPerDevice = omp_get_num_threads() / deviceCount;
      //cudaSetDevice( ceil( omp_get_num_threads() / threadsPerDevice  ) - 1 );
#endif
      StrengthsTensorDataBuffer tensorDataBuffer( MAX_UINT8_DATA_SIZE,
                                                  MAX_UINT16_DATA_SIZE,
                                                  MAX_INT_DATA_SIZE,
                                                  MAX_DOUBLE_DATA_SIZE,
                                                  MAX_DOUBLE_PTR_DATA_SIZE,
                                                  MAX_RESULTS_DATA_SIZE,
                                                  MAX_TENSORS_DATA_SIZE );
      
#pragma omp for schedule( dynamic )
// iteruj prez {na nb nc nd}
      for( int ishell = allShells - 1; ishell >= 0; ishell-- )
      { 
      // matrixElement = [ vec{i1}, vec{i2}, vec{i3}, vec{i4}, vec{J}, vec{Vpp}, vec{Vnn}, vec{Vpn} ] == <na * *, nb * *; * || V || nc * *, nd * *; *>
         const matrixElement& me_data = this->interactionData[ ishell ];
#pragma omp critical
         cout << " Time: " << defaultTimer.GetTime()
              << " \t Processing shell \t" << me_data.n[ 0 ] << "\t" << me_data.n[ 1 ] << "\t" << me_data.n[ 2 ] << "\t" << me_data.n[ 3 ] << "\t number "
              << allShells - shellsLeft << " / " << allShells 
              << "\t => " << ( double ) ( allShells - shellsLeft-- ) / ( double ) allShells * 100.0 << " % \t done ... Matrix elements: "
              << me_data.size << "\t -> thread " << omp_get_thread_num() << "         \n" << flush;

         SelectionRules IsIncluded;
         if( me_data.size == 0 || 
             ! IsIncluded( me_data.n[ 0 ], me_data.n[ 1 ], me_data.n[ 2 ], me_data.n[ 3 ] ) ) // operator does not have this combination of n1 n2 n3 n4
            continue;

         transformationResult result( me_data.n[ 0 ], me_data.n[ 1 ], me_data.n[ 2 ], me_data.n[ 3 ] );
         // procesuj maticove elementy typu <na * *, nb * *; * || V || nc * *, nd * *; *>
         processMatrixElements( me_data,
                               nmax,
                               IsIncluded,
                               result,
                               tensorDataBuffer );
         tensorDataBuffer.reset();
         results[ allShells - ishell - 1 ] = result;
      }
   }
}

void JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::
SaveOnDisk( std::vector< transformationResult >& results,
	    std::ofstream& str )
{
    for( int k = 0; k < results.size(); k++ )
    {
       transformationResult& result = results[ k ];
       result.write( str );
    }
}
      
void JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::
setCudaDevice()
{
#ifdef HAVE_CUDA
#ifdef HAVE_MPI

   /*int rank;
   MPI_Comm_rank( MPI_COMM_WORLD, &rank );
   
   if( rank < getCudaDeviceCount() + 1 )      // we are on the first node with the master process
   {
      if( rank != 0 )                         // it is the master process
         setCudaDevice( rank - 1 );
      else this->haveCudaDevice = false;
   }
   else                                       // on the other nodes we map CUDA devices by CPU cores
   {
      int cpu = sched_getcpu();
      if(  cpu < getCudaDevicCount() )
         setCudaDevice( cpu );
      else
         this->haveCudaDevice = false;
   }*/
      
#else // HAVE_MPI
   this->haveCudaDevice = true;         
#endif
#else  // HAVE_CUDA
   this->haveCudaDevice = false;   
#endif
}

#ifdef HAVE_MPI
void JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::MPI_Bcast_interactionData( int root, MPI_Comm comm )
{
   int my_rank;
   MPI_Comm_rank( comm, &my_rank );
   int size = this->interactionData.size();
   MPI_Bcast( &size, 1, MPI_INT, root, comm );
   if( my_rank != root )
      this->interactionData.resize( size );
   int bytes( 0 );
   for( int i = 0; i < size; i++ )
   {
      if( my_rank == 0 )
      {
         bytes += this->interactionData[ i ].sizeOf();
         cout << "  > " << i + 1 << " / " << size << " == > " << bytes / 1000000 << " Mbytes      \r" << flush;
      }
      this->interactionData[ i ].MPI_Bcast( root, comm );
   }
   if( my_rank == 0 )
      cout << endl;
}
#endif      

#endif
