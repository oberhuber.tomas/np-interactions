#ifndef logFactTable_h
#define logFactTable_h 

#ifdef HAVE_CUDA
#include <cuda.h>
#endif

#include <cmath>
#include "checkCUDADevice.h"
#include "device.h"
#include "deviceParams.h"

template< typename Device = host > class logFactTable{};

template<>
class logFactTable< host >
{
   public:

   logFactTable( const int size )
   {
      this->tableSize = size;
      data = new double[ size ];
      data[ 0 ] = data[ 1 ] = 0;
      for( int i = 2; i < size; i++ ) 
         data[ i ] = log( i ) + data[ i - 1 ];
      
#ifdef HAVE_CUDA
      cudaMalloc( ( void** ) &cudaData, this->tableSize * sizeof( double ) );
      cudaMemcpy( cudaData, data, this->tableSize * sizeof( double ), cudaMemcpyHostToDevice );
#endif
   };

   int getTableSize() const
   {
      return tableSize*warpSize;
   };

   double getLogFact( int number ) const
   {
      //printf( "logFactTable[ %d ] = %f \n", number, ( number < this->tableSize ) ?  data[ number ] : getLogFact( number-1 ) + log( number ) );
      return ( number < this->tableSize ) ?  data[ number ] : getLogFact( number-1 ) + log( number );
   };

   ~logFactTable()
   {
      delete[] data;
#ifdef HAVE_CUDA
      cudaFree( cudaData );
#endif      
   };

#ifdef HAVE_CUDA
   double* getGPUTable() const
   {
      return cudaData;
   };
#endif

   protected:
   
   // when decomposing 2LaLb up to nmax = 8 ... I need at most LogFact(35) ==> 128 should be enough
   int tableSize; 
   
   double* data;

#ifdef HAVE_CUDA
   double* cudaData;
#endif

};

#ifdef HAVE_CUDA
template<>
class logFactTable< cuda >
{   
   public:

   __device__ logFactTable( const double* globalTable,
                            double* sharedTable,
                            const int tableSize )
   : tableSize( tableSize )
   {
      this->sharedTable = globalTable;
      /*this->sharedTable = sharedTable;
      //const int tIdx = threadIdx.x; // TODO: opravit
      for( int tIdx = 0; tIdx < tableSize; tIdx++ )
      {
         int warpIdx = tIdx % warpSize;
         if( tIdx < tableSize )
         {
            double aux = globalTable[ tIdx ];
            for( int i = 0; i < warpSize; i++ )
                  sharedTable[ tIdx * warpSize + ( i + tIdx ) % warpSize ] = aux;
         }
      }
      this->tableOffset = threadIdx.x % warpSize;
      __syncthreads();*/
   }

   __device__ __host__ double getLogFact( const int number ) const
   {
      //return ( number < MAX_LOGFACT ) ?  sharedTable[ number*warpSize + tableOffset ] : getLogFact( number-1 ) + log( number );
      //printf( "logFactTable[ %d ] = %f \n", number, sharedTable[ number*warpSize + tableOffset ] );
      //return this->sharedTable[ number*warpSize + tableOffset ];
      return this->sharedTable[ number ];
   }
                                  
   protected:

   int tableSize;

   int tableOffset;

   const double* sharedTable;
};
#endif


#endif 
