/***************************************************************************
                          config.cpp  -  description
                             -------------------
    begin                : 2015/01/20
    copyright            : (C) 2015 by Tomá¹ Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <cstring>
#include <cstdlib>
#include <iostream>
#include "config.h"

using std::cout;
using std::cerr;
using std::endl;

Config config;

Config::Config()
: inputFile( 0 ),
  outputFile( 0 ),
  Nmax( 0 ),
  valenceShell( 0 ),
  debug( false ),
  gpuSpeedupTest( false ),
  optimizedGpuTransfer( true )
{
}

bool Config::parseCommandLine( int argc, char* argv[] )
{
   int i( 0 );
   for( int i = 1; i < argc; i++ )
   {
      if( strcmp( argv[ i ], "--input-file" ) == 0 ||
          strcmp( argv[ i ], "-i" ) == 0 )
      {
         this->inputFile = argv[ ++i ];
         continue;
      }
      if( strcmp( argv[ i ], "--output-file" ) == 0 ||
          strcmp( argv[ i ], "-o" ) == 0 )
      {
         this->outputFile = argv[ ++i ];
         continue;
      }
      if( strcmp( argv[ i ], "--Nmax" ) == 0 )
      {
         this->Nmax = atoi( argv[ ++i ] );
         if( this->Nmax < 1 )
            cerr << "Nmax must be positive integer number." << endl;
         continue;
      }
      if( strcmp( argv[ i ], "--valence-shell" ) == 0 )
      {
         this->valenceShell = atoi( argv[ ++i ] );
         if( this->valenceShell < 1 )
            cerr << "Nmax must be positive integer number." << endl;
         continue;
      }
      if( strcmp( argv[ i ], "--gpu-speedup-test" ) == 0 )
      {
         this->gpuSpeedupTest = true;
         continue;
      }
      if( strcmp( argv[ i ], "--gpu-non-optimized-transfer" ) == 0 )
      {
         this->optimizedGpuTransfer = false;
         continue;
      }      

      if( strcmp( argv[ i ], "--help" ) == 0 )
      {
         printHelp( argv[ 0 ] );
         return false;
      }
      if( strcmp( argv[ i ], "--debug" ) == 0 )
      {
         this->debug = true;
         continue;
      }
      cerr << "Unknown command line argument " << argv[ i ] << ". Use --help for more information." <<  endl;
      return false;
   }
   return true;
}

void Config::printHelp( const char* programName )
{
   cout << "Use of " << programName << ":" << endl
        << endl
        << "  --input-file or -i        Input file in proton-neutron J-coupled format." << endl
        << "  --ouput-file ot -o        Output file with SU3 tensors." << endl
        << "  --Nmax                    ???????????????????????" << endl
        << "  --valence-shell           ????????????????????????" << endl
#ifdef HAVE_CUDA
        << "  --gpu-speedup-test           Computes speed-up of the GPU accelerated part (SU3 strengths tensors)."
        << "  --gpu-non-optimized-transfer Turns off the optimized transfer of data to GPU."
#endif
        << endl
	<< "Structure of the output file:" << endl
	<< "n1 n2 n3 n4 IRf IRi IR0 iS" << endl
	<< "a_pp a_nn a_pn \t rho0 = 0 k0 = 0" << endl
	<< "a_pp a_nn a_pn \t rho0 = 1 k0 = 0" << endl
	<< " . " << endl
	<< " . " << endl
	<< "a_pp a_nn a_pn \t rho0 = rho0max k0 = 0" << endl
	<< "a_pp a_nn a_pn \t rho0 = 0 k0 = 1" << endl
	<< "a_pp a_nn a_pn \t rho0 = 1 k0 = 1" << endl
	<< " . " << endl
	<< " . " << endl
	<< "a_pp a_nn a_pn \t rho0 = rho0max k0 = 1" << endl
	<< " . " << endl
	<< " . " << endl
	<< "a_pp a_nn a_pn \t rho0 = rho0max k0 = 1" << endl;
}

