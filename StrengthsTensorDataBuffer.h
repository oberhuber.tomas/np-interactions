/* 
 * File:   StrengthsTensorDataBuffer.h
 * Author: oberhuber
 *
 * Created on March 31, 2018, 6:02 PM
 */

#pragma once

#include <cstdio>
#include <stdint.h>

class strengthsTensorData;

class StrengthsTensorDataBuffer
{
   public:
      
      StrengthsTensorDataBuffer( const size_t max_uint8_data_size,
                                 const size_t max_uint16_data_size,
                                 const size_t max_int_data_size,
                                 const size_t max_double_data_size,
                                 const size_t max_double_ptr_data_size,
                                 const size_t max_results_data_size,
                                 const size_t max_tensors_data_size );
      
      void allocate_uint8( const size_t size,
                          uint8_t*& host_data,
                          uint8_t*& device_data );
      
      void allocate_uint16( const size_t size,
                            uint16_t*& host_data,
                            uint16_t*& device_data );
      
      void allocate_int( const size_t size,
                         int*& host_data,
                         int*& device_data );

      void allocate_double( const size_t size,
                            double*& host_data,
                            double*& device_data );
      
      void allocate_double_ptr( const size_t size,
                                double**& host_data,
                                double**& device_data );
      
      void allocate_tensor( const size_t size,
                            strengthsTensorData*& host_data,
                            strengthsTensorData*& device_data );

      void allocate_results( const size_t size,
                             double*& host_data,
                             double*& device_data );
      
      void transferData();
      
      void transferResults();
      
      void reset();      
      
      ~StrengthsTensorDataBuffer();
   
   protected:
      
      uint8_t  *uint8_data_host, *uint8_data_device;
      uint16_t *uint16_data_host, *uint16_data_device;
      int *int_data_host, *int_data_device;
      double *double_data_host, *double_data_device;
      double **double_ptr_data_host, **double_ptr_data_device;
      double *results_data_host, *results_data_device;
      strengthsTensorData *tensors_data_host, *tensors_data_device;
      
      size_t uint8_data_pointer, uint16_data_pointer, int_data_pointer, double_data_pointer,
             double_ptr_data_pointer, results_data_pointer, tensors_data_pointer;
      
      size_t max_uint8_data_size, max_uint16_data_size, max_int_data_size, max_double_data_size,
             max_double_ptr_data_size, max_results_data_size, max_tensors_data_size;
};



