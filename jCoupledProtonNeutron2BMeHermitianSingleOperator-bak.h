#ifndef jCoupledProtonNeutron2BMeHermitianSingleOperatorH
#define jCoupledProtonNeutron2BMeHermitianSingleOperatorH

#include "nanbncnd.h"
#include "strengthsTensor.h"
#include "computeSU3CGs.h"
#include "su3xsu2.h"
#include "timer.h"
#include <map>
#include <vector>
#include <string>
#include <iostream>
#define SQRT sqrt
#ifdef HAVE_MPI
#include <mpi.h>
#define MY_MPI_COMM MPI_COMM_WORLD
#endif
using std::map;
using std::vector;
using std::string;
using std::cout;
using std::endl;

int SfSiS0[6][3] =  {{0, 0, 0}, {1, 1, 0}, {0, 1, 1}, {1, 0, 1}, {1, 1, 1},      {1, 1, 2}};
double PiSfSiS0[6] = {   1,         3,         3,         3,    sqrt(27),         sqrt(45)};

class JCoupledProtonNeutron2BMe_Hermitian_SingleOperator
{
   public:
      typedef double DOUBLE;

      struct ME_DATA
      {
         uint32_t size;
         uint16_t *i1, *i2, *i3, *i4;
         uint8_t* J;
         DOUBLE *Vpp, *Vnn, *Vpn;

           ME_DATA( uint32_t _size, uint16_t* _i1, uint16_t* _i2, uint16_t* _i3, uint16_t* _i4,
                    uint8_t* _J, DOUBLE* _Vpp, DOUBLE* _Vnn, DOUBLE* _Vpn )
           : size( _size ), i1( _i1 ), i2( _i2 ), i3( _i3 ), i4( _i4 ),
             J( _J ), Vpp( _Vpp ), Vnn( _Vnn ), Vpn( _Vpn )
           {};
        };
  
        typedef uint32_t SIZE_t;
        typedef uint16_t* I1_t;
        typedef uint16_t* I2_t;
        typedef uint16_t* I3_t;
        typedef uint16_t* I4_t;
        typedef uint8_t* J_t;
        typedef DOUBLE* VPP_t;
        typedef DOUBLE* VNN_t;
        typedef DOUBLE* VPN_t;


  typedef map<NANBNCND, ME_DATA> INTERACTION_DATA;

  INTERACTION_DATA interaction_data_;


  JCoupledProtonNeutron2BMe_Hermitian_SingleOperator(const std::string& sInteractionFileName, vector<NANBNCND>& ho_shells_combinations);

  void SwitchHOConvention();
  void MultiplyByCommonFactor();
  ME_DATA GetMe(NANBNCND nanbncnd);

  void Show();

  inline size_t GetNumberOfOperators() const {return 1;}


   void SaveOnDisk( std::vector<std::vector<NANBNCND> >& all_nanbncnd,
                    std::vector<std::vector<uint32_t> >& all_results_ntensors,
                    std::vector<std::vector<uint8_t> >&  all_results_iS_wf_wi_w0,
                    std::vector<std::vector<double> >&   all_results_coeffs,
                    std::ofstream& outfile );

	template< typename SelectionRules >
	void PerformSU3Decomposition( JCoupledProtonNeutron2BMe_Hermitian_SingleOperator& JCoupledPNME,
                                      int Nmax,
                                      int valence_shell,
                                      int nmax,
                                      const string& sOutputFileName,
                                      const int allShells );

   protected:

      enum {kA = 0, kB = 1, kC = 2, kD = 3};
      SU3::LABELS ir[4] = {SU3::LABELS(0, 0), SU3::LABELS(0, 0), SU3::LABELS(0, 0), SU3::LABELS(0, 0)};

#ifdef HAVE_CUDA
      typedef strengthsTensor< cuda > StrengthsTensorType;
#else
      typedef strengthsTensor< host > StrengthsTensorType;
#endif
      
      template< typename SelectionRules >
      void processMatrixElement( const ME_DATA& me_data,
                                 const int nmax,
                                 const SelectionRules& isIncluded,
                                 StrengthsTensorType& strTensor,
                                 int n1, int n2, int n3, int n4,
                                 vector< uint32_t >& results_ntensors,
                                 vector< uint8_t >& results_iS_wf_wi_w0,
                                 vector< double >& results_coeffs  );

};

JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::JCoupledProtonNeutron2BMe_Hermitian_SingleOperator(const std::string& sInteractionFileName, vector<NANBNCND>& ho_shells_combinations )
{
   int my_rank( 0 ), nprocs( 1 );
#ifdef HAVE_MPI
   /*boost::mpi::communicator mpi_comm_world;
   my_rank = mpi_comm_world.rank();
   nprocs  = mpi_comm_world.size();*/
   MPI_Comm_size( MPI_COMM_WORLD, &nprocs );
   MPI_Comm_rank( MPI_COMM_WORLD, &my_rank );
   //if( my_rank != 0 )
   //   return;
#endif
   int32_t n_elems, rank_destination( 0 );
   int na, nb, nc, nd;
   NANBNCND nanbncnd;

   if( my_rank == 0 )
   {
	std::ifstream file(sInteractionFileName.c_str(), std::ifstream::binary);
	if (!file)
	{ std::cout << "Could not open file '" << sInteractionFileName << "'!" << std::endl;
		exit (EXIT_FAILURE);
	}
	cout << "Start reading interaction file ... "; cout.flush();

        std::vector<int> number_combinations( nprocs, 0 );
	std::vector<int> number_tbme( nprocs, 0 );

	while (true)
	{
		int i1, i2, i3, i4, J;
		DOUBLE Vpp, Vnn, Vpn;
		file.read((char*)&na, sizeof(int));
		file.read((char*)&nb, sizeof(int));
		file.read((char*)&nc, sizeof(int));
		file.read((char*)&nd, sizeof(int));
		file.read((char*)&n_elems, sizeof(uint32_t));


		if( !file ) 
		{
                   n_elems = -1;
#ifdef HAVE_MPI
                   MPI_Bcast( ( void* )&n_elems, 1, MPI_INT, 0, MY_MPI_COMM );          
#endif                        
	           break;
		}
               
		I1_t vec_i1 = new uint16_t[n_elems]; 
		I2_t vec_i2 = new uint16_t[n_elems]; 
		I3_t vec_i3 = new uint16_t[n_elems]; 
		I4_t vec_i4 = new uint16_t[n_elems]; 
		J_t  vec_J  = new uint8_t[n_elems];
		VPP_t vec_Vpp = new double[n_elems]; 
		VNN_t vec_Vnn = new double[n_elems]; 
		VPN_t vec_Vpn = new double[n_elems]; 

		file.read((char*)vec_i1, n_elems*sizeof(uint16_t));
		file.read((char*)vec_i2, n_elems*sizeof(uint16_t));
		file.read((char*)vec_i3, n_elems*sizeof(uint16_t));
		file.read((char*)vec_i4, n_elems*sizeof(uint16_t));
		file.read((char*)vec_J, n_elems*sizeof(uint8_t));
		file.read((char*)vec_Vpp, n_elems*sizeof(double));
		file.read((char*)vec_Vnn, n_elems*sizeof(double));
		file.read((char*)vec_Vpn, n_elems*sizeof(double));

		nanbncnd[kNA] = na;
		nanbncnd[kNB] = nb;
		nanbncnd[kNC] = nc;
		nanbncnd[kND] = nd;
                  
                int position = std::distance(ho_shells_combinations.begin(), std::find(ho_shells_combinations.begin(), ho_shells_combinations.end(), nanbncnd));
                if (position == ho_shells_combinations.size()) // ==> {na, nb, nc, nd} is not between HO combinations to be processes
                {
                   delete []vec_i1;
                   delete []vec_i2;
                   delete []vec_i3;
                   delete []vec_i4;
                   delete []vec_J;
                   delete []vec_Vpp;
                   delete []vec_Vnn;
                   delete []vec_Vpn;
                   continue;
               }
#ifdef HAVE_MPI
               rank_destination = position % nprocs;
               number_tbme[rank_destination]   += n_elems;
               number_combinations[rank_destination] += 1;
               MPI_Bcast((void*)&n_elems, 1, MPI_INT, 0, MY_MPI_COMM );
               MPI_Bcast((void*)&rank_destination, 1, MPI_INT, 0, MY_MPI_COMM );
               cout << "Assigning " << na << " " << nb << " " << nc << " " << nd << " to process " << rank_destination << endl;

//			cout << na << " " << nb << " " << nc << " " << nd << "\tNumber of matrix elements:" << n_elems << "\t Assigned to process:" << rank_destination << endl;
#endif
               if (rank_destination != my_rank ) // data obtained from file will be sent to rank = rank_destination
               {
#ifdef HAVE_MPI                  
                       MPI_Send( &nanbncnd[kNA], 1, MPI_BYTE, rank_destination, 0, MPI_COMM_WORLD );
                       MPI_Send( &nanbncnd[kNB], 1, MPI_BYTE, rank_destination, 0, MPI_COMM_WORLD );
                       MPI_Send( &nanbncnd[kNC], 1, MPI_BYTE, rank_destination, 0, MPI_COMM_WORLD );
                       MPI_Send( &nanbncnd[kND], 1, MPI_BYTE, rank_destination, 0, MPI_COMM_WORLD );
                       MPI_Send( vec_i1, n_elems * sizeof( uint16_t ), MPI_BYTE, rank_destination, 0, MPI_COMM_WORLD );
                       MPI_Send( vec_i2, n_elems * sizeof( uint16_t ), MPI_BYTE, rank_destination, 0, MPI_COMM_WORLD );
                       MPI_Send( vec_i3, n_elems * sizeof( uint16_t ), MPI_BYTE, rank_destination, 0, MPI_COMM_WORLD );
                       MPI_Send( vec_i4, n_elems * sizeof( uint16_t ), MPI_BYTE, rank_destination, 0, MPI_COMM_WORLD );
                       MPI_Send( vec_J, n_elems * sizeof( uint8_t ), MPI_BYTE, rank_destination, 0, MPI_COMM_WORLD );
                       MPI_Send( vec_Vpp, n_elems * sizeof( double ), MPI_BYTE, rank_destination, 0, MPI_COMM_WORLD );
                       MPI_Send( vec_Vnn, n_elems * sizeof( double ), MPI_BYTE, rank_destination, 0, MPI_COMM_WORLD );
                       MPI_Send( vec_Vpn, n_elems * sizeof( double ), MPI_BYTE, rank_destination, 0, MPI_COMM_WORLD );

                       delete []vec_i1;
                       delete []vec_i2;
                       delete []vec_i3;
                       delete []vec_i4;
                       delete []vec_J;
                       delete []vec_Vpp;
                       delete []vec_Vnn;
                       delete []vec_Vpn;
#endif                       
               }
               else // (rank_destination != my_rank)*/
               {
                    INTERACTION_DATA::iterator it = interaction_data_.find(nanbncnd);
                    //cout << "Receiving " << na << " " << nb << " " << nc << " " << nd << " at process " << my_rank << endl;
                    if (it == interaction_data_.end())
                    {
                            interaction_data_.insert(std::make_pair(nanbncnd, ME_DATA(n_elems, vec_i1, vec_i2, vec_i3, vec_i4, vec_J, vec_Vpp, vec_Vnn, vec_Vpn)));
                    }
                    else
                    {
                            // This should never happen
                            std::cerr << "The same set of {na, nb, nc, nd}={" << na << ", " << nb << ", " << nc << ", " << nc << "} occurs multiple times in the input file '" << sInteractionFileName << "' !!!" << std::endl;
                            exit(EXIT_FAILURE);
                    }

               }

	}
	cout << "Done" << endl;
   }
#ifdef HAVE_MPI
   else // if( mpiRank == 0 ) 
   {
       do
       {
               MPI_Bcast((void*)&n_elems, 1, MPI_INT, 0, MY_MPI_COMM);
               if (n_elems == -1)
               {
                       break;
               }
               MPI_Bcast((void*)&rank_destination, 1, MPI_INT, 0, MY_MPI_COMM);

               if (my_rank == rank_destination)
               {                       
                       I1_t vec_i1 = new uint16_t[n_elems]; 
                       I2_t vec_i2 = new uint16_t[n_elems]; 
                       I3_t vec_i3 = new uint16_t[n_elems]; 
                       I4_t vec_i4 = new uint16_t[n_elems]; 
                       J_t  vec_J  = new uint8_t[n_elems];
                       VPP_t vec_Vpp = new double[n_elems]; 
                       VNN_t vec_Vnn = new double[n_elems]; 
                       VPN_t vec_Vpn = new double[n_elems]; 

                       //mpi_comm_world.recv(0, 0, nanbncnd);
                       //mpi_comm_world.recv(0, 0, vec_i1, n_elems);
                       //mpi_comm_world.recv(0, 0, vec_i2, n_elems);
                       //mpi_comm_world.recv(0, 0, vec_i3, n_elems);
                       //mpi_comm_world.recv(0, 0, vec_i4, n_elems);
                       //mpi_comm_world.recv(0, 0, vec_J, n_elems);
                       //mpi_comm_world.recv(0, 0, vec_Vpp, n_elems);
                       //mpi_comm_world.recv(0, 0, vec_Vnn, n_elems);
                       //mpi_comm_world.recv(0, 0, vec_Vpn, n_elems);
                       
                       MPI_Status status;
                       MPI_Recv( &nanbncnd[kNA], 1, MPI_BYTE, 0, 0, MPI_COMM_WORLD, &status );
                       MPI_Recv( &nanbncnd[kNB], 1, MPI_BYTE, 0, 0, MPI_COMM_WORLD, &status );
                       MPI_Recv( &nanbncnd[kNC], 1, MPI_BYTE, 0, 0, MPI_COMM_WORLD, &status );
                       MPI_Recv( &nanbncnd[kND], 1, MPI_BYTE, 0, 0, MPI_COMM_WORLD, &status );
                       MPI_Recv( vec_i1, n_elems * sizeof( uint16_t ), MPI_BYTE, 0, 0, MPI_COMM_WORLD, &status );
                       MPI_Recv( vec_i2, n_elems * sizeof( uint16_t ), MPI_BYTE, 0, 0, MPI_COMM_WORLD, &status );
                       MPI_Recv( vec_i3, n_elems * sizeof( uint16_t ), MPI_BYTE, 0, 0, MPI_COMM_WORLD, &status );
                       MPI_Recv( vec_i4, n_elems * sizeof( uint16_t ), MPI_BYTE, 0, 0, MPI_COMM_WORLD, &status );
                       MPI_Recv( vec_J, n_elems * sizeof( uint8_t ), MPI_BYTE, 0, 0, MPI_COMM_WORLD, &status );
                       MPI_Recv( vec_Vpp, n_elems * sizeof( double ), MPI_BYTE, 0, 0, MPI_COMM_WORLD, &status );
                       MPI_Recv( vec_Vnn, n_elems * sizeof( double ), MPI_BYTE, 0, 0, MPI_COMM_WORLD, &status );
                       MPI_Recv( vec_Vpn, n_elems * sizeof( double ), MPI_BYTE, 0, 0, MPI_COMM_WORLD, &status );
		       

                       INTERACTION_DATA::iterator it = interaction_data_.find(nanbncnd);
                       if (it == interaction_data_.end())
                       {
                               interaction_data_.insert(std::make_pair(nanbncnd, ME_DATA(n_elems, vec_i1, vec_i2, vec_i3, vec_i4, vec_J, vec_Vpp, vec_Vnn, vec_Vpn)));
                       }
                       else
                       {
                               // This should never happen
                               std::cerr << "The same set of {na, nb, nc, nd} occurs multiple times in the input file '" << sInteractionFileName << "' !!!" << std::endl;
                               exit(EXIT_FAILURE);
                       }
               }
       } while(true);
   }
#endif
}

JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::ME_DATA JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::GetMe(NANBNCND nanbncnd)
{
	INTERACTION_DATA::iterator it = interaction_data_.find(nanbncnd);
	if (it != interaction_data_.end())
	{
		return it->second;
	}
	else
	{
		return ME_DATA(0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
	}
}

// This function multiplies all matrix elements by a factor
// provided by the first line of equation (1) in devel/docs/Recoupling/Recoupling.pdf
void JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::MultiplyByCommonFactor()
{
	INTERACTION_DATA::iterator it = interaction_data_.begin();
	for (; it != interaction_data_.end(); ++it)
	{
		ME_DATA me_data = it->second;
		SIZE_t size = me_data.size;
		I1_t i1 = me_data.i1;
		I2_t i2 = me_data.i2;
		I3_t i3 = me_data.i3;
		I4_t i4 = me_data.i4;
		J_t  J   = me_data.J;
		VPP_t Vpp  = me_data.Vpp;
		VNN_t Vnn  = me_data.Vnn;
		VPN_t Vpn  = me_data.Vpn;

		for (int i = 0; i < size; ++i)
		{
			int na, la, jja, nb, lb, jjb, nc, lc, jjc, nd, ld, jjd, JJ;

			sps2Index::getNLJ(i1[i], na, la, jja);
			sps2Index::getNLJ(i2[i], nb, lb, jjb);
			sps2Index::getNLJ(i3[i], nc, lc, jjc);
			sps2Index::getNLJ(i4[i], nd, ld, jjd);
			JJ = 2*J[i];

			double common_phase = MINUSto(nd + nc - (jjd + jjc)/2)*SQRT(jja + 1)*SQRT(jjb + 1)*SQRT(jjc + 1)*SQRT(jjd + 1)*(JJ + 1);
			//	delta_{na nb}{la lb}{ja jb}{ta tb}
			int iab = (na == nb) && (la == lb) && (jja == jjb);
			//	delta_{nc nd}{lc ld}{jc jd}{tc td}
			int	idc = (nd == nc) && (ld == lc) && (jjd == jjc);

			Vpp[i] *= common_phase*SQRT((iab + 1)*(idc + 1));
			Vnn[i] *= common_phase*SQRT((iab + 1)*(idc + 1));
			Vpn[i] *= common_phase;
		}
	}
}

void JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::SwitchHOConvention()
{
	INTERACTION_DATA::iterator it = interaction_data_.begin();
	for (; it != interaction_data_.end(); ++it)
	{
		ME_DATA me_data = it->second;

		SIZE_t size = me_data.size;
		I1_t i1 = me_data.i1;
		I2_t i2 = me_data.i2;
		I3_t i3 = me_data.i3;
		I4_t i4 = me_data.i4;
		VPP_t Vpp  = me_data.Vpp;
		VNN_t Vnn  = me_data.Vnn;
		VPN_t Vpn  = me_data.Vpn;

		for (int i = 0; i < size; ++i)
		{
			int na, la, jja, nb, lb, jjb, nc, lc, jjc, nd, ld, jjd, JJ;
                        
                        //cout << "i = " << i << endl;

			sps2Index::getNLJ(i1[i], na, la, jja);
			sps2Index::getNLJ(i2[i], nb, lb, jjb);
			sps2Index::getNLJ(i3[i], nc, lc, jjc);
			sps2Index::getNLJ(i4[i], nd, ld, jjd);

			int phase = MINUSto((na - la + nb - lb + nc - lc + nd - ld)/2);

			Vpp[i] *= phase;
			Vnn[i] *= phase;
			Vpn[i] *= phase;
		}
	}
}

void JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::Show()
{
	cout << "na   nb   nc   nd" << endl;
	INTERACTION_DATA::iterator it = interaction_data_.begin();
	for (; it != interaction_data_.end(); ++it)
	{
		NANBNCND nanbncnd = it->first;
		ME_DATA me_data = it->second;

		cout << (int)nanbncnd[kNA] << "    " << (int)nanbncnd[kNB] << "    " << (int)nanbncnd[kNC] << "    " << (int)nanbncnd[kND] << endl;

		SIZE_t size = me_data.size;
		I1_t i1 = me_data.i1;
		I2_t i2 = me_data.i2;
		I3_t i3 = me_data.i3;
		I4_t i4 = me_data.i4;
		J_t  J   = me_data.J;
		VPP_t Vpp  = me_data.Vpp;
		VNN_t Vnn  = me_data.Vnn;
		VPN_t Vpn  = me_data.Vpn;

		for (int i = 0; i < size; ++i)
		{
			int na, la, jja, nb, lb, jjb, nc, lc, jjc, nd, ld, jjd, JJ;

			sps2Index::getNLJ(i1[i], na, la, jja);
			sps2Index::getNLJ(i2[i], nb, lb, jjb);
			sps2Index::getNLJ(i3[i], nc, lc, jjc);
			sps2Index::getNLJ(i4[i], nd, ld, jjd);
			cout << "\t\t";
/*			
			cout << i1[i] << " " << i2[i] << " " << i3[i] << " " << i4[i] << " J:" << (int)J[i] << "\t\t\t";
*/			

			cout << la << " " << lb << " " << lc << " " << ld << "\t\t";
			cout << (int)J[i] << "\t\t" << jja << "/2 " << jjb << "/2 " << jjc << "/2 " << jjd << "/2\t\t";

			cout << Vpp[i] << " " << Vnn[i] << " " << Vpn[i] << endl;
		}
	}
}

template< typename SelectionRules >
void
JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::
processMatrixElement( const ME_DATA& me_data,
                      const int nmax,                                                                
                      const SelectionRules& isIncluded,
                      StrengthsTensorType& strTensor,
                      int n1, int n2, int n3, int n4,
                      vector< uint32_t >& results_ntensors,
                      vector< uint8_t >& results_iS_wf_wi_w0,
                      vector< double >& results_coeffs  )
{
                SIZE_t size = me_data.size;
		I1_t i1 = me_data.i1;
		I2_t i2 = me_data.i2;
		I3_t i3 = me_data.i3;
		I4_t i4 = me_data.i4;
		J_t  J   = me_data.J;
		VPP_t Vpp  = me_data.Vpp;
		VNN_t Vnn  = me_data.Vnn;
		VPN_t Vpn  = me_data.Vpn;
   vector<uint16_t> lalb_indices; // (la, lb): lalb_indices[la*lb_max + lb] ---> <(na 0) la; (nb 0) lb || wf * *>
   vector<uint16_t> ldlc_indices; // (ld, lc): lalb_indices[ld*lc_max + lc] ---> <(0 nd) ld; (0 nc) lc || wi * *>
   vector<uint16_t> lfli_indices;// (Lf, Li): lflf_indices[Lf*Li_max + Li] ---> {<wf * Lf; wi * Li || w0 * L0=0>_{*}, <wf * Lf; wi * Li || w0 * L0=1>_{*}, <wf * Lf; wi * Li || w0 * L0=2>_{*} }

   vector<double> abf_cgSU3; // <(na 0) * (nb 0) * || (lm mu)f *>
   vector<double> dci_cgSU3; // <(0 nd) * (0 nc) * || (lm mu)i *>
   vector<double> fi0_cgSU3; // <wf * wi * || w0 * L0={0,1,2}>_{*}

	
   abf_cgSU3.reserve(1024);
   dci_cgSU3.reserve(1024);
   fi0_cgSU3.reserve(1024);
          



		//std::cout << ir[kA] << " " << ir[kB] << " " << ir[kC] << " " << ir[kD] << "\r" << std::flush;
               SU3_VEC FinalIrreps;	//	list of (lmf muf) irreps resulting from (na 0) x (nb 0)
               SU3_VEC InitIrreps;		//	list of (lmi mui) irreps resulting from (0 nd) x (0 nc)

               FinalIrreps.reserve(nmax + 1); // (na 0) x (nb 0) ---> max(na, nb) SU(3) irreps
               InitIrreps.reserve(nmax + 1);
	
		FinalIrreps.clear(); InitIrreps.clear();
		SU3::Couple(ir[kA], ir[kB], FinalIrreps);	//	(na 0) x (nb 0) -> rho=1{(lmf muf)}
		SU3::Couple(ir[kD], ir[kC], InitIrreps);	//	(0 nd) x (0 nc) -> rho=1{(lmi mui)}	

//	iterate over (lmf muf) irreps	
                uint32_t ntensors = 0;	
		for (int iif = 0; iif < FinalIrreps.size(); ++iif)
		{
			ComputeSU3CGs_Simple(ir[kA], ir[kB], FinalIrreps[iif], lalb_indices, abf_cgSU3);

//	iterate over (lmi mui) irreps		
			for (int ii = 0; ii < InitIrreps.size(); ++ii)
			{
				ComputeSU3CGs_Simple(ir[kD], ir[kC], InitIrreps[ii], ldlc_indices, dci_cgSU3);

				SU3_VEC Irreps0;
				SU3::Couple(FinalIrreps[iif], InitIrreps[ii], Irreps0); // (lmf muf) x (lmi mui) -> rho_{0} (lm0 mu0)
//	iterate over rho(lmu0 mu0)
				for (int i0 = 0; i0 < Irreps0.size(); ++i0)
				{                                   
					if (!isIncluded(Irreps0[i0])) // Irrep0 does not have tensorial character of the operator
					{
						continue;
					}
                                        int rho0_max = Irreps0[i0].rho;
                                        double wfwiw0_results[6*3*2*9];
				        memset(wfwiw0_results, 0, sizeof(double)*6*3*2*rho0_max);

		                        //std::cout << ir[kA] << " " << ir[kB] << " " << ir[kC] << " " << ir[kD] 
					//          << "\t" << (int) Irreps0[i0].rho << " " << (int) Irreps0[i0].lm << " " << (int) Irreps0[i0].mu << " \r" << std::flush;

					ComputeSU3CGs(FinalIrreps[iif], InitIrreps[ii], Irreps0[i0], lfli_indices, fi0_cgSU3);
/*	
 *	Scalar opetator [i.e. J0=0] has L0==S0. Since we are dealing with
 *	a two-body interaction ==> S0 = 0, 1, or 2.
 *	S0=0 appears 2 times: Sf=0 x Si=0 --> S0=0 and Sf=1 x Si=1 --> S0=0
 *	==> 2 x SU3::kmax(w0, L0=0) x rho0_max x 3
 *
 *	S0=1 appears 3 times: Sf=1 x Si=0 --> S0=1; Sf=0 x Si=1 --> S0=0; Sf=1 x Si=1 ---> S0=1
 *	==> 3 x SU3::kmax(w0, L0=1) x rho0_max x 3
 *
 *	S0=2 appears 1 times: Sf=1 x Si=1 --> S0=2
 *	==> 1 x SU3::kmax(w0, L0=1) x rho0_max x 3
 *
 *	Two body interaciton has L0=S0 and  can be either 0, 1, or 2.
 *
 */	
//					int nresults = (2*(SU3::kmax(Irreps0[i0], 0)) + 3*SU3::kmax(Irreps0[i0], 1) + SU3::kmax(Irreps0[i0], 2))*Irreps0[i0].rho*3;
//	Order of elements in results: results[iS, k0, rho0, type], where iS = 0 ... 6 [= number of Sf Si S0 quantum numbers for two-body interaction]
//	index = iS x k0_max x rho0_max x 3 + k0 x rho0_max x 3 + rho0 x 3 + type
					/*std::vector<double> results[6];
					results[0].resize(3*SU3::kmax(Irreps0[i0], 0)*Irreps0[i0].rho);
					results[1].resize(3*SU3::kmax(Irreps0[i0], 0)*Irreps0[i0].rho);
					results[2].resize(3*SU3::kmax(Irreps0[i0], 1)*Irreps0[i0].rho);
					results[3].resize(3*SU3::kmax(Irreps0[i0], 1)*Irreps0[i0].rho);
					results[4].resize(3*SU3::kmax(Irreps0[i0], 1)*Irreps0[i0].rho);
					results[5].resize(3*SU3::kmax(Irreps0[i0], 2)*Irreps0[i0].rho);*/
                                        // alokovat 6 x 3 x 2 x Irreps0[i0].rho
                                        int resultsISSize = 3*2*Irreps0[i0].rho;
                                        int resultsSize = 6*resultsISSize;
                                        double* results = new double[ resultsSize ];
                                        for( int resultIdx = 0; resultIdx < resultsSize; resultIdx++ )
                                           results[ resultIdx ] = 0.0;
                                       
                                        strengthsTensorData 
                                           tensorData( FinalIrreps[iif], InitIrreps[ii], Irreps0[i0], \
						       size, i1, i2, i3, i4, J, Vpp, Vnn, Vpn, \
						       &lalb_indices[0], lalb_indices.size(), \
                                                       &ldlc_indices[0], ldlc_indices.size(), \
                                                       &lfli_indices[0], lfli_indices.size(), \
						       &abf_cgSU3[0], abf_cgSU3.size(), \
                                                       &dci_cgSU3[0], dci_cgSU3.size(), \
                                                       &fi0_cgSU3[0], fi0_cgSU3.size(), \
                                                       &SfSiS0[0][0], \
                                                       &PiSfSiS0[0], \
						       results, \
                                                       resultsISSize );
                                        if( ! strTensor.compute( tensorData ) )
                                        {
                                           return;
                                        }
                                          
					for (int iS = 0, index = 0; iS < 6; ++iS, index += 3*2*rho0_max )
					{
						int k0_max = SU3::kmax(Irreps0[i0], SfSiS0[iS][2]);
						if (!k0_max)
						{
							continue;
						}
                                                
						/*if (std::count_if(results[iS].begin(), results[iS].end(), Negligible) == 3*k0_max*Irreps0[i0].rho)
							continue;*/
                                                int negligibels( 0 );
                                                for( int resultsIdx = 0; resultsIdx < 3*k0_max*Irreps0[i0].rho; resultsIdx++ )
                                                   if( Negligible( results[ iS*resultsISSize + resultsIdx ] ) )
                                                      negligibels++;
                                                if( negligibels == 3*k0_max*Irreps0[i0].rho )
                                                {
                                                   //cout << " ALL neglibiles" << endl;
                                                   continue;
                                                }

						//outfile << n1 << " " << n2 << " " << n3 << " " << n4;
						//outfile << "\t\t" << FinalIrreps[iif] << " " << 2*SfSiS0[iS][0] << "\t" << InitIrreps[ii] << " " << 2*SfSiS0[iS][1] << "\t" << Irreps0[i0];
						//outfile << " " << 2*SfSiS0[iS][2] << "\n";
						for (int k0 = 0, j = 0; k0 < k0_max; k0++)
						{
							for (int rho0 = 0; rho0 < Irreps0[i0].rho; ++rho0, j += 3)
							{
                                                                /*const int resultsIdx = iS*resultsISSize + j;
								if (Negligible(results[ resultsIdx ]))
								{
									outfile << 0 << " ";
								}
								else
								{
									outfile << results[ resultsIdx ] << " ";
								}

								if (Negligible(results[ resultsIdx + 1 ]))
								{
									outfile << 0 << " ";
								}
								else
								{
									outfile << results[ resultsIdx + 1] << " ";
								}

								if (Negligible(results[ resultsIdx + 2]))
								{
									outfile << 0 << "\n";
								}
								else
								{
									outfile << results[ resultsIdx + 2] << "\n";
								}*/
                                                                							
#pragma omp critical
								{
									results_iS_wf_wi_w0.push_back(iS);
									results_iS_wf_wi_w0.push_back(FinalIrreps[iif].lm);
									results_iS_wf_wi_w0.push_back(FinalIrreps[iif].mu);
									results_iS_wf_wi_w0.push_back(InitIrreps[ii].lm);
									results_iS_wf_wi_w0.push_back(InitIrreps[ii].mu);
									results_iS_wf_wi_w0.push_back(Irreps0[i0].lm);
									results_iS_wf_wi_w0.push_back(Irreps0[i0].mu);
									results_coeffs.insert(results_coeffs.end(), &results[index], &results[index + 3*k0_max*rho0_max]);
									ntensors++;
								}
							

							}
						}
					}
                                        delete[] results;
				}
			}
		}
               	results_ntensors.push_back(ntensors);

}
                /*processMatrixElement( me_data,
                                      nmax,
                                      IsIncluded,
                                      strTensor,
                                      n1, n2, n3, n4,
                                      results_ntensors,
                                      results_iS_wf_wi_w0,
                                      results_coeffs );*/

template< typename SelectionRules >
void 
JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::
PerformSU3Decomposition( JCoupledProtonNeutron2BMe_Hermitian_SingleOperator& JCoupledPNME,
                         int Nmax,
                         int valence_shell,
                         int nmax,
                         const string& sOutputFileName,
                         const int allShells )
{
   int my_rank( 0 ), nprocs( 1 );
#ifdef HAVE_MPI
   MPI_Comm_size( MPI_COMM_WORLD, &nprocs );
   MPI_Comm_rank( MPI_COMM_WORLD, &my_rank );
   //cout << "Setting my rank to " << my_rank << endl;
#endif


#ifdef HAVE_CUDA
   strengthsTensor< cuda > strTensor( MAX_LOGFACT );
#else
   strengthsTensor< host > strTensor( MAX_LOGFACT );
#endif
	std::ofstream outfile(sOutputFileName.c_str());
	if (!outfile)
	{
		std::cerr << "Unable to open output file '" << sOutputFileName << "'" << std::endl;
		exit(EXIT_FAILURE);
	}
	outfile.precision(10);

	SelectionRules IsIncluded;

	enum {kA = 0, kB = 1, kC = 2, kD = 3};

	vector<uint16_t> lalb_indices; // (la, lb): lalb_indices[la*lb_max + lb] ---> <(na 0) la; (nb 0) lb || wf * *>
	vector<uint16_t> ldlc_indices; // (ld, lc): lalb_indices[ld*lc_max + lc] ---> <(0 nd) ld; (0 nc) lc || wi * *>
	vector<uint16_t> lfli_indices;// (Lf, Li): lflf_indices[Lf*Li_max + Li] ---> {<wf * Lf; wi * Li || w0 * L0=0>_{*}, <wf * Lf; wi * Li || w0 * L0=1>_{*}, <wf * Lf; wi * Li || w0 * L0=2>_{*} }

	vector<double> abf_cgSU3; // <(na 0) * (nb 0) * || (lm mu)f *>
	vector<double> dci_cgSU3; // <(0 nd) * (0 nc) * || (lm mu)i *>
	vector<double> fi0_cgSU3; // <wf * wi * || w0 * L0={0,1,2}>_{*}

//	
	abf_cgSU3.reserve(1024);
	dci_cgSU3.reserve(1024);
	fi0_cgSU3.reserve(1024);

	//vector<NANBNCND> ho_shells_combinations;
	//GenerateAllCombinationsHOShells(Nmax, valence_shell, nmax, ho_shells_combinations);

	SU3::LABELS ir[4] = {SU3::LABELS(0, 0), SU3::LABELS(0, 0), SU3::LABELS(0, 0), SU3::LABELS(0, 0)};

	SU3_VEC FinalIrreps;	//	list of (lmf muf) irreps resulting from (na 0) x (nb 0)
	SU3_VEC InitIrreps;		//	list of (lmi mui) irreps resulting from (0 nd) x (0 nc)

	FinalIrreps.reserve(nmax + 1); // (na 0) x (nb 0) ---> max(na, nb) SU(3) irreps
	InitIrreps.reserve(nmax + 1);
//	iterate over {na, nb, nc, nd} HO shell quantum numbers
//	for (int ishells = 0; ishells < ho_shells_combinations.size(); ++ishells)

        std::vector<NANBNCND> nanbncnd;

        std::vector<uint32_t> results_ntensors;
	// array with tensor labels: iS lmf muf lmi mui lm0 mu0
	std::vector<uint8_t> results_iS_wf_wi_w0;
	std::vector<double> results_coeffs;

        int ishells( 0 ); // just counter
        typedef INTERACTION_DATA::iterator ME_ITER;
        for (ME_ITER nanbncnd_tbme = interaction_data_.begin(); nanbncnd_tbme != interaction_data_.end(); ++nanbncnd_tbme)
	{
           ishells ++;
		//ir[kA].lm = ho_shells_combinations[ishells][kA];
		//ir[kB].lm = ho_shells_combinations[ishells][kB];
		//ir[kC].mu = ho_shells_combinations[ishells][kC];
		//ir[kD].mu = ho_shells_combinations[ishells][kD];

                ir[kA].lm = nanbncnd_tbme->first[kNA];
		ir[kB].lm = nanbncnd_tbme->first[kNB];
		ir[kC].mu = nanbncnd_tbme->first[kNC];
		ir[kD].mu = nanbncnd_tbme->first[kND];

		int n1 = ir[kA].lm;
		int n2 = ir[kB].lm;
		int n3 = ir[kD].mu;
		int n4 = ir[kC].mu;

		if (!IsIncluded(n1, n2, n3, n4)) // operator does not have this combination of n1 n2 n3 n4
		{
			continue;
		}

		double* meV;
		uint16_t* labels;
		int me_phase;
//		Obtain a set of matrix elements {<na* nb*|| {Vpp, Vnn, Vpn} || nc* nd*>}
		//ME_DATA me_data = JCoupledPNME.GetMe(ho_shells_combinations[ishells]);
                ME_DATA me_data = nanbncnd_tbme->second;

		SIZE_t size = me_data.size;
		I1_t i1 = me_data.i1;
		I2_t i2 = me_data.i2;
		I3_t i3 = me_data.i3;
		I4_t i4 = me_data.i4;
		J_t  J   = me_data.J;
		VPP_t Vpp  = me_data.Vpp;
		VNN_t Vnn  = me_data.Vnn;
		VPN_t Vpn  = me_data.Vpn;
          
               cout << "PID: " << my_rank << " Time: " << defaultTimer.GetTime() << " >> Processing " << ishells << " / " << allShells 
                   << " => " << ( double ) ishells / ( double ) allShells * 100.0 << " % done ... Matrix elements: " << size << "           \n" << flush;


		if (size == 0)
		{
			continue;
		}

		//std::cout << ir[kA] << " " << ir[kB] << " " << ir[kC] << " " << ir[kD] << "\r" << std::flush;
	
		FinalIrreps.clear(); InitIrreps.clear();
		SU3::Couple(ir[kA], ir[kB], FinalIrreps);	//	(na 0) x (nb 0) -> rho=1{(lmf muf)}
		SU3::Couple(ir[kD], ir[kC], InitIrreps);	//	(0 nd) x (0 nc) -> rho=1{(lmi mui)}	

//	iterate over (lmf muf) irreps	
                uint32_t ntensors = 0;	
		for (int iif = 0; iif < FinalIrreps.size(); ++iif)
		{
			ComputeSU3CGs_Simple(ir[kA], ir[kB], FinalIrreps[iif], lalb_indices, abf_cgSU3);

//	iterate over (lmi mui) irreps		
			for (int ii = 0; ii < InitIrreps.size(); ++ii)
			{
				ComputeSU3CGs_Simple(ir[kD], ir[kC], InitIrreps[ii], ldlc_indices, dci_cgSU3);

				SU3_VEC Irreps0;
				SU3::Couple(FinalIrreps[iif], InitIrreps[ii], Irreps0); // (lmf muf) x (lmi mui) -> rho_{0} (lm0 mu0)
//	iterate over rho(lmu0 mu0)
				for (int i0 = 0; i0 < Irreps0.size(); ++i0)
				{                                   
					if (!IsIncluded(Irreps0[i0])) // Irrep0 does not have tensorial character of the operator
					{
						continue;
					}
                                        int rho0_max = Irreps0[i0].rho;
                                        double wfwiw0_results[6*3*2*9];
				        memset(wfwiw0_results, 0, sizeof(double)*6*3*2*rho0_max);

		                        //std::cout << ir[kA] << " " << ir[kB] << " " << ir[kC] << " " << ir[kD] 
					//          << "\t" << (int) Irreps0[i0].rho << " " << (int) Irreps0[i0].lm << " " << (int) Irreps0[i0].mu << " \r" << std::flush;

					ComputeSU3CGs(FinalIrreps[iif], InitIrreps[ii], Irreps0[i0], lfli_indices, fi0_cgSU3);
/*	
 *	Scalar opetator [i.e. J0=0] has L0==S0. Since we are dealing with
 *	a two-body interaction ==> S0 = 0, 1, or 2.
 *	S0=0 appears 2 times: Sf=0 x Si=0 --> S0=0 and Sf=1 x Si=1 --> S0=0
 *	==> 2 x SU3::kmax(w0, L0=0) x rho0_max x 3
 *
 *	S0=1 appears 3 times: Sf=1 x Si=0 --> S0=1; Sf=0 x Si=1 --> S0=0; Sf=1 x Si=1 ---> S0=1
 *	==> 3 x SU3::kmax(w0, L0=1) x rho0_max x 3
 *
 *	S0=2 appears 1 times: Sf=1 x Si=1 --> S0=2
 *	==> 1 x SU3::kmax(w0, L0=1) x rho0_max x 3
 *
 *	Two body interaciton has L0=S0 and  can be either 0, 1, or 2.
 *
 */	
//					int nresults = (2*(SU3::kmax(Irreps0[i0], 0)) + 3*SU3::kmax(Irreps0[i0], 1) + SU3::kmax(Irreps0[i0], 2))*Irreps0[i0].rho*3;
//	Order of elements in results: results[iS, k0, rho0, type], where iS = 0 ... 6 [= number of Sf Si S0 quantum numbers for two-body interaction]
//	index = iS x k0_max x rho0_max x 3 + k0 x rho0_max x 3 + rho0 x 3 + type
					/*std::vector<double> results[6];
					results[0].resize(3*SU3::kmax(Irreps0[i0], 0)*Irreps0[i0].rho);
					results[1].resize(3*SU3::kmax(Irreps0[i0], 0)*Irreps0[i0].rho);
					results[2].resize(3*SU3::kmax(Irreps0[i0], 1)*Irreps0[i0].rho);
					results[3].resize(3*SU3::kmax(Irreps0[i0], 1)*Irreps0[i0].rho);
					results[4].resize(3*SU3::kmax(Irreps0[i0], 1)*Irreps0[i0].rho);
					results[5].resize(3*SU3::kmax(Irreps0[i0], 2)*Irreps0[i0].rho);*/
                                        // alokovat 6 x 3 x 2 x Irreps0[i0].rho
                                        int resultsISSize = 3*2*Irreps0[i0].rho;
                                        int resultsSize = 6*resultsISSize;
                                        double* results = new double[ resultsSize ];
                                        for( int resultIdx = 0; resultIdx < resultsSize; resultIdx++ )
                                           results[ resultIdx ] = 0.0;
                                       
                                        strengthsTensorData 
                                           tensorData( FinalIrreps[iif], InitIrreps[ii], Irreps0[i0], \
						       size, i1, i2, i3, i4, J, Vpp, Vnn, Vpn, \
						       &lalb_indices[0], lalb_indices.size(), \
                                                       &ldlc_indices[0], ldlc_indices.size(), \
                                                       &lfli_indices[0], lfli_indices.size(), \
						       &abf_cgSU3[0], abf_cgSU3.size(), \
                                                       &dci_cgSU3[0], dci_cgSU3.size(), \
                                                       &fi0_cgSU3[0], fi0_cgSU3.size(), \
                                                       &SfSiS0[0][0], \
                                                       &PiSfSiS0[0], \
						       results, \
                                                       resultsISSize );
                                        if( ! strTensor.compute( tensorData ) )
                                        {
                                           return;
                                        }
                                          
					for (int iS = 0, index = 0; iS < 6; ++iS, index += 3*2*rho0_max )
					{
						int k0_max = SU3::kmax(Irreps0[i0], SfSiS0[iS][2]);
						if (!k0_max)
						{
							continue;
						}
                                                
						/*if (std::count_if(results[iS].begin(), results[iS].end(), Negligible) == 3*k0_max*Irreps0[i0].rho)
							continue;*/
                                                int negligibels( 0 );
                                                for( int resultsIdx = 0; resultsIdx < 3*k0_max*Irreps0[i0].rho; resultsIdx++ )
                                                   if( Negligible( results[ iS*resultsISSize + resultsIdx ] ) )
                                                      negligibels++;
                                                if( negligibels == 3*k0_max*Irreps0[i0].rho )
                                                {
                                                   //cout << " ALL neglibiles" << endl;
                                                   continue;
                                                }

						outfile << n1 << " " << n2 << " " << n3 << " " << n4;
						outfile << "\t\t" << FinalIrreps[iif] << " " << 2*SfSiS0[iS][0] << "\t" << InitIrreps[ii] << " " << 2*SfSiS0[iS][1] << "\t" << Irreps0[i0];
						outfile << " " << 2*SfSiS0[iS][2] << "\n";
						for (int k0 = 0, j = 0; k0 < k0_max; k0++)
						{
							for (int rho0 = 0; rho0 < Irreps0[i0].rho; ++rho0, j += 3)
							{
                                                                /*const int resultsIdx = iS*resultsISSize + j;
								if (Negligible(results[ resultsIdx ]))
								{
									outfile << 0 << " ";
								}
								else
								{
									outfile << results[ resultsIdx ] << " ";
								}

								if (Negligible(results[ resultsIdx + 1 ]))
								{
									outfile << 0 << " ";
								}
								else
								{
									outfile << results[ resultsIdx + 1] << " ";
								}

								if (Negligible(results[ resultsIdx + 2]))
								{
									outfile << 0 << "\n";
								}
								else
								{
									outfile << results[ resultsIdx + 2] << "\n";
								}*/
                                                                							
#pragma omp critical
								{
									results_iS_wf_wi_w0.push_back(iS);
									results_iS_wf_wi_w0.push_back(FinalIrreps[iif].lm);
									results_iS_wf_wi_w0.push_back(FinalIrreps[iif].mu);
									results_iS_wf_wi_w0.push_back(InitIrreps[ii].lm);
									results_iS_wf_wi_w0.push_back(InitIrreps[ii].mu);
									results_iS_wf_wi_w0.push_back(Irreps0[i0].lm);
									results_iS_wf_wi_w0.push_back(Irreps0[i0].mu);
									results_coeffs.insert(results_coeffs.end(), &results[index], &results[index + 3*k0_max*rho0_max]);
									ntensors++;
								}
							

							}
						}
					}
                                        delete[] results;
				}
			}
		}
               	results_ntensors.push_back(ntensors);
		nanbncnd.push_back(nanbncnd_tbme->first);

	}

#ifdef TIMING	
	if (my_rank ==0)
	{
	//	cout << "Computing <wf wi || w0>:" << total_duration_SU3_3 << "\t" << "\t Transformation:" << total_duration_transformation << endl;
	}
#endif

#ifdef HAVE_MPI
	//mpi_comm_world.barrier();
        MPI_Barrier( MPI_COMM_WORLD );
#endif

	if (my_rank == 0)
	{
		std::ofstream outfile(sOutputFileName.c_str());
		if (!outfile)
		{
			std::cerr << "Unable to open output file '" << sOutputFileName << "'" << std::endl;
			exit(EXIT_FAILURE);
		}
		outfile.precision(10);

		std::vector<std::vector<NANBNCND> > all_nanbncnd;
		std::vector<std::vector<uint32_t> > all_results_ntensors;
		std::vector<std::vector<uint8_t> >  all_results_iS_wf_wi_w0;
		std::vector<std::vector<double> >   all_results_coeffs;

		all_nanbncnd.resize(nprocs);
		all_results_ntensors.resize(nprocs);
		all_results_iS_wf_wi_w0.resize(nprocs);
		all_results_coeffs.resize(nprocs);


#ifdef HAVE_MPI
                all_nanbncnd[ 0 ].resize( nanbncnd.size() );
                for( int j = 0; j < nanbncnd.size(); j ++ )
                   all_nanbncnd[ 0 ][ j ] = nanbncnd[ j ];
                for( int i = 1; i < nprocs; i++ )
                {
                   MPI_Status status;
                   size_t size;
                   MPI_Recv( &size, sizeof( size_t ), MPI_BYTE, i, 0, MPI_COMM_WORLD, &status );
                   all_nanbncnd[ i ].resize( size );
                   MPI_Recv( all_nanbncnd[ i ].data(), size * NANBNCND::sizeOf(), MPI_BYTE, i, 0, MPI_COMM_WORLD, &status );
                }

                all_results_ntensors[ 0 ].resize( results_ntensors.size() );
                for( int j = 0; j < results_ntensors.size(); j++ )
                   all_results_ntensors[ 0 ][ j ] = results_ntensors[ j ];
                for( int i = 1; i < nprocs; i++ )
                {
                   MPI_Status status;
                   size_t size;
                   MPI_Recv( &size, sizeof( size_t ), MPI_BYTE, i, 0, MPI_COMM_WORLD, &status );
                   all_results_ntensors[ i ].resize( size );
                   MPI_Recv( all_results_ntensors[ i ].data(), size * sizeof( uint32_t ), MPI_BYTE, i, 0, MPI_COMM_WORLD, &status );
                }

                all_results_iS_wf_wi_w0[ 0 ].resize( results_iS_wf_wi_w0.size() );
                for( int j = 0; j < results_iS_wf_wi_w0.size(); j++ )
                   all_results_iS_wf_wi_w0[ 0 ][ j ] = results_iS_wf_wi_w0[ j ];
                for( int i = 1; i < nprocs; i++ )
                {
                   MPI_Status status;
                   size_t size;
                   MPI_Recv( &size, sizeof( size_t ), MPI_BYTE, i, 0, MPI_COMM_WORLD, &status );
                   all_results_iS_wf_wi_w0[ i ].resize( size );
                   MPI_Recv( all_results_iS_wf_wi_w0[ i ].data(), size * sizeof( uint8_t ), MPI_BYTE, i, 0, MPI_COMM_WORLD, &status );
                }
                
                all_results_coeffs[ 0 ].resize( results_coeffs.size() );
                for( int j = 0; j < results_coeffs.size(); j++ )
                   all_results_coeffs[ 0 ][ j ] = results_coeffs[ j ];
                for( int i = 1; i < nprocs; i++ )
                {
                   MPI_Status status;
                   size_t size;
                   MPI_Recv( &size, sizeof( size_t ), MPI_BYTE, i, 0, MPI_COMM_WORLD, &status );
                   all_results_coeffs[ i ].resize( size );
                   MPI_Recv( all_results_coeffs[ i ].data(), size * sizeof( double ), MPI_BYTE, i, 0, MPI_COMM_WORLD, &status );
                }

		SaveOnDisk(all_nanbncnd, all_results_ntensors, all_results_iS_wf_wi_w0, all_results_coeffs, outfile);
#else
		all_nanbncnd.push_back( nanbncnd );
		all_results_ntensors.push_back( results_ntensors );
		all_results_iS_wf_wi_w0.push_back( results_iS_wf_wi_w0 );
		all_results_coeffs.push_back( results_coeffs );
		SaveOnDisk(all_nanbncnd, all_results_ntensors, all_results_iS_wf_wi_w0, all_results_coeffs, outfile);
#endif
	}
	else
	{
#ifdef HAVE_MPI           
                size_t size = nanbncnd.size();
                MPI_Send( &size, sizeof( size_t ), MPI_BYTE, 0, 0, MPI_COMM_WORLD );
                MPI_Send( nanbncnd.data(), size * NANBNCND::sizeOf(), MPI_BYTE, 0, 0, MPI_COMM_WORLD );

                size = results_ntensors.size();
                MPI_Send( &size, sizeof( size_t ), MPI_BYTE, 0, 0, MPI_COMM_WORLD );
                MPI_Send( results_ntensors.data(), size * sizeof( uint32_t ), MPI_BYTE, 0, 0, MPI_COMM_WORLD );

                size = results_iS_wf_wi_w0.size();
                MPI_Send( &size, sizeof( size_t ), MPI_BYTE, 0, 0, MPI_COMM_WORLD );
                MPI_Send( results_iS_wf_wi_w0.data(), size * sizeof( uint8_t ), MPI_BYTE, 0, 0, MPI_COMM_WORLD );
                
                size = results_coeffs.size();
                MPI_Send( &size, sizeof( size_t ), MPI_BYTE, 0, 0, MPI_COMM_WORLD );
                MPI_Send( results_coeffs.data(), size * sizeof( double ), MPI_BYTE, 0, 0, MPI_COMM_WORLD );
#endif
	}

        //outfile.close();
}

void JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::
SaveOnDisk( std::vector<std::vector<NANBNCND> >& all_nanbncnd,
            std::vector<std::vector<uint32_t> >& all_results_ntensors,
	    std::vector<std::vector<uint8_t> >&  all_results_iS_wf_wi_w0,
	    std::vector<std::vector<double> >&   all_results_coeffs,
	    std::ofstream& outfile )
{
	assert(all_nanbncnd.size() == all_results_ntensors.size() == all_results_iS_wf_wi_w0.size() == all_results_coeffs.size());

	for (int rank = 0; rank < all_nanbncnd.size(); ++rank)
	{
		uint32_t fi0_index = 0;
		uint32_t coeff_index = 0;
		for (int k = 0; k < all_nanbncnd[rank].size(); ++k)
		{
			int na = all_nanbncnd[rank][k][kNA];
			int nb = all_nanbncnd[rank][k][kNB];
			int nc = all_nanbncnd[rank][k][kNC];
			int nd = all_nanbncnd[rank][k][kND];

			uint32_t ntensors = all_results_ntensors[rank][k];

			for (int itensor = 0; itensor < ntensors; ++itensor, fi0_index += 7)
			{
				int iS  = all_results_iS_wf_wi_w0[rank][fi0_index];
				int SSf = 2*SfSiS0[iS][0];
				int SSi = 2*SfSiS0[iS][1];
				int SS  = 2*SfSiS0[iS][2];
				int lmf = all_results_iS_wf_wi_w0[rank][fi0_index + 1];
				int muf = all_results_iS_wf_wi_w0[rank][fi0_index + 2];
				int lmi = all_results_iS_wf_wi_w0[rank][fi0_index + 3];
				int mui = all_results_iS_wf_wi_w0[rank][fi0_index + 4];
				int lm0 = all_results_iS_wf_wi_w0[rank][fi0_index + 5];
				int mu0 = all_results_iS_wf_wi_w0[rank][fi0_index + 6];

				SU3xSU2::LABELS wf(1, lmf, muf, SSf);
				SU3xSU2::LABELS wi(1, lmi, mui, SSi);
				SU3xSU2::LABELS w0(1, lm0, mu0, SS);
				w0.rho = SU3::mult(wf, wi, w0);

				outfile << na << " " << nb << " " << nd << " " << nc;
				outfile << "\t\t" << wf //<< " " << ( int ) wf.S2
                                        << "\t" << wi //<< " " << ( int ) wi.S2
                                        << "\t" << w0 //<< " " << ( int ) w0.S2
                                        << "\n";
			
				int k0_max = SU3::kmax(w0, w0.S2/2);
				assert(k0_max);

				double* coeffs  = &all_results_coeffs[rank][coeff_index];
				for (int k0 = 0, j = 0; k0 < k0_max; k0++)
				{
					for (int rho0 = 0; rho0 < w0.rho; ++rho0, j += 3)
					{
						if (Negligible(coeffs[j]))
						{
							outfile << 0 << " ";
						}
						else
						{
							outfile << coeffs[j] << " ";
						}

						if (Negligible(coeffs[j + 1]))
						{
							outfile << 0 << " ";
						}
						else
						{
							outfile << coeffs[j + 1] << " ";
						}

						if (Negligible(coeffs[j + 2]))
						{
							outfile << 0 << "\n";
						}
						else
						{
							outfile << coeffs[j + 2] << "\n";
						}
					}
				}
				coeff_index += 3*k0_max*w0.rho;
			}
		}
	}
}
#endif
