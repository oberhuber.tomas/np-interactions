#ifndef su2_H
#define su2_H

#include <stdlib.h>

namespace SU2
{
   typedef unsigned char LABEL;
   typedef char CANONICAL;


#ifdef HAVE_CUDA
__host__ __device__
#endif
   inline size_t mult( const SU2::LABEL& S1,
                       const SU2::LABEL& S2,
                       const SU2::LABEL& S3 )
   {
      return (S3 >= abs(S1 - S2) && S3 <= S1 + S2) && !((S1 + S2 + S3) & 0x0001);
   }
}

#endif
