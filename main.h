#include "hoShellsCombinations.h"
#include "timer.h"
#include "selectionRules.h"
#include "computeSU3CGs.h"
#include "jCoupledProtonNeutron2BMeHermitianSingleOperator.h"
#include "config.h"

#ifdef HAVE_MPI
#include <mpi.h>
#endif


#include <sys/types.h>
#include <unistd.h>

int main(int argc, char* argv[])
{
   int mpiRank( 0 ), nprocs( 0 );
#ifdef HAVE_MPI
   MPI_Init( &argc, &argv );
   MPI_Comm_rank( MPI_COMM_WORLD, &mpiRank );
   MPI_Comm_size( MPI_COMM_WORLD, &nprocs );
#endif   
/** \todo nmax could be obtained from input_file */	

   if( ! config.parseCommandLine( argc, argv ) )
      return EXIT_FAILURE;
       
   int nmax = config.Nmax + config.valenceShell;
   
   /****
    * Constructor of CBlocks calls blocks_() function of su3lib which is required
    * for the latter to work properly
    */
   //CBlocks blocks;	
   blocks_();

   /*****
    * Debug mode -- it allows to attach particular processes by gdb:
    * The processes wait for the master process which can be started
    * by entering
    * 
    * set var i = 1
    *
    * in gdb.
    */
   if( config.debug )
   {
#ifdef HAVE_MPI
      int i = 0;
      char hostname[256];
      gethostname(hostname, sizeof(hostname));
      for( int i = nprocs - 1; i >= 0; i-- )
      {
         if( mpiRank == i )
            cout << "Process " << i << " on " << hostname << " can be attached using PID " << getpid() << flush << endl;
         MPI_Barrier( MPI_COMM_WORLD );
      }
      if( mpiRank == 0 )
         while( i == 0 )
            sleep( 5 );
      MPI_Barrier( MPI_COMM_WORLD );
#endif            
   }
   
   if( mpiRank == 0 )
   {
#ifdef HAVE_MPI      
      cout << "Using MPI ..." << endl;
#endif      
#ifdef HAVE_CUDA
      cout << "Using CUDA ..." << endl;
#endif      
      /****
       * Create a list of {na nb nc nd} HO shells that are allowed by
       * CInteractionSelectionRules.
       */
      cout << " Time: " << defaultTimer.GetTime() << " >> Generating HO Shels combinations...." << endl;
      vector<NANBNCND> ho_shells_combinations;	
      GenerateAllCombinationsHOShells< ParityPreserving >( config.Nmax, config.valenceShell, nmax, ho_shells_combinations);
      cout << " Time: " << defaultTimer.GetTime() << " >> Generating HO Shels combinations.... done" << endl;

      cout << " Time: " << defaultTimer.GetTime() << " >> Reading interactions...." << endl;
      JCoupledProtonNeutron2BMe_Hermitian_SingleOperator JCoupledPNME( config.inputFile, ho_shells_combinations );
      cout << " Time: " << defaultTimer.GetTime() << " >> Reading interactions.... done" << endl;

      // JCoupledPNME.Show();
      JCoupledPNME.SwitchHOConvention();
      JCoupledPNME.MultiplyByCommonFactor();     
      cout << " Time: " << defaultTimer.GetTime() << " >> Performing SU3 decompostion...." << endl;
      JCoupledPNME.PerformSU3Decomposition< Cinteraction_sel_rules >( config.Nmax, config.valenceShell, nmax, config.outputFile );
      cout << " Time: " << defaultTimer.GetTime() << " >> Performing SU3 decompostion.... done" << endl;
   }
   else
   {
      JCoupledProtonNeutron2BMe_Hermitian_SingleOperator JCoupledPNME;
      JCoupledPNME.PerformSU3Decomposition< Cinteraction_sel_rules >( config.Nmax, config.valenceShell, nmax, string("") );
   }
#ifdef HAVE_MPI
      MPI_Barrier( MPI_COMM_WORLD );
      MPI_Finalize();
#endif
   return EXIT_SUCCESS;
}
