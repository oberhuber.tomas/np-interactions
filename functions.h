#ifndef functionsH
#define functionsH

#include <math.h>
#include <stdlib.h>

#ifdef HAVE_CUDA
__device__ __host__
#endif
inline int MINUSto( int power ) 
{ 
   return ( ( power % 2 ) != 0 ) ? -1.0 : 1.0; 
}

#ifdef HAVE_CUDA
__device__ __host__
#endif
inline bool Negligible7( double val )
{
   return fabs( val ) < 1.0e-7;
}

#ifdef HAVE_CUDA
__device__ __host__
#endif
inline bool Negligible6( double val )
{ 
   return fabs( val ) < 1.0e-6;
}

#ifdef HAVE_CUDA
__device__ __host__
#endif
inline bool Negligible( double val )
{ 
   return Negligible6( val );
}

#ifdef HAVE_CUDA
__device__ __host__
#endif
inline bool isOdd( int a )
{
    return ( a & 0x0001 == 1 );
}

#ifdef HAVE_CUDA
__device__ __host__
#endif
inline bool isAllowed(int a2, int b2, int c2)
{
    return (!isOdd(a2 + b2 + c2) && c2 >= abs(a2 - b2) && c2 <= a2 + b2 );
}

template< typename T >
#ifdef HAVE_CUDA
__device__ __host__
#endif
inline T max( const T& a, const T& b )
{
   return a > b ? a :b;
}

template< typename T >
#ifdef HAVE_CUDA
__device__ __host__
#endif
inline T min( const T& a, const T& b )
{
   return a < b ? a :b;
}

#endif
