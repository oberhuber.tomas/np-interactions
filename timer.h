#ifndef tnlTimerRTH
#define tnlTimerRTH

class tnlTimerRT
{
   public:

   tnlTimerRT();

   void Reset();

   void Stop();

   void Continue();

   double GetTime();

   protected:

   double initial_time;

   double total_time;

   bool stop_state;
};

extern tnlTimerRT defaultTimer;
#endif



