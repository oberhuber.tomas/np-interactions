include Makefile.inc

HEADERS = \
          CTuple.h \
	  functions.h \
	  racah.h \
	  su2.h \
	  su3.h \
	  un.h \
	  np-interactions.h \
	  checkCUDADevice.h \
	  logFactTable.h \
	  device.h \
	  deviceParams.h \
	  get-time.h \
	  logDelta2.h \
	  strengthsTensor.h \
	  strengthsTensorData.h \
	  StrengthsTensorDataBuffer.h \
          timer.h \
          main.h

SOURCES = \
          main.cpp \
          main-cuda.cu \
          factorial.cpp \
	  checkCUDADevice.cu \
          timer.cpp \
          config.cpp \
	  $(FORTRAN_SOURCES)

TEST_SOURCES = \
          tests/cuda-logfact-test.cu \
          tests/cuda-delta2-test.cu \
          tests/cuda-racah-test.cu \
          tests/tensor-strengths-test.h \
          tests/tensor-strengths-test.cpp \
          tests/cuda-tensor-strengths-test.cu 

FORTRAN_SOURCES = \
	  fortran/blocks.F \
	  fortran/conmat.F \
	  fortran/ijfact.F \
	  fortran/u3mult.F \
	  fortran/wu3r3w.F \
	  fortran/xewu3.F \
	  fortran/xwu3r3.F \
	  fortran/dtu3r3.F \
	  fortran/qtu3r3.F \
	  fortran/multu3.F \
	  fortran/multhy.F \
	  fortran/xewu3s.F \
	  fortran/dwr3.F \
	  fortran/delta.F

FORTRAN_OBJECTS = \
	  fortran/blocks.o \
	  fortran/conmat.o \
	  fortran/ijfact.o \
	  fortran/u3mult.o \
	  fortran/wu3r3w.o \
	  fortran/xewu3.o \
	  fortran/xwu3r3.o \
	  fortran/dtu3r3.o \
	  fortran/qtu3r3.o \
	  fortran/multu3.o \
	  fortran/multhy.o \
	  fortran/xewu3s.o \
	  fortran/dwr3.o \
	  fortran/delta.o

OBJECTS = \
	  $(FORTRAN_OBJECTS) \
          timer.o \
          transformationResult.o \
          config.o

CUDA_OBJECTS = \
	  checkCUDADevice.o \
	  StrengthsTensorDataBufferCuda.o \
	  $(OBJECTS)

NOCUDA_OBJECTS = \
          main.o \
          StrengthsTensorDataBuffer.o \
	  $(OBJECTS)

DIST = $(SOURCES) $(HEADERS) $(TEST_SOURCES) Makefile Makefile.inc

all: $(TARGET) tests

ifdef WITH_CUDA
tests: cuda-logfact-test \
       cuda-logdelta2-test \
       cuda-racah-test \
       cuda-wigner6j-test \
       cuda-wigner9j-test 
#       tensor-strengths-test 
#       cuda-tensor-strengths-test
else
tests:
endif

dist: $(DIST)
	tar zcvf $(TARGET).tgz $(DIST) 

install: $(TARGET)
	cp $(TARGET) $(INSTALL_DIR)/bin

uninstall: $(TARGET)
	rm -f $(INSTALL_DIR)/bin/$(TARGET) 

ifdef WITH_CUDA
$(TARGET): $(CUDA_OBJECTS) main-cuda.o
	$(CXX) $(CUDA_OBJECTS) main-cuda.o $(CUDA_LD_FLAGS) -o $(TARGET)

cuda-logfact-test: $(CUDA_OBJECTS) tests/cuda-logfact-test.o
	$(CXX) tests/cuda-logfact-test.o $(CUDA_OBJECTS) $(CUDA_LD_FLAGS) -o tests/cuda-logfact-test

cuda-logdelta2-test: $(CUDA_OBJECTS) tests/cuda-logdelta2-test.o
	$(CXX) tests/cuda-logdelta2-test.o $(CUDA_OBJECTS) $(CUDA_LD_FLAGS) -o tests/cuda-logdelta2-test

cuda-racah-test: $(CUDA_OBJECTS) tests/cuda-racah-test.o
	$(CXX) tests/cuda-racah-test.o $(CUDA_OBJECTS) $(CUDA_LD_FLAGS) -o tests/cuda-racah-test

cuda-wigner6j-test: $(CUDA_OBJECTS) tests/cuda-wigner6j-test.o
	$(CXX) tests/cuda-wigner6j-test.o $(CUDA_OBJECTS) $(CUDA_LD_FLAGS) -o tests/cuda-wigner6j-test

cuda-wigner9j-test: $(CUDA_OBJECTS) tests/cuda-wigner9j-test.o
	$(CXX) tests/cuda-wigner9j-test.o $(CUDA_OBJECTS) $(CUDA_LD_FLAGS) -o tests/cuda-wigner9j-test

#tensor-strengths-test: $(OBJECTS) tests/tensor-strengths-test.o
#	$(CXX) tests/tensor-strengths-test.o $(OBJECTS) $(LD_FLAGS) -o tests/tensor-strengths-test

#cuda-tensor-strengths-test: $(CUDA_OBJECTS) tests/cuda-tensor-strengths-test.o
#	$(CXX) tests/cuda-tensor-strengths-test.o $(CUDA_OBJECTS) $(CUDA_LD_FLAGS) -o tests/cuda-tensor-strengths-test

clean: 
	rm -f $(CUDA_OBJECTS) main-cuda.o
else
$(TARGET): $(NOCUDA_OBJECTS) main.o
	$(CXX) $(NOCUDA_OBJECTS) $(LD_FLAGS) -o $(TARGET)

clean: 
	rm -f $(NOCUDA_OBJECTS) main.o
endif

%.o: %.cpp
	$(CXX) -c -o $@ $(CXX_FLAGS) $<

%.o: %.cu
	$(CUDA_CXX) -c -o $@ $(CUDA_CXX_FLAGS) $<

%.o: %.F
	$(FORTRAN) -c -o $@ $(FORTRAN_FLAGS) $<
