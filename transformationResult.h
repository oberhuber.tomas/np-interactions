/***************************************************************************
                          transformationResult.h  -  description
                             -------------------
    begin                : 2015/01/16
    copyright            : (C) 2015 by Tomá¹ Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef transformationResultH
#define transformationResultH

#include <assert.h>
#include <vector>
#include <iostream>
#ifdef HAVE_MPI
   #include <mpi.h>
#endif

#include "su3xsu2.h"   

// TODO: do this the right way
extern int SfSiS0[6][3]; // =  {{0, 0, 0}, {1, 1, 0}, {0, 1, 1}, {1, 0, 1}, {1, 1, 1},      {1, 1, 2}};
extern double PiSfSiS0[6];  // = {   1,         3,         3,         3,    sqrt(27),         sqrt(45)};

extern int* SfSiS0_device;
extern double* PiSfSiS0_device;

class transformationResult
{
   public:

   transformationResult()
   : na( 0 ), nb( 0 ), nc( 0 ), nd( 0 ), ntensors( 0 )
#ifdef HAVE_MPI
   , recvStatus( - 1 )
#endif
   {};

   transformationResult( int na, int nb, int nc, int nd )
   : na( na ), nb( nb ), nc( nc ), nd( nd ), ntensors( 0 )
#ifdef HAVE_MPI
   , recvStatus( - 1 )
#endif
   {};

   void write( std::ostream& str )
   {
      int fi0_index( 0 ), coeff_index( 0 );
      for( int itensor = 0; itensor < this->ntensors; ++itensor, fi0_index += 7 )
      {
         int iS  = this->iS_wf_wi_w0[ fi0_index ]; 
         int SSf = 2 * SfSiS0[ iS ][ 0 ];
         int SSi = 2 * SfSiS0[ iS ][ 1 ];
         int SS  = 2 * SfSiS0[ iS ][ 2 ];
              
         int lmf = this->iS_wf_wi_w0[ fi0_index + 1 ];
         int muf = this->iS_wf_wi_w0[ fi0_index + 2 ];
         int lmi = this->iS_wf_wi_w0[ fi0_index + 3 ];
         int mui = this->iS_wf_wi_w0[ fi0_index + 4 ];
         int lm0 = this->iS_wf_wi_w0[ fi0_index + 5 ];
         int mu0 = this->iS_wf_wi_w0[ fi0_index + 6 ];

         SU3xSU2::LABELS wf( 1, lmf, muf, SSf );
         SU3xSU2::LABELS wi( 1, lmi, mui, SSi );
         SU3xSU2::LABELS w0( 1, lm0, mu0, SS) ;
         w0.rho = SU3::mult( wf, wi, w0 );

         str << na << " " << nb << " " << nd << " " << nc;
         str << "\t\t" << wf //<< " " << ( int ) wf.S2
             << "\t"   << wi //<< " " << ( int ) wi.S2
             << "\t"   << w0 //<< " " << ( int ) w0.S2
             << "\n";
      
         int k0_max = SU3::kmax( w0, w0.S2 / 2 );
         assert( k0_max );
               
         const double* auxCoeffs  = &this->coeffs[ coeff_index ];
         for( int k0 = 0, j = 0; k0 < k0_max; k0++ )
         {
            for( int rho0 = 0; rho0 < w0.rho; ++rho0, j += 3 )
            {
               for( int i = 0; i < 3; i++ )
               {
                  if( Negligible( auxCoeffs[ j + i ] ) )
                     str << 0;
                  else
                     str << auxCoeffs[ j + i ];
                  if( i < 2 ) 
                     str << " ";
               }
               str << std::endl;
            }
         }
         coeff_index += 3 * k0_max * w0.rho;
      }
   };

#ifdef HAVE_MPI
   void MPI_Send( int dest, int tag, MPI_Comm comm )
   {
      /*::MPI_Isend( &this->na, 1, MPI_INT, dest, tag, comm, &transferRequests[ 0 ] );
      ::MPI_Isend( &this->nb, 1, MPI_INT, dest, tag, comm, &transferRequests[ 1 ] );
      ::MPI_Isend( &this->nc, 1, MPI_INT, dest, tag, comm, &transferRequests[ 2 ] );
      ::MPI_Isend( &this->nd, 1, MPI_INT, dest, tag, comm, &transferRequests[ 3 ] );
      
      ::MPI_Isend( &this->ntensors, sizeof( uint32_t ), MPI_BYTE, dest, tag, comm, &transferRequests[ 4 ] );
      this->iS_wf_wi_w0_size = this->iS_wf_wi_w0.size();
      ::MPI_Isend( &this->iS_wf_wi_w0_size, sizeof( size_t ), MPI_BYTE, dest, tag, comm, &transferRequests[ 5 ] );
      this->coeffs_size = this->coeffs.size();
      ::MPI_Isend( &this->coeffs_size, sizeof( size_t ), MPI_BYTE, dest, tag, comm, &transferRequests[ 6 ] );
      
      ::MPI_Isend( &this->iS_wf_wi_w0[ 0 ], this->iS_wf_wi_w0_size * sizeof( uint8_t ), MPI_BYTE, dest, tag, comm, &transferRequests[ 7 ] );
      ::MPI_Isend( &this->coeffs[ 0 ], this->coeffs_size, MPI_DOUBLE, dest, tag, comm, &transferRequests[ 8 ] );*/
      
      ::MPI_Send( &this->na, 1, MPI_INT, dest, tag, comm );
      ::MPI_Send( &this->nb, 1, MPI_INT, dest, tag, comm );
      ::MPI_Send( &this->nc, 1, MPI_INT, dest, tag, comm );
      ::MPI_Send( &this->nd, 1, MPI_INT, dest, tag, comm );
      
      ::MPI_Send( &this->ntensors, sizeof( uint32_t ), MPI_BYTE, dest, tag, comm );
      
      size_t size = this->iS_wf_wi_w0.size();
      ::MPI_Send( &size, sizeof( size_t ), MPI_BYTE, dest, tag, comm );
      size = this->coeffs.size();
      ::MPI_Send( &size, sizeof( size_t ), MPI_BYTE, dest, tag, comm );
      
      size = this->iS_wf_wi_w0.size();
      ::MPI_Send( &this->iS_wf_wi_w0[ 0 ], size * sizeof( uint8_t ), MPI_BYTE, dest, tag, comm );
      size = this->coeffs.size();
      ::MPI_Send( &this->coeffs[ 0 ], size, MPI_DOUBLE, dest, tag, comm );
   }
   
   bool MPI_Finish_Send()
   {
      int flag;
      MPI_Testall( 9, transferRequests, &flag, MPI_STATUSES_IGNORE );
      if( flag ) return true;
      return false;
   }
   
   void MPI_Recv( int source, int tag, MPI_Comm comm )
   {      
      //cout << "Receving na, nb, nc, nd." << endl;
      ::MPI_Recv( &this->na, 1, MPI_INT, source, tag, comm, MPI_STATUS_IGNORE );
      ::MPI_Recv( &this->nb, 1, MPI_INT, source, tag, comm, MPI_STATUS_IGNORE );
      ::MPI_Recv( &this->nc, 1, MPI_INT, source, tag, comm, MPI_STATUS_IGNORE );
      ::MPI_Recv( &this->nd, 1, MPI_INT, source, tag, comm, MPI_STATUS_IGNORE );

      //cout << "Receving tensors" << endl;
      ::MPI_Recv( &this->ntensors, sizeof( uint32_t ), MPI_BYTE, source, tag, comm, MPI_STATUS_IGNORE );
      
      //cout << "Receving sizes" << endl;
      ::MPI_Recv( &this->iS_wf_wi_w0_size, sizeof( size_t ), MPI_BYTE, source, tag, comm, MPI_STATUS_IGNORE );
      ::MPI_Recv( &this->coeffs_size, sizeof( size_t ), MPI_BYTE, source, tag, comm, MPI_STATUS_IGNORE );
           
      //cout << "Resizing vectors...to sizes " << this->iS_wf_wi_w0_size << " " << this->coeffs_size << endl; 
      this->iS_wf_wi_w0.resize( this->iS_wf_wi_w0_size );            
      this->coeffs.resize( this->coeffs_size );

      //cout << "Receiving tensors..." << endl;
      ::MPI_Recv( &this->iS_wf_wi_w0[ 0 ], this->iS_wf_wi_w0_size * sizeof( uint8_t ), MPI_BYTE, source, tag, comm, MPI_STATUS_IGNORE );
      
      ::MPI_Recv( &this->coeffs[ 0 ], this->coeffs_size, MPI_DOUBLE, source, tag, comm, MPI_STATUS_IGNORE );
   }


   void MPI_Init_Recv( int source, int tag, MPI_Comm comm, MPI_Status* status )
   {      
      ::MPI_Irecv( &this->na, 1, MPI_INT, source, tag, comm, &transferRequests[ 0 ] );
      ::MPI_Irecv( &this->nb, 1, MPI_INT, source, tag, comm, &transferRequests[ 1 ] );
      ::MPI_Irecv( &this->nc, 1, MPI_INT, source, tag, comm, &transferRequests[ 2 ] );
      ::MPI_Irecv( &this->nd, 1, MPI_INT, source, tag, comm, &transferRequests[ 3 ] );

      ::MPI_Irecv( &this->ntensors, sizeof( uint32_t ), MPI_BYTE, source, tag, comm, &transferRequests[ 4 ] );
      
      ::MPI_Irecv( &this->iS_wf_wi_w0_size, sizeof( size_t ), MPI_BYTE, source, tag, comm, &this->iS_wf_wi_w0_recvRequest );
      ::MPI_Irecv( &this->coeffs_size, sizeof( size_t ), MPI_BYTE, source, tag, comm, &this->coeffs_recvRequest );

      this->mpiSource = source;
      this->mpiTag = tag;
      this->mpiComm = comm;
      recvStatus = 0;
   }

   bool MPI_Finish_Recv()
   {
      if( recvStatus == -1 )
      {
         std::cerr << "The receving process was not started yet." << std::endl;
         exit( EXIT_FAILURE );
      }
      if( recvStatus == 0 )
      {
         //MPI_Status status;
         int flag1, flag2;
         MPI_Test( &this->iS_wf_wi_w0_recvRequest, &flag1, MPI_STATUS_IGNORE );
         MPI_Test( &this->coeffs_recvRequest, &flag2, MPI_STATUS_IGNORE );
         //cout << "   Checking receiving data -- flags are " << flag1 << " " << flag2 << endl;
         if( flag1 && flag2 )
         {
            //cout << "Setting size to " << this->iS_wf_wi_w0_size << " and " << this->coeffs_size << endl;
            this->iS_wf_wi_w0.resize( this->iS_wf_wi_w0_size );            
            this->coeffs.resize( this->coeffs_size );
            ::MPI_Irecv( &this->iS_wf_wi_w0[ 0 ], this->iS_wf_wi_w0_size * sizeof( uint8_t ), MPI_BYTE, this->mpiSource, this->mpiTag, this->mpiComm, &this->iS_wf_wi_w0_recvRequest );
            
            ::MPI_Irecv( &this->coeffs[ 0 ], this->coeffs_size, MPI_DOUBLE, this->mpiSource, this->mpiTag, this->mpiComm, &this->coeffs_recvRequest );
            
            recvStatus = 1;      
         }
      }
      if( recvStatus == 1 )
      {
         MPI_Status status;
         int flag1( false ), flag2( false );
         MPI_Test( &this->iS_wf_wi_w0_recvRequest, &flag1, &status );
         MPI_Test( &this->coeffs_recvRequest, &flag2, &status );
         if( flag1 && flag2 )
         {
            recvStatus = 2;
            return true;
         }
      }
      if( recvStatus == 2 )
         return true;
      return false;
   }

   int recvStatus;
      
   MPI_Request transferRequests[ 9 ];

   MPI_Request iS_wf_wi_w0_recvRequest, coeffs_recvRequest;

   int mpiSource, mpiTag;
      
   size_t iS_wf_wi_w0_size, coeffs_size;
   
   MPI_Comm mpiComm;

#endif    

   int na, nb, nc, nd;

   uint32_t ntensors;

   std::vector< uint8_t > iS_wf_wi_w0;

   std::vector< double > coeffs;
};

#endif
