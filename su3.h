#ifndef su3H
#define su3H

#include "functions.h"
#include <vector>
#include <iostream>

namespace SU3
{
   typedef double WIGNER;
   typedef char rho_type;
   typedef char RHO_MAX_TYPE;
   typedef unsigned char mu_type;
   typedef unsigned char lm_type;

   struct LABELS 
   {
      LABELS( const SU3::LABELS& R )
      : rho(R.rho), lm(R.lm), mu(R.mu) {} 
      
      LABELS( int rhot, int lmt, int mut)
      : rho(rhot), lm(lmt), mu(mut) {}

      LABELS( int lmt, int mut )
      :rho(+1), lm(lmt), mu(mut) {} // if no rho in input ==> implicitly set rhomax/rho = 1
      
      LABELS()
      : rho(+1), lm(0), mu(0) {}

      inline int C2() const
      { 
         return (lm*lm + lm*mu + mu*mu + 3*(lm + mu));
      }

      inline bool operator< (const SU3::LABELS& rhs) const 
      {
         return (lm < rhs.lm) || (lm == rhs.lm && mu < rhs.mu);
      }

      inline bool operator==( const SU3::LABELS& rhs ) const
      {
         return ((lm == rhs.lm) && (mu == rhs.mu) && (rho == rhs.rho));
      }

      inline bool operator!=( const SU3::LABELS& rhs ) const
      {
         return ((lm != rhs.lm) || (mu != rhs.mu) || (rho != rhs.rho));
      }

      inline size_t dim()
      {
         return (size_t)((lm + 1.0) * (mu + 1.0) * ((lm + mu)/2.0 + 1.0));
      }

      rho_type rho; // this variable should rahter be  called rhomax 
      lm_type lm;
      mu_type mu;
   };

#ifdef HAVE_CUDA
__host__ __device__
#endif
  inline int kmax( const SU3::LABELS& RhoLmMu, const int L ) 
  {
     return( max( 0, ( RhoLmMu.lm+RhoLmMu.mu+2-L ) / 2 ) - max( 0, (RhoLmMu.lm+1-L)/2)-max(0,(RhoLmMu.mu+1-L)/2) );
  }

  template< class T1, class T2, class T3 >
  int mult( const T1& RhoLmMu1,
            const T2& RhoLmMu2,
            const T3& RhoLmMu3 )
  {
     int lm1(RhoLmMu1.lm), mu1(RhoLmMu1.mu), lm2(RhoLmMu2.lm), mu2(RhoLmMu2.mu), lm3(RhoLmMu3.lm), mu3(RhoLmMu3.mu);
     unsigned result = 0;

     int phase, redphase, invl1, invl2;
     phase = lm1 + lm2 - lm3 - mu1 - mu2 + mu3;

     if( phase % 3 == 0 )
     {
        int l1, l2, l3, m1, m2, m3;

        redphase = phase / 3;
	if( phase >= 0 )
        {
    	   l1 = lm1; l2 = lm2; l3 = lm3;
           m1 = mu1; m2 = mu2; m3 = mu3;
        }
        else
        {
           l1 = mu1; l2 = mu2; l3 = mu3;
           m1 = lm1; m2 = lm2; m3 = lm3;
           redphase = -redphase;
        }
    	phase = redphase + m1 + m2 - m3;

	invl1 = std::min(l1 - redphase, m2 );
        if( invl1 < 0 )
           return result;

        invl2 = std::min(l2 - redphase, m1 );
    	if( invl2 < 0 )
           return result;

        result = std::max(std::min(phase,invl2) - std::max(phase-invl1,0) + 1, 0);
     }
     return result;
   }
}

inline std::ostream& operator << (std::ostream& os, const SU3::LABELS& labels)
{
#ifdef USER_FRIENDLY_OUTPUT		
   if( labels.rho < 0 )
   {
      return ( os << "(" << (int)labels.lm << " " << (int)labels.mu << ")");
   }
   else
   {
      return (os << (int)labels.rho << "(" << (int)labels.lm << " " << (int)labels.mu << ")");
   }
#else
   return (os << (int)labels.rho << " " << (int)labels.lm << " " << (int)labels.mu << " ");
#endif		
}

typedef std::vector<SU3::LABELS> SU3_VEC;

namespace SU3
{
   template< class T1, class T2 >
   size_t Couple( const T1& ir1,
                  const T2& ir2,
                  SU3_VEC& ResultingIrreps )
   {
      SU3::LABELS ir3;
      size_t max = (ir1.lm + ir2.lm + ir1.mu + ir2.mu);
      ResultingIrreps.reserve(max);

      for( ir3.lm = 0; ir3.lm <= max; ++ir3.lm ) 
      {
         for( ir3.mu = 0; ir3.mu <= max; ++ir3.mu ) 
         {
            ir3.rho = SU3::mult( ir1, ir2, ir3 );
            if( ir3.rho != 0 ) 
	    {
               ResultingIrreps.push_back(ir3);
            }
	 }
      }	
      return ResultingIrreps.size();
   }
}

#endif
