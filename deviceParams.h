#ifndef deviceParams_h
#define deviceParams_h

#define warpSize  32

#define shmBanks 32

#define shmBanksLog2 5

#define maxGridSize 65536
//#define maxGridSize 1

#endif
