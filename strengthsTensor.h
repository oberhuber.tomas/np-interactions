#ifndef strengthsTensorH
#define strengthsTensorH

#include <iostream>
#include <fstream>
#include <stdint.h>
#include "logFactTable.h"
#include "wigner.h"
#include "sps2Index.h"
#include "su2.h"
#include "su3.h"
#include "deviceParams.h"
#include "strengthsTensorData.h"
#include "tensorResultsUpdater.h"
#include "constants.h"
#include "StrengthsTensorDataBuffer.h"

using std::cout;
using std::cerr;
using std::endl;
using std::flush;
using std::fstream;
using std::ios;

template< typename Device >
class strengthsTensor
{
   public:
   
   strengthsTensor( const int maxLogFact );

   bool computeHost( const strengthsTensorData& tensorData ) const;
   
   bool computeDevice( const strengthsTensorData& tensorData,
                       const int size,
                       const int rho0_max,
                       const int resultsISSize,
                       double* results ) const;
   
   bool computeDeviceFast( std::vector< strengthsTensorData >& tensorData_list,
                           const int size,
                           const std::vector< int >& rho0_max_list,
                           const std::vector< int >& resultsISSize_list,
                           std::vector< double* >& results_list,
                           StrengthsTensorDataBuffer& tensorDataBuffer,
                           bool optimizedTransfer ) const;
   
   
   protected:
   
   logFactTable< host > lfTable;
};
   
template< typename Device, int blockSize > 
#ifdef HAVE_CUDA
__device__ __host__
#endif      
void processTriplet( const int ime,
                     const int Lf_max,
                     const int Li_max,
                     const int rho0_max,
                     float* local_results,
                     const strengthsTensorData& tensorData,
                     tensorResultsUpdater< Device, blockSize >& resultsUpdater,
                     const wigner6jCoefficients< Device >& wigner6j,
                     const wigner9jCoefficients< Device >& wigner9j );

template< typename Device >
strengthsTensor< Device >::strengthsTensor( const int maxLogFact )
: lfTable( maxLogFact )
{
}

#ifdef HAVE_CUDA

__global__ void resetResults( double* results,
                              const int size )
{
   const int gridSize = gridDim.x*blockDim.x;
   int elementIdx = blockIdx.x*blockDim.x + threadIdx.x;
   while( elementIdx < size )
   {
      results[ elementIdx ] = 0.0;
      elementIdx += gridSize;
   }
}

template< int blockSize >
__global__ void tripletsSummingKernel( const double* globalLogFactTable,
                                       const int maxLogFact,
                                       const int gridIdx,
                                       const int rho0_max,
                                       const strengthsTensorData tensorData,
                                       const int blocksNum )
{
   extern __shared__ double resultsReduction[];
   float* local_results = ( float* ) &resultsReduction[ blockDim.x ];
   double* logFactShm = ( double* ) &local_results[ 2*rho0_max*blockDim.x ];
   local_results = &local_results[ threadIdx.x ];
   
   
   const int blkIdx = gridIdx*maxGridSize + blockIdx.x;
   logFactTable< cuda > lfTable( globalLogFactTable, logFactShm, maxLogFact );
   wigner6jCoefficients< cuda > wigner6j( &lfTable );
   wigner9jCoefficients< cuda > wigner9j( &lfTable );
  
   const int lastValidThread =  
     min( blockDim.x, tensorData.size-blkIdx*blockDim.x );
   
   tensorResultsUpdater< cuda, blockSize > resultsUpdater(
      resultsReduction,
      min( blockDim.x, tensorData.size-blkIdx*blockDim.x ),
      blocksNum,
      blkIdx );
   const int elementIdx = blkIdx*blockDim.x+threadIdx.x;
   
   if( elementIdx < tensorData.size )
   {
      const int Lf_max(tensorData.wf.lm + tensorData.wf.mu + 1);
      const int Li_max(tensorData.wi.lm + tensorData.wi.mu + 1);   

      processTriplet< cuda, blockSize >( elementIdx,
                                         Lf_max,
                                         Li_max,
                                         rho0_max,
                                         local_results,
                                         tensorData,
                                         resultsUpdater,
                                         wigner6j,
                                         wigner9j );
   }
     
}

template< int blockSize >
__global__ void reduceBlockData( const double* inputArray,
                                 const int size,
                                 double* output,
                                 const int resultSize )
{
   extern __shared__ double sharedData[];
   int localThreadIdx = threadIdx. x;
   int lastElementIdx = size - 2 * blockIdx. x * blockDim. x;
   //if( threadIdx.x == 0 )
   //   printf( "Last element idx = %d \n", lastElementIdx );

   for( int resultIdx = 0; resultIdx < resultSize; resultIdx++ )
   {
      int globalThreadIdx = 2 * blockIdx. x * blockDim. x + threadIdx. x;
      const double* input = &inputArray[ resultIdx*size ];
      /***
       * Start with the sequential reduction.
       */
      if( globalThreadIdx + blockDim. x < size )
         sharedData[ localThreadIdx ] = input[ globalThreadIdx ] + 
                                        input[ globalThreadIdx + blockDim. x ];
      else if( globalThreadIdx < size )
         sharedData[ localThreadIdx ] = input[ globalThreadIdx ];
      
      //if( globalThreadIdx < size )
      //   printf( "   %d <- %f \n", threadIdx.x, sharedData[ threadIdx.x ] );
      
      int gridSize = 2 * blockDim. x * gridDim.x;
      globalThreadIdx += gridSize;
      while( globalThreadIdx + blockDim. x < size )
      {
         sharedData[ localThreadIdx ] += input[globalThreadIdx] + 
                                         input[ globalThreadIdx + blockDim. x ];
         globalThreadIdx += gridSize;
      }
      if( globalThreadIdx < size )
         sharedData[ localThreadIdx ] += input[globalThreadIdx];
      __syncthreads();      

      /***
       *  Perform the parallel reduction.
       */
      if( lastElementIdx < blockDim. x )
      {
         int reducedPartSize = lastElementIdx;
         for( int stride = reducedPartSize / 2; stride > 0; stride >>= 1 )
         {
            __syncthreads();
            if( localThreadIdx < stride &&
                localThreadIdx + stride < lastElementIdx )
                  sharedData[ localThreadIdx ] += 
                     sharedData[ localThreadIdx + stride ];
            if( 2 * stride < reducedPartSize &&
               localThreadIdx == 0 )
                  sharedData[ 0 ] += sharedData[ reducedPartSize - 1 ];
            reducedPartSize = stride;
         }
      }
      else
      {
         if( blockSize >= 1024 )
            reduceSum( localThreadIdx, 512, sharedData );
         if( blockSize >= 512 )
            reduceSum( localThreadIdx, 256, sharedData );
         if( blockSize >= 256 )
            reduceSum( localThreadIdx, 128, sharedData );
         if( blockSize >= 128 )
            reduceSum( localThreadIdx, 64, sharedData );
         __syncthreads();

         /***
          * This runs in one warp so it is synchronised implicitly.
          */      
         if( localThreadIdx < 32 )
         {
            volatile double* shared = sharedData;
            if( blockSize >= 64 )
               shared[ localThreadIdx ] += 
                  shared[ localThreadIdx + 32 ];
            if( blockSize >= 32 )          
               shared[ localThreadIdx ] += 
                  shared[ localThreadIdx + 16 ];
            if( blockSize >= 16 )
               shared[ localThreadIdx ] += 
                  shared[ localThreadIdx + 8 ];
            if( blockSize >= 8 )
               shared[ localThreadIdx ] += 
                  shared[ localThreadIdx + 4 ];
            if( blockSize >= 4 )
               shared[ localThreadIdx ] += 
                  shared[ localThreadIdx + 2 ];
            if( blockSize >= 2 )
               shared[ localThreadIdx ] += 
                  shared[ localThreadIdx + 1 ];
         }
      }
      /***
       * Store the result back in the global memory.
       */
      if( threadIdx.x == 0 )
      {
         output[ resultIdx*gridDim.x + blockIdx. x ] = sharedData[ 0 ];
         //printf( "Result = %f \n", sharedData[ 0 ] );
      } 
      __syncthreads();
   }
}
#endif

template< typename Device >
bool strengthsTensor< Device >::computeHost( const strengthsTensorData& tensorData ) const
{
   /****
    * Set-up often used loop limits
    *	Lf_max: number of possible values Lf can have in wf irrep.	
    *	Li_max: number of possible values Li can have in wi irrep.	
    *	rho0_max: ???
    * They must by firstprivate in OpenMP
    */
   const int Lf_max(tensorData.wf.lm + tensorData.wf.mu + 1);
   const int Li_max(tensorData.wi.lm + tensorData.wi.mu + 1);
   const int rho0_max(tensorData.w0.rho);
   
   /****
    * Iterate over all la, lb, lc, ld, ja, jb, jc, jd and J.
    * Note, there to each such combination we have a triplet
    * { V_pp, V_nn and V_pn } appearing in the formula.
    */
   wigner6jCoefficients< host > wigner6j( &this->lfTable );
   wigner9jCoefficients< host > wigner9j( &this->lfTable );
   //#pragma omp parallel for firstprivate( Lf_max, Li_max, rho0_max, ilabel, iv )
   tensorResultsUpdater< host > resultsUpdater;
   float* local_results = new float[ 2 * rho0_max ];
   for( int i = 0; i < tensorData.size; i++ )
   {
      processTriplet< host >( i,
                              Lf_max,
                              Li_max,
                              rho0_max,
                              local_results,
                              tensorData,
                              resultsUpdater,
                              wigner6j,
                              wigner9j );
   }
   delete[] local_results;
   return true;
}

template< typename Device >
bool strengthsTensor< Device >::computeDevice( const strengthsTensorData& deviceTensorData,
                                               const int size,
                                               const int rho0_max,
                                               const int resultsISSize,
                                               double* results ) const
{
#ifdef HAVE_CUDA
   //cerr << " CUDA " << endl;
   dim3 blockSize, gridSize;
   const int BLK_SIZE( 256 );
   blockSize.x = BLK_SIZE;
   //cout << "Processing " << size << " elements on the GPU..." << endl;
   const int blocksNumber = size / blockSize.x + ( size % blockSize.x != 0 );
   const int gridsNumber = blocksNumber / maxGridSize + ( blocksNumber % maxGridSize != 0 );
   const int sharedMemory = this->lfTable.getTableSize()*sizeof( double ) +
                            blockSize.x*sizeof( double )+
                            2*rho0_max*blockSize.x*sizeof( float );
   const int resultsSize = blocksNumber*6*resultsISSize;
   gridSize.x = min( maxGridSize, resultsSize/blockSize.x + ( resultsSize%blockSize.x != 0 ) );
   //cout << "Reseting the results array..." << endl;
   resetResults<<< gridSize, blockSize >>>( deviceTensorData.results, resultsSize );
   CHECK_CUDA_DEVICE;
   /*if( ! checkCUDADevice() )
   {
      cerr << "Unable to reset the results array on the GPU." << endl;
      return false;
   }*/
   //cout << gridsNumber << " CUDA grids will be used ... " << endl;
   for( int gridIdx = 0; gridIdx < gridsNumber; gridIdx++ )
   {
      gridSize.x = std::min( maxGridSize, blocksNumber - gridIdx*maxGridSize );
      //cout << "Processing the triplets using the grid " << gridIdx << " grid size is " << gridSize.x << endl;
      tripletsSummingKernel< BLK_SIZE >
                           <<< gridSize, blockSize, sharedMemory >>>
                           ( this->lfTable.getGPUTable(),
                             MAX_LOGFACT,
                             gridIdx,
                             rho0_max, 
                             deviceTensorData,
                             blocksNumber );
      CHECK_CUDA_DEVICE;
      /*if( ! checkCUDADevice() )
      {
         cerr << "Unable to process the input triplets on the GPU." << endl;
         return false;
      }*/
   }

   /****
    * Since we have only "few" blocks, the reduction of the results
    * from the blocks can be done on the CPU.
    */
   double* auxHost = new double[ resultsSize ];
   cudaMemcpy( auxHost, deviceTensorData.results, resultsSize*sizeof( double ), cudaMemcpyDeviceToHost );
   CHECK_CUDA_DEVICE;
   /*if( ! checkCUDADevice() )
   {
      cerr << "Unable to copy the result from the GPU for the reduction on the host." << endl;
      return false;
   }*/
   for( int resIdx = 0; resIdx < 6*resultsISSize; resIdx ++ )
   {
      results[ resIdx ] = 0.0;
      for( int i = 0; i < blocksNumber; i++ )
      {
         results[ resIdx ] += auxHost[ resIdx*blocksNumber + i ]; 
      }
   }
   delete[] auxHost;
#endif
   return true;
}

#ifdef HAVE_CUDA

__global__ void resetResultsFast( const int gridIdx,
                                  strengthsTensorData* tensors,
                                  const int blocksPerResult,
                                  const int blocksCount )
{
   const int blockId = gridIdx * maxGridSize + blockIdx.x;
   const int tensorIdx = blockId / blocksPerResult;
   double* results = tensors[ tensorIdx ].results;
   const int results_size = 6 * blocksCount * tensors[ tensorIdx ].resultsISSize;

   const int elementIdx = ( blockId % blocksPerResult ) * blockDim.x + threadIdx.x;
   if( elementIdx < results_size )
      results[ elementIdx ] = 0.0;
}

template< int blockSize >
__global__ void tripletsSummingKernelFast( const double* globalLogFactTable,
                                           const int maxLogFact,
                                           const int gridIdx,
                                           const int* rho0_max_list,
                                           const strengthsTensorData* tensorData_list,
                                           const int blocksNum )
{
   const int blockId = gridIdx * maxGridSize + blockIdx.x;
   const int tensor_id = blockId / blocksNum;
   const strengthsTensorData& tensorData = tensorData_list[ tensor_id ];
   const int rho0_max = rho0_max_list[ tensor_id ];
   const int blkIdx = blockId % blocksNum;
   
   extern __shared__ double resultsReduction[];
   float* local_results = ( float* ) &resultsReduction[ blockDim.x ];
   double* logFactShm = ( double* ) &local_results[ 2*rho0_max*blockDim.x ];
   local_results = &local_results[ threadIdx.x ];   
   
   logFactTable< cuda > lfTable( globalLogFactTable, logFactShm, maxLogFact );
   wigner6jCoefficients< cuda > wigner6j( &lfTable );
   wigner9jCoefficients< cuda > wigner9j( &lfTable );
  
   const int lastValidThread =  
     min( blockDim.x, tensorData.size-blkIdx*blockDim.x );
   
   tensorResultsUpdater< cuda, blockSize > resultsUpdater(
      resultsReduction,
      min( blockDim.x, tensorData.size-blkIdx*blockDim.x ),
      blocksNum,
      blkIdx );
   const int elementIdx = blkIdx*blockDim.x+threadIdx.x;
   
   if( elementIdx < tensorData.size )
   {
      const int Lf_max(tensorData.wf.lm + tensorData.wf.mu + 1);
      const int Li_max(tensorData.wi.lm + tensorData.wi.mu + 1);   

      processTriplet< cuda, blockSize >( elementIdx,
                                         Lf_max,
                                         Li_max,
                                         rho0_max,
                                         local_results,
                                         tensorData,
                                         resultsUpdater,
                                         wigner6j,
                                         wigner9j );
   }    
}
#endif

template< typename Device >
bool strengthsTensor< Device >::computeDeviceFast( std::vector< strengthsTensorData >& tensorData_list,
                                                   const int size,
                                                   const std::vector< int >& rho0_max_list,
                                                   const std::vector< int >& resultsISSize_list,
                                                   std::vector< double* >& results_list,
                                                   StrengthsTensorDataBuffer& tensorDataBuffer,
                                                   bool optimizedTransfer ) const
{
#ifdef HAVE_CUDA   
   /****
    * Copy lists to the device
    */
   const int tensors_count = tensorData_list.size();
   strengthsTensorData* tensorData_list_device;
   int *rho0_max_list_device, *resultsISSize_list_device;
   double** results_list_device;
   
   if( ! optimizedTransfer )
   {
      cudaMalloc( &tensorData_list_device, tensors_count * sizeof( strengthsTensorData ) );   
      cudaMalloc( &rho0_max_list_device, tensors_count * sizeof( int ) );
      cudaMalloc( &resultsISSize_list_device, tensors_count * sizeof( int ) );
      cudaMalloc( &results_list_device, tensors_count * sizeof( double* ) );

      cudaMemcpy( tensorData_list_device, tensorData_list.data(), tensors_count * sizeof( strengthsTensorData ), cudaMemcpyHostToDevice );
      cudaMemcpy( rho0_max_list_device, rho0_max_list.data(), tensors_count * sizeof( int ), cudaMemcpyHostToDevice );
      cudaMemcpy( resultsISSize_list_device, resultsISSize_list.data(), tensors_count * sizeof( int ), cudaMemcpyHostToDevice );
      cudaMemcpy( results_list_device, results_list.data(), tensors_count * sizeof( double* ), cudaMemcpyHostToDevice );
   }
   
   int *rho0_max_list_host, *resultsISSize_list_host;
   strengthsTensorData* tensorData_list_host;
   double** results_list_host;
   tensorDataBuffer.allocate_tensor( tensors_count, tensorData_list_host, tensorData_list_device );
   tensorDataBuffer.allocate_int( tensors_count, rho0_max_list_host, rho0_max_list_device );
   tensorDataBuffer.allocate_int( tensors_count, resultsISSize_list_host, resultsISSize_list_device );
   tensorDataBuffer.allocate_double_ptr( tensors_count, results_list_host, results_list_device );
   memcpy( tensorData_list_host, tensorData_list.data(), tensors_count * sizeof( strengthsTensorData ) );
   memcpy( rho0_max_list_host, rho0_max_list.data(), tensors_count * sizeof( int ) );
   memcpy( resultsISSize_list_host, resultsISSize_list.data(), tensors_count * sizeof( int ) );
   memcpy( results_list_host, results_list.data(), tensors_count * sizeof( double* ) );
   
   tensorDataBuffer.transferData();
   CHECK_CUDA_DEVICE;
   
   const int max_resultsISSize = *std::max_element( resultsISSize_list.begin(), resultsISSize_list.end() );
   const int max_rho0_max = *std::max_element( rho0_max_list.begin(), rho0_max_list.end() );
      
   
   const int blockSize( 256 );
   dim3 blockDim, gridSize;
   blockDim.x = blockSize;
   const int blocksCount = size / blockSize + ( size % blockSize != 0 );
   const int gridsNumber = blocksCount / maxGridSize + ( blocksCount % maxGridSize != 0 );
   
   
   //for( int i = 0; i < tensors_count; i++ )
   {
      /*const strengthsTensorData& deviceTensorData = tensorData_list[ i ];
      const int rho0_max = rho0_max_list[ i ];
      const int resultsISSize = resultsISSize_list[ i ];
      double* results = results_list[ i ];*/
      //const int resultsSize = blocksCount*6*resultsISSize;
      
      const int results_size = blocksCount*6*max_resultsISSize;
      const int blocksPerResult = results_size / blockSize + ( results_size % blockSize != 0 );
      
      const int resetBlocksNumber = blocksPerResult * tensors_count;
      const int gridsNumber = resetBlocksNumber / maxGridSize + ( resetBlocksNumber % maxGridSize != 0 );
      for( int gridIdx = 0; gridIdx < gridsNumber; gridIdx ++ )
      {
         gridSize.x = min( maxGridSize, resetBlocksNumber - gridIdx * maxGridSize );
         //cout << "Reseting the results array... " << resultsSize << endl;
         //resetResults<<< gridSize, blockSize >>>( deviceTensorData.results, results_size );
         resetResultsFast<<< gridSize, blockSize >>>( gridIdx, tensorData_list_device, blocksPerResult, blocksCount );
         CHECK_CUDA_DEVICE;
      }
   }   
      
   
   //for( int i = 0; i < tensors_count; i++ )
   {
      /*const strengthsTensorData& tensorData = tensorData_list[ i ];
      const int rho0_max = rho0_max_list[ i ];
      const int resultsISSize = resultsISSize_list[ i ];
      double* results = results_list[ i ];
      const int resultsSize = blocksCount*6*resultsISSize;*/
      
      const int totalBlocksCount = blocksCount * tensors_count;
      const int gridsNumber = totalBlocksCount / maxGridSize + ( totalBlocksCount % maxGridSize != 0 );
      
      const int sharedMemory = this->lfTable.getTableSize()*sizeof( double ) +
                               blockSize*sizeof( double )+
                               2*max_rho0_max*blockSize*sizeof( float );
      
      
      //cout << gridsNumber << " CUDA grids will be used ... " << endl;
      for( int gridIdx = 0; gridIdx < gridsNumber; gridIdx++ )
      {
         gridSize.x = std::min( maxGridSize, totalBlocksCount - gridIdx*maxGridSize );
         //cout << "Processing the triplets using the grid " << gridIdx << " grid size is " << gridSize.x << endl;
         tripletsSummingKernelFast< blockSize >
                              <<< gridSize, blockDim, sharedMemory >>>
                              ( this->lfTable.getGPUTable(),
                                MAX_LOGFACT,
                                gridIdx,
                                rho0_max_list_device, 
                                tensorData_list_device,
                                blocksCount );
         CHECK_CUDA_DEVICE;
      }
   }   
   

   if( optimizedTransfer )
      tensorDataBuffer.transferResults();
   for( int i = 0; i < tensors_count; i++ )
   {
      const strengthsTensorData& deviceTensorData = tensorData_list_host[ i ];
      const int rho0_max = rho0_max_list[ i ];
      const int resultsISSize = resultsISSize_list[ i ];
      double* results = results_list[ i ];
      const int resultsSize = blocksCount*6*resultsISSize;
      
      /****
       * Since we have only "few" blocks, the reduction of the results
       * from the blocks can be done on the CPU.
       */
      double* auxHost;
      if( ! optimizedTransfer )
      {
         double* auxHost = new double[ resultsSize ];
         cudaMemcpy( auxHost, deviceTensorData.results, resultsSize*sizeof( double ), cudaMemcpyDeviceToHost );
      }
      else
         auxHost = deviceTensorData.results_host;
      CHECK_CUDA_DEVICE;
      for( int resIdx = 0; resIdx < 6*resultsISSize; resIdx ++ )
      {
         results[ resIdx ] = 0.0;
         for( int i = 0; i < blocksCount; i++ )
         {
            results[ resIdx ] += auxHost[ resIdx*blocksCount + i ];
         }
      }
      if( ! optimizedTransfer )
         delete[] auxHost;
   }
   
   if( ! optimizedTransfer )
   {
      cudaFree( tensorData_list_device );
      cudaFree( rho0_max_list_device );
      cudaFree( resultsISSize_list_device );
      cudaFree( results_list_device );
      CHECK_CUDA_DEVICE;
   }
   return true;
#else
   return false;
#endif      
}


template< typename Device, int blockSize >
#ifdef HAVE_CUDA
   __device__ __host__
#endif      
void processTriplet( const int ithread,
                     const int Lf_max,
                     const int Li_max,
                     const int rho0_max,
                     float* local_results,
                     const strengthsTensorData& tensorData,
                     tensorResultsUpdater< Device, blockSize >& resultsUpdater,
                     const wigner6jCoefficients< Device >& wigner6j,
                     const wigner9jCoefficients< Device >& wigner9j )
{
   int na, nb, nc, nd, la, lb, lc, ld, jja, jjb, jjc, jjd;
   assert( ithread < tensorData.size );
   sps2Index::getNLJ( tensorData.i1[ithread], na, la, jja);
   sps2Index::getNLJ( tensorData.i2[ithread], nb, lb, jjb);
   sps2Index::getNLJ( tensorData.i3[ithread], nc, lc, jjc);
   sps2Index::getNLJ( tensorData.i4[ithread], nd, ld, jjd);
   int JJ =  2*tensorData.J[ithread];

   assert( la*(nb+1) + lb < tensorData.lalb_indices_size );
   assert( ld*(nc+1) + lc < tensorData.ldlc_indices_size );
   int iab_cgs0 = tensorData.lalb_indices[la*(nb+1) + lb]; // abf_cgSU3[iab_cgs] ---> <(na 0) 0 la; (nb 0) 0 lb || wf * *>_{0}
   int idc_cgs0 = tensorData.ldlc_indices[ld*(nc+1) + lc]; // dci_cgSU3[idc_cgs] ---> <(0 nd) 0 ld; (0 nc) 0 lc || wi * *>_{0}

   for (int iS = 0; iS < 6; iS++)
   {
            int S0 = tensorData.SfSiS0[3*iS+2];
            int k0_max = SU3::kmax( tensorData.w0, S0);

            for( int i = 0; i < k0_max*rho0_max; i++ )
               local_results[ i*blockSize ] = 0.0;

            if (idc_cgs0 != kDoesNotExist && iab_cgs0 != kDoesNotExist && k0_max)
            {
                    int Sf = tensorData.SfSiS0[3*iS];
                    int Si = tensorData.SfSiS0[3*iS+1];
                     
                    int kf_max( 0 );
                    for (int Lf = abs(la-lb), iab_cgs = iab_cgs0; (Lf <= (la+lb) && Lf < Lf_max); ++Lf, iab_cgs += kf_max)
                    {
                            kf_max = SU3::kmax( tensorData.wf, Lf);
                            if (!kf_max)
                            {
                                    continue;
                            }

                            if (!SU2::mult(2*Lf, 2*Sf, JJ)) // wigner9j(la, lb, Lf, 1, 1, Sf, ja, jb, J) = 0
                            {
                                    continue;
                            }
                            double wig1 = wigner9j.getCoefficient(2*la, 2*lb, 2*Lf, 1, 1, 2*Sf, jja, jjb, JJ);
                            
                            int ki_max( 0 );
                            for (int Li = abs(ld-lc), idc_cgs = idc_cgs0; (Li <= (ld+lc) && Li < Li_max); ++Li, idc_cgs += ki_max)
                            {
                                    ki_max = SU3::kmax( tensorData.wi, Li);
                                    if (!ki_max)
                                    {
                                            continue;
                                    }
                                    if (S0 < abs(Lf-Li) || S0 > (Lf+Li))
                                    {
                                            continue;
                                    }
                                    if (!SU2::mult(2*Li, 2*Si, JJ)) // wigner9j(ld, lc, Li, 1, 1, Si, jd, jc, J) = 0
                                    {
                                            continue;
                                    }

                                    int index_LfLi = tensorData.lfli_indices[Lf*Li_max + Li]; // fi0_cgSU3[index_LfLi] ---> <wf * Lf; wi * Li || w0 * S0min>_{*}
                                    if (index_LfLi == kDoesNotExist)
                                    {
                                            continue;
                                    }

                                    // at this momnet index_LfLi points to first SU(3) CG with S0min, which may be less then S0
                                    // ==> we need to move it so it points to SU(3) CG corresponding to S0
                                    for (int s0 = abs(Lf-Li); s0 < S0; s0++)
                                    {
                                            index_LfLi += SU3::kmax(tensorData.w0, s0)*rho0_max*kf_max*ki_max; 
                                    }

                                    double phaseSU2 = tensorData.PiSfSiS0[iS]*MINUSto(Sf + S0 + Li)*sqrt(2*Li+1)*sqrt(2*Lf+1);
                                    phaseSU2*= wig1*wigner9j.getCoefficient(2*ld, 2*lc, 2*Li, 1, 1, 2*Si, jjd, jjc, JJ)*
                                                    wigner6j.getCoefficient(2*Lf, 2*Li, 2*S0, 2*Si, 2*Sf, JJ);
                                    for (int i = 0, j = 0; i < rho0_max*k0_max; ++i)
                                    {
                                            double su3cg_sum = 0;
                                            for (int kf = 0; kf < kf_max; kf++)
                                            {
                                                    double sum_ki_result(0);
                                                    for (int ki = 0; ki < ki_max; ki++, j++)
                                                    {
                                                       assert( index_LfLi+j < tensorData.fi0_cgSU3_size );
                                                       assert( idc_cgs+ki < tensorData.dci_cgSU3_size );
                                                            sum_ki_result += tensorData.fi0_cgSU3[index_LfLi+j] * tensorData.dci_cgSU3[idc_cgs+ki];
                                                    }
                                                    su3cg_sum += sum_ki_result * tensorData.abf_cgSU3[iab_cgs+kf];
                                            }
                                            local_results[i*blockSize ] += phaseSU2*su3cg_sum;
                                    }
                            } // Li
                    } // Lf
            } // if (idc_cgs0 != kDoesNotExist && iab_cgs0 != kDoesNotExist && k0_max)
            

            for (int j = 0, i = 0; j < k0_max*rho0_max; j++, i += 3)
            {
               //printf( "iS = %d ithread = %d \n", iS, ithread );
               resultsUpdater.update( local_results[ j*blockSize ], iS,
                                      tensorData.Vpp[ ithread ], tensorData.Vnn[ ithread ], tensorData.Vpn[ ithread ],
                                      tensorData.results, tensorData.resultsISSize, i );
                
                    /*results[iS][i]   += (double)local_results[j]*Vpp[ithread];
                    results[iS][i+1] += (double)local_results[j]*Vnn[ithread];
                    results[iS][i+2] += (double)local_results[j]*Vpn[ithread];*/
            }
    } // iS
}
#endif

