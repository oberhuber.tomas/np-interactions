#ifndef logDelta2H
#define logDelta2H

#include "logFactTable.h"

template< typename Device >
class logDelta2
{
   public:
   
#ifdef HAVE_CUDA
   __device__ __host__
#endif   
   logDelta2( const logFactTable< Device >* _lfTable )
   : lfTable( _lfTable )
   {
   };

#ifdef HAVE_CUDA
   __device__ __host__
#endif   
   double getLogDelta2( const int a2,
                        const int b2,
                        const int c2 ) const
   {
      return ( lfTable->getLogFact( ( a2 + b2 - c2 ) / 2 ) +
               lfTable->getLogFact( ( c2 + a2 - b2 ) / 2 ) +
               lfTable->getLogFact( ( b2 + c2 - a2 ) / 2 ) -
               lfTable->getLogFact( ( a2 + b2 + c2 ) / 2 + 1 ) );
   };
   
   protected:

   const logFactTable< Device >* lfTable;
};

#endif
