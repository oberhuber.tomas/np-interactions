#ifndef computeSU3CGsH
#define computeSU3CGsH

#include <vector>
#include <assert.h>
#include <string.h>
#include <stdint.h>
#include "su3.h"
#include "constants.h"

using std::vector;

extern "C" {
extern void blocks_(void);
extern void wu3r3w_( int&, int&, int&, int&, int&, int&, int&, int&, int&, int&, int&, int&, int&, 
		     double[MAX_K][MAX_K][MAX_K][MAX_K] );
}

/****
 * This function calculates and returns SU(3) CGs of the following type:
 *  <(na 0) 0 *; (nb 0) 0 * || wf * *>.
 *
 * For each allowed value la and lb, elements la x lb_max + la contains index of su3cgs vector 
 * whose element contains <(na 0) 0 la, (nb 0) 0 lb || wf 0 min(L0)>_{0}, where
 * min(L0) is the lowest allowed value of L0 for a given irrep wf.
 *
 * Order: index = L0 x k0_max + L0 -------> su3cgs[k0, rho0, kf, ki] 
 */
void ComputeSU3CGs_Simple( const SU3::LABELS& wf,
                           const SU3::LABELS& wi, 
                           const SU3::LABELS& w0,
                           vector< uint16_t >& indices,
                           vector< double >& su3cgs )
{
   assert ((wf.lm == 0 && wi.lm == 0) || (wf.mu == 0 && wi.mu == 0));

   su3cgs.resize(0);
   int max_Lf = wf.lm + wf.mu + 1;
   int max_Li = wi.lm + wi.mu + 1;
   int max_L0 = w0.lm + w0.mu + 1;

   double dCG[ MAX_K ][ MAX_K ][ MAX_K ][ MAX_K ]; // this array is required by su3lib
   memset( dCG, sizeof( dCG ), 0 );

   vector< uint16_t > temp_indices( max_Lf*max_Li, kDoesNotExist );
   double temp_su3cgs[ MAX_K*max_L0 ];

   int lmf( wf.lm ), muf( wf.mu ), lmi( wi.lm ), mui( wi.mu ), lm0( w0.lm ), mu0( w0.mu );
   int max_kf, max_ki, max_k0, max_rho;
   int nCGs;

   /****
    * Iterate over all Lf in (lmf muf).
    */
   for( int Lf = 0; Lf < max_Lf; Lf++)
   {
      if( !SU3::kmax( wf, Lf ) )
         continue;
      /****
       * Iterate over all Li in (lmi mui).
       */
      for( int Li = 0; Li < max_Li; Li++ ) 
      {
         if( !SU3::kmax( wi, Li ) )
	    continue;
	 nCGs = 0;
	 for( int L0 = abs(Lf-Li); L0 <= (Lf + Li) && L0 < max_L0; L0++ )
	 {
            if( !SU3::kmax( w0, L0 ) )
	       continue;
#ifndef AIX			
            wu3r3w_(lmf, muf, lmi, mui, lm0, mu0, Lf, Li, L0, max_rho, max_kf, max_ki, max_k0, dCG);
#else
            wu3r3w (lmf, muf, lmi, mui, lm0, mu0, Lf, Li, L0, max_rho, max_kf, max_ki, max_k0, dCG);
#endif
            /****
             * For all values of k0, store SU(3) Wigner coefficient.
             */
            for( int k0 = 0; k0 < max_k0; k0++ ) 
	    { 
               temp_su3cgs[nCGs++] = dCG[k0][0][0][0]; 
	    }
	 }
	 if( nCGs )
	 {
            temp_indices[ Lf*max_Li + Li ] = su3cgs.size();
            su3cgs.insert( su3cgs.end(), &temp_su3cgs[0], &temp_su3cgs[0] + nCGs );
         }
      }
   }
   temp_indices.swap(indices);
}

/****
 * This function calculates and returns SU(3) CGs of the following type:
 * <wf * *; wi * * || w0 * L0>_{*}, where L0 = 0, 1, 2 ... i.e. I assume
 * it is needed for a two-body scalar (L0=S0) interaction. It could be 
 * easily generalized for a list of L0 values if needed [i.e. for 3-b interactions]
 *
 * For each allowed value Lf and Li, elements Lf x Li_max + Li contains index of su3cgs vector 
 * whose element contains <wf 0 Lf, wi 0 Li || w0 0 min(L0)>_{0}, where
 * min(L0) is the lowest allowed value of L0 for a given irrep w0.
 *
 * Order: index = k0 x rho0_max x kf_max x ki_max + rho0 x kf_max x ki_max + kf x ki_max + ki -------> su3cgs[k0, rho0, kf, ki] 
 */
void ComputeSU3CGs( const SU3::LABELS& wf, const SU3::LABELS& wi, const SU3::LABELS& w0, vector< uint16_t >& indices, vector< double >& su3cgs )
{
   su3cgs.clear();

   /****
    * NOTE: Lf ranges from 0 up to (lm + mu) ---> number of Lf == max_Lf
    */
   int max_Lf = wf.lm + wf.mu + 1;
   int max_Li = wi.lm + wi.mu + 1;

   /****
    * This array is required by su3lib function wu3r3w.
    */
   double dCG[MAX_K][MAX_K][MAX_K][MAX_K]; 
   memset(dCG, sizeof(dCG), 0);

   vector< uint16_t > temp_indices( max_Lf*max_Li, kDoesNotExist );

   /****
    * Estimation of maximal size of temp_su3cgs:
    * maximal k and \rho values allowed by su3lib are equal to K_MAX.  However,
    * L0=0 has k_max = 1 at most ===>  maximal number of SU3 CG's for L0=0 is
    * equal to rho0max <= K_MAX.  for L0=1 and 2, I assume k and rho <= K_MAX
    * ===> 2*MAX_K*MAX_K
    */
   
   double temp_su3cgs[MAX_K + 2*MAX_K*MAX_K]; 

   int lmf(wf.lm), muf(wf.mu), lmi(wi.lm), mui(wi.mu), lm0(w0.lm), mu0(w0.mu);
   int max_kf, max_ki, max_k0, max_rho;
   int nCGs;

   /****
    * Iterate over all Lf in (lmf muf).
    */
   for( int Lf = 0; Lf < max_Lf; Lf++ )  
   {
      if( !SU3::kmax( wf, Lf ) )
         continue;

      /****
       * Iterate over all Li in (lmi mui).
       */
      for( int Li = 0; Li < max_Li; Li++ )
      {
         if( !SU3::kmax( wi, Li ) )
	    continue;
	 nCGs = 0;
	 for( int L0 = abs(Lf-Li); L0 <= std::min(2, Lf+Li); L0++ )
         {
            if( !SU3::kmax( w0, L0 ) )
	       continue;
#ifndef AIX			
            wu3r3w_(lmf, muf, lmi, mui, lm0, mu0, Lf, Li, L0, max_rho, max_kf, max_ki, max_k0, dCG);
#else
            wu3r3w (lmf, muf, lmi, mui, lm0, mu0, Lf, Li, L0, max_rho, max_kf, max_ki, max_k0, dCG);
#endif
            /****
             * For all values of k0:
             */
            for( int k0 = 0; k0 < max_k0; k0++ ) 
	    { 
               /****
                * For all possible multiplicities rho0:
                */
               for( int irho = 0; irho < max_rho; irho++ )
	       {
                  /****
                   * For all values of kf:
                   */
                  for( int kf = 0; kf < max_kf; kf++ )  
		  {
                     /****
                      * For all values of ki:
                      */
                     for( int ki = 0; ki < max_ki; ki++ ) 
		     {
                        /****
                         * Store SU(3) wigner coefficient:
                         */
                        temp_su3cgs[nCGs++] = dCG[k0][ki][kf][irho]; 
                     }
                  }
               }
            }
	 }
         if( nCGs )
	 {
            temp_indices[ Lf*max_Li + Li ] = su3cgs.size();
            su3cgs.insert(su3cgs.end(), &temp_su3cgs[0], &temp_su3cgs[0] + nCGs);
         }
      }
   }
   temp_indices.swap( indices );
}

#endif
