#include "StrengthsTensorDataBuffer.h"
#include "strengthsTensorData.h"
#include <iostream>

#ifdef HAVE_CUDA
#include <cuda.h>
#endif


StrengthsTensorDataBuffer::StrengthsTensorDataBuffer( const size_t max_uint8_data_size,
                                                      const size_t max_uint16_data_size,
                                                      const size_t max_int_data_size,
                                                      const size_t max_double_data_size,
                                                      const size_t max_double_ptr_data_size,
                                                      const size_t max_results_data_size,
                                                      const size_t max_tensors_data_size )
: max_uint8_data_size( max_uint8_data_size ),
  max_uint16_data_size( max_uint16_data_size ),
  max_int_data_size( max_int_data_size ),
  max_double_data_size( max_double_data_size ),
  max_double_ptr_data_size( max_double_ptr_data_size ),
  max_results_data_size( max_results_data_size ),
  max_tensors_data_size( max_tensors_data_size )
{
   this->uint8_data_host = new uint8_t[ max_uint8_data_size ];
   this->uint16_data_host = new uint16_t[ max_uint16_data_size ];
   this->int_data_host = new int[ max_int_data_size ];
   this->double_data_host = new double[ max_double_data_size ];
   this->double_ptr_data_host = new double*[ max_double_ptr_data_size ];
   this->results_data_host = new double[ max_results_data_size ];
   this->tensors_data_host = new strengthsTensorData[ max_tensors_data_size ];

#ifdef HAVE_CUDA
   cudaMalloc( ( void**) &this->uint8_data_device, max_uint8_data_size * sizeof( uint8_t ) );
   cudaMalloc( ( void**) &this->uint16_data_device, max_uint16_data_size * sizeof( uint16_t ) );
   cudaMalloc( ( void**) &this->int_data_device, max_int_data_size * sizeof( int ) );
   cudaMalloc( ( void**) &this->double_data_device, max_double_data_size * sizeof( double ) );
   cudaMalloc( ( void**) &this->double_ptr_data_device, max_double_ptr_data_size * sizeof( double* ) );
   cudaMalloc( ( void**) &this->results_data_device, max_results_data_size * sizeof( double ) );
   cudaMalloc( ( void**) &this->tensors_data_device, max_tensors_data_size * sizeof( strengthsTensorData ) );
#endif
   this->reset();
}
      
void StrengthsTensorDataBuffer::allocate_uint8( const size_t size,
                                                uint8_t*& host_data,
                                                uint8_t*& device_data )
{
   host_data = &this->uint8_data_host[ this->uint8_data_pointer ];
   device_data = &this->uint8_data_device[ this->uint8_data_pointer ];
   this->uint8_data_pointer += size;
   if( this->uint8_data_pointer >= this->max_uint8_data_size )
   {
      std::cerr << "Strengths tensor data buffer exceeded limit for uint8 ( "
                << this->max_uint8_data_size << " )." << std::endl;
      throw( 0 );
   }

}

void StrengthsTensorDataBuffer::allocate_uint16( const size_t size,
                                                 uint16_t*& host_data,
                                                 uint16_t*& device_data )
{
   host_data = &this->uint16_data_host[ this->uint16_data_pointer ];
   device_data = &this->uint16_data_device[ this->uint16_data_pointer ];
   this->uint16_data_pointer += size;
   if( this->uint16_data_pointer >= this->max_uint16_data_size )
   {
      std::cerr << "Strengths tensor data buffer exceeded limit for uint16 ( "
                << this->max_uint16_data_size << " )." << std::endl;
      throw( 0 );
   }
}

void StrengthsTensorDataBuffer::allocate_int( const size_t size,
                                              int*& host_data,
                                              int*& device_data )
{
   host_data = &this->int_data_host[ this->int_data_pointer ];
   device_data = &this->int_data_device[ this->int_data_pointer ];
   this->int_data_pointer += size;
   if( this->int_data_pointer >= this->max_int_data_size )
   {
      std::cerr << "Strengths tensor data buffer exceeded limit for int ( "
                << this->max_int_data_size << " )." << std::endl;
      throw( 0 );
   }
}


void StrengthsTensorDataBuffer::allocate_double( const size_t size,
                                                 double*& host_data,
                                                 double*& device_data )
{
   host_data = &this->double_data_host[ this->double_data_pointer ];
   device_data = &this->double_data_device[ this->double_data_pointer ];
   this->double_data_pointer += size;
   if( this->double_data_pointer >= this->max_double_data_size )
   {
      std::cerr << "Strengths tensor data buffer exceeded limit for double ( "
                << this->max_double_data_size << " )." << std::endl;
      throw( 0 );
   }
}

void StrengthsTensorDataBuffer::allocate_double_ptr( const size_t size,
                                                     double**& host_data,
                                                     double**& device_data )
{
   host_data = &this->double_ptr_data_host[ this->double_ptr_data_pointer ];
   device_data = &this->double_ptr_data_device[ this->double_ptr_data_pointer ];
   this->double_ptr_data_pointer += size;
   if( this->double_ptr_data_pointer >= this->max_double_ptr_data_size )
   {
      std::cerr << "Strengths tensor data buffer exceeded limit for double pointer ( "
                << this->max_double_ptr_data_size << " )." << std::endl;
      throw( 0 );
   }
}

void StrengthsTensorDataBuffer::allocate_results( const size_t size,
                                                 double*& host_data,
                                                 double*& device_data )
{
   host_data = &this->results_data_host[ this->results_data_pointer ];
   device_data = &this->results_data_device[ this->results_data_pointer ];
   this->results_data_pointer += size;
   if( this->results_data_pointer >= this->max_results_data_size )
   {
      std::cerr << "Strengths tensor data buffer exceeded limit for results ( "
                << this->max_results_data_size << " )." << std::endl;
      throw( 0 );
   }
}

void StrengthsTensorDataBuffer::allocate_tensor( const size_t size,
                                                 strengthsTensorData*& host_data,
                                                 strengthsTensorData*& device_data )
{
   host_data = &this->tensors_data_host[ this->tensors_data_pointer ];
   device_data = &this->tensors_data_device[ this->tensors_data_pointer ];
   this->tensors_data_pointer += size;
   if( this->tensors_data_pointer >= this->max_tensors_data_size )
   {
      std::cerr << "Strengths tensor data buffer exceeded limit for strengthsTensorData ( "
                << this->max_tensors_data_size << " )." << std::endl;
      throw( 0 );
   }
}

void StrengthsTensorDataBuffer::transferData()
{
#ifdef HAVE_CUDA
   cudaMemcpy( this->uint8_data_device,  this->uint8_data_host,  this->uint8_data_pointer  * sizeof( uint8_t ),  cudaMemcpyHostToDevice );
   cudaMemcpy( this->uint16_data_device, this->uint16_data_host, this->uint16_data_pointer * sizeof( uint16_t ), cudaMemcpyHostToDevice );
   cudaMemcpy( this->int_data_device,    this->int_data_host,    this->int_data_pointer    * sizeof( int ),      cudaMemcpyHostToDevice );   
   cudaMemcpy( this->double_data_device, this->double_data_host, this->double_data_pointer * sizeof( double ),   cudaMemcpyHostToDevice );
   cudaMemcpy( this->double_ptr_data_device, this->double_ptr_data_host, this->double_ptr_data_pointer * sizeof( double* ),   cudaMemcpyHostToDevice );
   cudaMemcpy( this->tensors_data_device, this->tensors_data_host, this->tensors_data_pointer * sizeof( strengthsTensorData ),   cudaMemcpyHostToDevice );
#endif
}

void StrengthsTensorDataBuffer::transferResults()
{
#ifdef HAVE_CUDA
   cudaMemcpy( this->results_data_host, this->results_data_device, this->results_data_pointer * sizeof( double ),   cudaMemcpyDeviceToHost );
#endif
}

void StrengthsTensorDataBuffer::reset()
{
   this->uint8_data_pointer = 0;
   this->uint16_data_pointer = 0;
   this->int_data_pointer = 0;
   this->double_data_pointer = 0;
   this->double_ptr_data_pointer = 0;
   this->results_data_pointer = 0;
   this->tensors_data_pointer = 0;
}

StrengthsTensorDataBuffer::~StrengthsTensorDataBuffer()
{
   delete[] this->uint8_data_host;
   delete[] this->uint16_data_host;
   delete[] this->int_data_host;
   delete[] this->double_data_host;
   delete[] this->double_ptr_data_host;
   delete[] this->results_data_host;
   delete[] this->tensors_data_host;
#ifdef HAVE_CUDA
   cudaFree( this->uint8_data_device );
   cudaFree( this->uint16_data_device );
   cudaFree( this->int_data_device );
   cudaFree( this->double_data_device );
   cudaFree( this->double_ptr_data_device );
   cudaFree( this->results_data_device );
   cudaFree( this->tensors_data_device );
#endif
}
