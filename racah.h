#ifndef racahH
#define racahH

#include <iostream>
#include "functions.h"
#include "logFactTable.h"
#include "device.h"
#include "logDelta2.h"


template< typename Device >
class racahCoefficients
{
   public:
   
#ifdef HAVE_CUDA
   __device__ __host__
#endif   
   racahCoefficients( const logFactTable< Device >* lfTable )
   : lfTable( lfTable ),
     lgDelta2( lfTable )
   {
   };

#ifdef HAVE_CUDA
   __device__ __host__
#endif   
   double getCoefficient( const int a2,
                          const int b2,
                          const int e2,
                          const int d2,
                          const int c2,
                          const int f2 ) const
   {
      double result( 0.0 ), numerator( 0.0 ), denominator( 0.0 );

      if( isAllowed( a2, b2, c2 ) &&
          isAllowed( c2, d2, e2 ) &&
          isAllowed( a2, e2, f2 ) &&
          isAllowed( b2, d2, f2 ) )
      {
          int zmin, zmax;
          zmin = max( max( 0, -(-a2 + c2 - d2 + f2) ),
                      -(-b2 + c2 - e2 + f2) );
          zmax = min( min( min( min( a2 + b2 + d2 + e2 + 2,
                                     a2 + b2 - c2 ),
                                -c2 + d2 + e2 ),
                           a2 + e2 - f2),
                      b2 + d2 - f2);
         
          numerator = 0.5 * ( lgDelta2.getLogDelta2( a2, b2, c2 ) + 
                              lgDelta2.getLogDelta2( c2, d2, e2 ) +
                              lgDelta2.getLogDelta2( a2, e2, f2 ) +
                              lgDelta2.getLogDelta2( b2, d2, f2 ) );

          for( int z2 = zmin; z2 <= zmax; z2 += 2 )
          {
             denominator = lfTable->getLogFact( ( a2 + b2 + d2 + e2 + 2 - z2 ) / 2 )
                           - ( lfTable->getLogFact( z2/2 ) +
                               lfTable->getLogFact( ( a2 + b2 - c2 - z2 ) / 2 ) +
                               lfTable->getLogFact( (-c2 + d2 + e2 - z2 ) / 2 ) +
                               lfTable->getLogFact( ( a2 + e2 - f2 - z2 ) / 2 ) +
                               lfTable->getLogFact( ( b2 + d2 - f2 - z2 ) / 2 ) +
                               lfTable->getLogFact( (-a2 + c2 - d2 + f2 + z2 ) / 2 ) +
                               lfTable->getLogFact( (-b2 + c2 - e2 + f2 + z2 ) / 2 ) );

              if( isOdd( z2 / 2 ) )
                  result -= exp( numerator + denominator );
              else
                  result += exp( numerator + denominator );
          }
      }
      return result;
  };
  protected:

  const logFactTable< Device >* lfTable;

  const logDelta2< Device > lgDelta2;
};

#endif
